///Ext.Loader.setPath('Ext.ux', '../ux');
Ext.application({
    
    requires: ['Ext.container.Viewport',
               'Ext.ux.form.field.BoxSelect',
               'Ext.ux.GMapPanel'
               
               
    ],
    name: 'PYA',
    appFolder: 'app',
    controllers: [
        'Grouptabs',
        'Handbooks',
        'Lots',
        'BlocksController',
        'AddEditLots',
     ],
    launch: function() {
        //Ext.QuickTips.init();
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
             items: [{
             xtype: 'grouptabpanel',
              activeGroup: 0,
              items: [ {
                        mainItem: 1,
                        items: [ {
                            title: 'Здания',
                            tabTip: 'Tickets tabtip',
                            border: false,
                            xtype: 'AddEditLotLayout',
                            //margin: '10',
                            //html:'rrr',
                            layout: 'fit',
                            height: null
                            
                        },
                        { title: 'Блоки',
                            iconCls: 'x-icon-subscriptions',
                            tabTip: 'Subscriptions tabtip',
                            style: 'padding: 10px;',
                            border: false,
                            layout: 'fit',
                            xtype:'BlocksLayout'
                          },
//                        {
//                            title: 'Продажа',
//                            iconCls: 'x-icon-subscriptions',
//                            tabTip: 'Subscriptions tabtip',
//                            style: 'padding: 10px;',
//                            border: false,
//                            layout: 'fit',
//                            xtype:'LotLayout'
//                        }
                    ]
                    },
                    /////
                   
                   
//                        {
//                        mainItem: 0,
//                        items: [ {
//                            xtype: 'portalpanel',
//                            title: 'Загородная',
//                            tabTip: 'Dashboard tabtip',
//                            border: false,
//                            html: 'Всё что касается отдела загородной недвижимости'
//                        }, {
//                            title: 'Продажа',
//                            iconCls: 'x-icon-subscriptions',
//                            tabTip: 'Subscriptions tabtip',
//                            style: 'padding: 10px;',
//                            border: false,
//                            layout: 'fit',
//                            items: [{
//                                xtype: 'tabpanel',
//                                activeTab: 1,
//                                items: [{
//                                    title: 'Nested Tabs',
//                                    html: 'Ext.example.shortBogusMarkup'
//                                }]
//                            }]
//                        }]
//                    }
//                    ,
                    //////
                  { expanded: true,
                    mainItem: 0,   
                    items: [{
                        title: 'Настройки',
                        iconCls: 'x-icon-configuration',
                        tabTip: 'Configuration tabtip',
                        style: 'padding: 10px;',
                        html: 'В этом разделе приведены настройки приложения'
                        
                    }, {
                        title: 'Справочники',
                        iconCls: 'x-icon-templates',
                        tabTip: 'Templates tabtip',
                        style: 'padding: 10px;',
                        border: false,
                        xtype:'HdbLayout'
                       
                    }, {
                            title: 'Сотрудники',
                            iconCls: 'x-icon-users',
                            tabTip: 'Users tabtip',
                            style: 'padding: 10px;',
                            html: 'Ext.example.shortBogusMarkup'
                        }]
                }, 
             { expanded: false,
                items: {
                    title: 'Аналитика',
                    bodyPadding: 10,
                    html: '<h1>Различные графики и отчёты</h1>',
                    border: false
                }
            }, 
             { expanded: false,
                items: {
                    title: 'Заявки',
                    bodyPadding: 10,
                    html: '<h1>Заявки</h1>',
                    border: false
                }
            },{ expanded: false,
                items: {
                    title: 'Звонки',
                    bodyPadding: 10,
                    html: '<h1>Звонки</h1>',
                    border: false
                }
            }]
        }]
        });
    }
});