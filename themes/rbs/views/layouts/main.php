<?php /* @var $this Controller */ ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
        <!-- Ext -->
        
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/ext-4/resources/css/ext-all.css">
    
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/grouptabs/portal.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/grouptabs/GroupTabPanel.css" />

    
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/ext-4/shared/writer.css" />

    
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/css/boxselect/BoxSelect.css" />
    <!-- GC -->

    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/ext-4/ext.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/grouptabs/all-classes.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/ext-4/locale/ext-lang-ru.js"></script>
    
    
    
    
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/boxselect/BoxSelect.js"></script>
     <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/GMapPanel/GMapPanel.js"></script>

    <!-- page specific -->
    <style type="text/css">
        /* styles for iconCls */
        .x-icon-tickets {
            background-image: url('images/tickets.png');
        }
        .x-icon-subscriptions {
            background-image: url('images/subscriptions.png');
        }
        .x-icon-users {
            background-image: url('images/group.png');
        }
        .x-icon-templates {
            background-image: url('images/templates.png');
        }
    </style>
    <?php 
       $uid = Yii::app()->user->getId(); 
      if ($uid!=0)
        {
          echo '<script type="text/javascript" src="'.Yii::app()->request->baseUrl.'/app.js"></script>';
          
        }
        else {
          echo '<script type="text/javascript" src="'.Yii::app()->request->baseUrl.'/login.js"></script>';   
         }
        
    ?>
    
    <title> <?php echo CHtml::encode(Yii::app()->name) .' -  '. Yii::app()->user->name .'-'.Yii::app()->request->baseUrl; ?></title>
</head>

<body>
<?php /*?>
<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->
<?php */ ?>
 
</body>
</html>
