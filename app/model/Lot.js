Ext.define('PYA.model.Lot', {
    extend: 'Ext.data.Model',
    fields: ['ID','LAST_DATE', 'LOT', 'ADDRESS', 'NAME', 'PRICE_RUB','PRICE_USD','PRICE_EUR', 'AREA',
            'METRO_SATATION', 'DIRECTON',  'BUILDING_CLASS', 'RENT_WITH_NDS', 'READY_VALUE','LOT_PIC','ROOMS_AREA_DIAPAZON']
    
});

