Ext.define('PYA.model.Handbook.Handbook', {
    extend: 'Ext.data.Model',
    fields: ['ID', 'HANDBOOK','H_GROUP_ID','LAST_DATE',
        'LAST_UID','USER_NAME']
  
});