Ext.define('PYA.model.Handbook.HandbookGroup', {
    extend: 'Ext.data.Model',
    fields: ['ID', 'GROUP_NAME', 'LAST_DATE', 'LAST_UID']
});