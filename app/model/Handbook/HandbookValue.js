Ext.define('PYA.model.Handbook.HandbookValue', {
    extend: 'Ext.data.Model',
    fields: ['ID', 'HANDBOOK_ID', 'INDEX_ID', 'VARCHAR_VALUE', 'INT_VALUE', 'DATE_VALUE', 'ORDER_ID', 'LAST_DATE', 'LAST_UID']
    
    
});