Ext.define('PYA.model.LotRooms', {
    extend: 'Ext.data.Model',
    fields: ['ID','FLOOR_NUMBER', 'AREA', 'BROKER', 'OUR_PERCENT', 
              'RENT_PRICES_RUB','RENT_PRICES_USD','RENT_PRICES_EUR', 'WITH_NDS', 'FINISHING','CONTARCT_TYPE',
              'PLANING','OPERATING_COSTS','UTILITY_BILLS','DEPOSITS',
              'PREPAYD_VALUE','RENT_TYPE', 'RENT_CONTRACT_ID',
              'SALE_PRICES', 'SALE_CONTRACT_ID', 'LAST_DATE', 'LAST_UID ',
              'READY_DATE','ROOM_PIC','ROOM_LOT'
            ],
    
});

