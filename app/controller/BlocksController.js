Ext.define('PYA.controller.BlocksController', {
    extend: 'Ext.app.Controller',
    views: [
      'Blocks.BlocksLayout',
      'Lists.Blocks.BlockList',
      'Panels.Blocks.BlocksSearchPan'
    ],
    stores: ['Blocks.BlockCardStore',
             'Blocks.BlocksStore',
             'Lots.LotRooms_1',
            ],
     //models: ['Blocks.BlockModel'],
    refs:[
        { ref : 'blockslist',     selector: 'blockslist'     },
        { ref : 'searchblocks', selector: 'searchblocks'},
        { ref: 'tabs', selector: 'BlocksLayout' },
    ],  
    init: function() {
      this.control({
            'searchblocks button[action=search]':{
                click: this.DoSearch
            },
            'blockslist': {
                itemclick: this.updateInfo,
                itemdblclick: this.addTab,
            },
      })
      console.log('Initialized blocks');
//        this.control({
////            'viewport > userlist': {
////                itemdblclick: this.editUser
////            },
////            'useredit button[action=save]': {
////                click: this.updateUser
////            },
////            'userlist': {
////                itemclick: this.editUser1
////            },
////            'detailpanel > body':{
////                click: this.mapClick                
////            }
//        });
//        
    },
    DoSearch:function(button){
        
      var win = button.up('panel'),
        form   = win.down('form'),
        //record = form.getRecord(),
        values = form.getValues();
        //var tabs = this.getTabs();
        var params = {'subway_id':[values['subway_id']],
                      'PRICE_FROM':values['PRICE_FROM'],
                      'PRICE_TO':values['PRICE_TO'],
                      'rent_curr_id':values['rent_curr_id'],
                      'PRICE_TYPE':values['PRICE_TYPE'],
                      'AREA_FROM':values['AREA_FROM'],
                      'AREA_TO':values['AREA_TO'],

                       'limit':	25,
                       'page':	1,
                       'start':0
                    };
        console.log(values['PRICE_FROM']);
     //   this.getStore('Blocks.BlocksStore').getProxy().extraParams = params;
      //  this.getStore('Blocks.BlocksStore').reload(); 
        
      //  tabs.setActiveTab(1);
        
  },
  updateInfo:function(grid, record){
            console.log('Grid id - '+record.get('LOT_ID'));
             var params = {'ID':record.get('ID'),
                         'limit':	25,
                         'page':	1,
                         'start':0
                    };
        
//        
        this.getStore('Lots.LotRooms_1').getProxy().extraParams = params;
        this.getStore('Lots.LotRooms_1').reload(); 
//        
//        this.getStore('Contacts.LotOwners').getProxy().extraParams = params;
//        this.getStore('Contacts.LotOwners').reload(); 
//        
//        this.getStore('Rooms.LotRooms').getProxy().extraParams = params;
//        this.getStore('Rooms.LotRooms').reload(); 
             
             
  },          
          
  addTab:function(grid, record){
        
        var items = [],
            item,
            cardAddres,
            cardStore,
            title;
        
        item =  record.get('ID')
      
        cardStore = this.getStore('Lots.LotRooms_1');
      
        if(cardStore.lastOptions) {
        var recdata  = cardStore.proxy.reader.jsonData;
        //var recdata =  this.getDataForCard(item);
               cardAddres = recdata.ADDRESS;
               title =  record.get('ID') + ' '+ recdata.ADDRESS;
               var tabs = this.getTabs();   

              var tab = tabs.add(Ext.create("PYA.view.lot.TabView",{
                         //inTab: true,    
                        active: record,
                        title: title,
                        //id:item,
                        closable:true,
                        data: recdata,
                        //store:'Lots.LotRooms_1',
                        //html:'html1'    

                    }));
              tabs.setActiveTab(tab);
            
        }
        
       
  } 
          
             
     
    
    
});