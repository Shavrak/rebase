Ext.define('PYA.controller.AddEditLots', {
    extend: 'Ext.app.Controller',
    views: [
        'Lists.Lots.List',
        'Panels.Lots.ComonInfo',
        'Panels.Lots.Infrastructure',
        'Panels.Lots.RoomsPanel',
        'Address.Addresses',
        'Address.AddressList',
        'Address.Landmarks',
        'Address.AddressLandmark',
        //'Forms.Lots.Add',
        'Forms.Lots.AddCommonInfo',
        'Forms.Contacts.AddNewOwner',
        'Forms.Contacts.AddNewCompany',
        'Forms.Contacts.AddOwnersToLot',
        'Forms.Contacts.AddNewAgent',
        'Forms.Elevators.AddNewElevator',
        'Contacts.OwnersList',
        'Panels.Contacts.LotOwnerPanel',
        'Forms.Rooms.AddRoom',
        'Forms.Rooms.wOwnerContract',
        'Lists.Rooms.List',
        'lot.AddEditLotLayout',
        ///поисковая форма
        'SearchPanel.ComonPanel'
        
    ],
     stores: ['Address.Streets', 'Address.LotStreets','Address.Subways',
              'Address.Directons','Address.Locations','Address.Railways', 
              'Address.Districts','Users.WholeUserList','Lots.LotStatus','Lots',
              'Handbooks.CurrencyList','Handbooks.ContractType','Handbooks.Deposit',
              'Handbooks.Planning','Handbooks.RentType','Handbooks.OwnerContractType',
              'Handbooks.UnityBills','Handbooks.WithNDS', 'Handbooks.Finishing',
              'Handbooks.Elevators',
              'Handbooks.BuldingClass','Handbooks.BuldingActivePassiv','Handbooks.BuldingReady',
              'Handbooks.BuldingType',
              'Contacts.Owners', 'Contacts.LotOwners','Contacts.Companies',
              'Contacts.Agents',
              'Rooms.LotRooms','Address.MapAddresses','Handbooks.PriceType',
              'Lots.LotRooms_1',
             ],
     refs: [
        { ref : 'gridlots',       selector: 'gridlots'     },
        { ref : 'comoninfopanel', selector: 'comoninfopanel'},
        { ref : 'lotownerpanel', selector: 'lotownerpanel'},
        { ref : 'ownerlist', selector: 'ownerlist'},
        { ref : 'gridrooms', selector: 'gridrooms'},
        { ref: 'tabs', selector: 'AddEditLotLayout' },
        { ref : 'comonsearch', selector: 'comonsearch'},
        
        
   ],
     
  init: function() {
        this.control({
            'comonsearch button[action=search]':{
                click: this.DoSearch
            },
            'gridlots toolbar [action=addbuilding]': {
                click: this.addLot
            },
            'gridlots toolbar [action=booklet]': {
                click: this.openBooklet
            },
            'gridlots': {
                itemclick: this.updateInfo,
                itemdblclick: this.addTab,
            },
            'AddBuilding button[action=AddressChoise]': {
                click: this.AddressChoise
            },
            'AddBuilding button[action=LandmarkChoise]': {
                click: this.LandmarkChoise
            },
            'AddBuilding button[action=savelot]': {
                click: this.SaveNewLot
            },
            'AddBuilding button[action=OwnerChoise]': {
                click: this.addOwnerToLot
            },
            'AddBuilding button[action=DeveloperChoise]': {
                click: this.newCompany
            },
            'AddBuilding button[action=InvestorChoise]': {
                click: this.newCompany
            },
            'AddBuilding button[action=ManageCompanyChoise]': {
                click: this.newCompany
            },
            
            'AddBuilding button[action=ExclAgentChoise]': {
                click: this.newAgent
            },
            'AddBuilding button[action=CoExclAgentChoise]': {
                click: this.newAgent
            },
            'AddBuilding button[action=PassangerElevator]': {
                click: this.newElevatorP
            },
            'AddBuilding button[action=CargoElevator]': {
                click: this.newElevatorC
            },
            'NewElevator button[action=saveElevator]': {
                click: this.saveElevator
            },
            'NewAgent button[action=saveAgent]': {
                click: this.saveAgent
            },
            'comoninfopanel toolbar [action=addCommonInf]': {
                click: this.addCommonInfo
            }
            ,
            'addresses button[action=chose]': {
                click: this.AdddresDone
            },
            'landmarks button[action=chose]': {
                click: this.LandmarksDone
            },
            
            'AddOwnersToLot button[action=chose]': {
                click: this.OwnersToLotDone
            },
            'AddOwnersToLot toolbar [action=newOwner]': {
                click: this.newOwner
            },
            
            'AddNewOwner button[action=saveOwner]': {
                click: this.saveOwner
            },
            'AddOwnersToLot toolbar [action=newCompany]': {
                click: this.newCompany
            },
            'AddNewCompany button[action=saveCompany]': {
                click: this.saveCompany
            },
            'ownerlist toolbar [action=removeOwner]': {
                click: this.removeOwnerFromLot
            },
            'lotownerpanel toolbar [action=newCompany]': {
                click: this.newCompany
            },
            'lotownerpanel toolbar [action=newOwner]': {
                click: this.newOwner
            },
            'roomspanel toolbar [action=addRoom]': {
                click: this.addRoom
            },
            // Новые помещения
            'AddRoom button[action=saveroom]': {
                click: this.saveRoom
            },
            'AddRoom button[action=RoomOwnerContrRent]': {
                click: this.RoomOwnerContrRent
            },
            'AddRoom button[action=RoomOwnerContrSale]': {
                click: this.RoomOwnerContrSale
            },
            'ownercontract button[action=saveContract]': {
                click: this.ContractSave
            },
            
            
        });
    },
  
  addLot:function(){

                var view = Ext.widget('AddBuilding');    
            /*создаю */
             view.down('form').getForm().load({
               url: 'index.php?r=lot/addlot',
             });

      
//          var lot_id = 0;
//          var grid = this.getGridlots();
//          if (grid.getSelectionModel().hasSelection()) {
//            var row = grid.getSelectionModel().getSelection()[0]],
//            lot_id = row.get('ID')
//         }
//          
//          if (lot_id ==0){
//              
//          }else{
//            Ext.MessageBox.alert('Внимание!', 'Логика для редактирования здания');
//            
//          }
        
      
      
  },     
  SaveNewLot:function(button){
        console.log('Save New Lot');
        var win = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
          
          
           Ext.Ajax.request({
            url: 'index.php?r=Lot/SaveNewLot', 
            params: { 
                lot_id              :[values['lot_id']],
                lot                 :[values['lot']],
                lot_name            :[values['lot_name']],
                broker_id           :[values['broker_id']],
                lot_status_id       :[values['lot_status_id']],
                create_date         :[values['create_date']],
                ////Входные данные
                developer           :[values['developer']],
                investor            :[values['investor']],
                project_cost        :[values['project_cost']],
                curr_proj_cost      :[values['curr_proj_cost']],
                exclusive_agent     :[values['exclusive_agent']],
                coexclusive_agent   :[values['coexclusive_agent']],
                manage_company      :[values['manage_company']],
                ////Данные по зданию
                ready_id            :[values['ready_id']],
                ready_dt            :[values['ready_dt']],
                type_id             :[values['type_id']],
                class_id            :[values['class_id']],
                ////Площадь
                comon_area          :[values['comon_area']],
                office_area         :[values['office_area']],
                trade_area          :[values['trade_area']],
                guest_area          :[values['guest_area']],
                intertaiment_area   :[values['intertaiment_area']],
                apartment_area      :[values['apartment_area']],
                live_area           :[values['live_area']],
                wh_area             :[values['wh_area']],
                industrial_area     :[values['industrial_area']],
                ////Количество этажей
                upground_count      :[values['upground_count']],
                undeground_count    :[values['undeground_count']],
                is_mansard          :[values['is_mansard']],
                is_basement         :[values['is_basement']],
                build_year          :[values['build_year']],
                reconstruction_year :[values['reconstruction_year']],
                state_commision     :[values['state_commision']],
                certificate         :[values['certificate']],            
                ////Технические характеристики
                PassangerElevatorId :[values['PassangerElevatorId']],
                CargoElevatorId     :[values['CargoElevatorId']],
                ceiling_height      :[values['ceiling_height']],
                column_grid         :[values['column_grid']],            
                floor_depth         :[values['floor_depth']],            
                max_floor_area      :[values['max_floor_area']],            
                min_block           :[values['min_block']],           
                ////Аренда
                rent_price          :[values['rent_price']],            
                rent_curr_id        :[values['rent_curr_id']],            
                rent_with_nds       :[values['rent_with_nds']],            
                operating_costs     :[values['operating_costs']],
                operating_curr_id   :[values['operating_curr_id']],
                utility_bills_id    :[values['utility_bills_id']],            
                deposit_id          :[values['deposit_id']],            
                prepayd_value       :[values['prepayd_value']],            
                rent_type_id        :[values['rent_type_id']],            
                ////Продажа
                sale_bloks          :[values['sale_bloks']],            
                sale_price          :[values['sale_price']],            
                sale_curr_id        :[values['sale_curr_id']],            
                sale_with_nds       :[values['sale_with_nds']],            
                ////
                object_cost         :[values['object_cost']],            
                object_curr_id      :[values['object_curr_id']],            
                object_with_nds     :[values['object_with_nds']],    
              
            },
            headers: {
                'Accept': 'application/json'
            },
            scope:this,
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                console.log(record);
                this.getStore('Lots').reload();
                this.win.close();
                
            }
        }); 
        
      
  },        
          
  openBooklet:function(){
      console.log('I click  openBooklet');
       var lot_id = 0;
          var grid = this.getGridlots();
          if (grid.getSelectionModel().hasSelection()) {
            var row = grid.getSelectionModel().getSelection()[0],
            lot_id = row.get('ID')
         }
          
          if (lot_id ==0){
              Ext.MessageBox.alert('Внимание!', 'Выберите лот из списка, для создании листовки!');
              
          }else{
            Ext.MessageBox.alert('Внимание!', 'Логика для создания листовки');
            
          }
      //var view = Ext.widget('AddBuilding');
  },     
  AddressChoise:function(button){
        var view = Ext.widget('addresses');
       /*передаю в окно адресов id только что созданного лота*/ 
       view.down('form').getForm().load({
        url: 'index.php?r=lot/GetDraftLot',
       });
      
  },  
  AdddresDone:function (){
          console.log('address done');
          this.windowAlreadyOpen(9).close();
          var window = this.windowAlreadyOpen(7);
           //widget('AddBuilding').show();
          var form =  window.down('form').getForm();
          form.findField("address").setValue('заполнен');
  },           
 windowAlreadyOpen: function(id){
        //Check for already opened window with the same  ID
        var windowOpen = Ext.ComponentQuery.query('window[id='+id+']'); //always returns array
        if (windowOpen && windowOpen[0]){
                try {
                    windowOpen[0].toFront();            //bring to front
                    if (windowOpen[0].collapsed){
                       windowOpen[0].toggleCollapse();  //expand window if collapsed
                    }
                } catch (e) {
                    console.error(e);
                }
                return windowOpen[0]
        }
        return false;
    },         
          
  LandmarkChoise:function(){
      var view = Ext.widget('landmarks');
       /*передаю в окно адресов id только что созданного лота*/ 
       view.down('form').getForm().load({
        url: 'index.php?r=lot/GetDraftLot',
       });
      
  },   
  LandmarksDone:function (button){
        console.log('landmarks done');
        var win = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
           Ext.Ajax.request({
            url: 'index.php?r=address/SaveLandmarks', 
            params: { 
                lot_id: values['lot_id'],
                subway_id:[values['subway_id']],
                sbw_min_walk:values['sbw_min_walk'],
                sbw_min_auto:values['sbw_min_auto'],
                railway_id:[values['railway_id']],
                railway_min_walk:values['railway_min_walk'],
                railway_min_auto:values['railway_min_auto'],
                district_id:values['district'],
                direction_id:values['direction'],
                location_id:values['location'],
                za_mkad:values['za_mkad'],
                description_loaction: values['description_loaction'],
                
            },
            headers: {
                'Accept': 'application/json'
            },
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                console.log(record);
            }
        }); 
          
          
          this.windowAlreadyOpen(10).close();
          var window = this.windowAlreadyOpen(7);
           //widget('AddBuilding').show();
          var form_a =  window.down('form').getForm();
          form_a.findField("landmarks").setValue('заполнен');
  }, 
  addCommonInfo:function(){
          var lot_id = 0;
          var grid = this.getGridlots();
          if (grid.getSelectionModel().hasSelection()) {
            var row = grid.getSelectionModel().getSelection()[0]
            lot_id = row.get('ID')
         }
          
          if (lot_id ==0){
              Ext.MessageBox.alert('Внимание!', 'Выберите лот из списка!');
          }else{
            var view = Ext.widget('AddCommonInfo');
           //widget('AddBuilding').show();
            var form =  view.down('form').getForm();
            form.findField("lot_id").setValue(lot_id);  
            
          }
  }, 
          
  newOwner:function(){
      var view = Ext.widget('AddNewOwner'); 
  },        
  saveOwner:function(button){
      var win = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
        Ext.Ajax.request({
            url: 'index.php?r=contacts/AddOwner', 
            params: { 
                NAME:values['name'],
                PHONE:values['phone'],
                COMPANY_ID:values['company']
            },
            headers: {
                'Accept': 'application/json'
            },
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                console.log(record);
                
            }
        });
        this.windowAlreadyOpen(14).close();
        var window = this.windowAlreadyOpen(13);
        this.getStore('Contacts.Owners').reload();
  },   
  newCompany:function(){
      var view = Ext.widget('AddNewCompany'); 
  },        
  saveCompany:function(button){
       var win = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
        Ext.Ajax.request({
            url: 'index.php?r=contacts/AddCompany', 
            params: { 
                NAME:values['name'],
                PHONE:values['phone'],
                ADDRESS_ID:values['address'],
                DESCRIPTION:values['description']
            },
            headers: {
                'Accept': 'application/json'
            },
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                console.log(record);
                
            }
        });
        this.windowAlreadyOpen(15).close();
        var window = this.windowAlreadyOpen(13);
        this.getStore('Contacts.Companies').reload();
  },
  
  
  
  newElevatorC:function(){
      var view = Ext.widget('NewElevator'); 
            var form =  view.down('form').getForm();
            form.findField("elevator_type").setValue('cargo');  
      
  },        
  newElevatorP:function(){
      var view = Ext.widget('NewElevator'); 
            var form =  view.down('form').getForm();
            form.findField("elevator_type").setValue('passanger');  
      
  },                
  saveElevator:function(button){
      var win = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();


                  Ext.Ajax.request({
                    url: 'index.php?r=lot/SaveElevator', 
                    params: { 
                        elevator_type:values['elevator_type'],
                        brand_id:values['brand_id'],
                        elevators_count:values['elevators_count'],
                        elevators_load:values['elevators_load']
                    },
                    headers: {
                        'Accept': 'application/json'
                    },
                    scope : this,
                    success: function(response){
                        // responseText should be in json format
                        record = response.responseText;
                        var obj = Ext.decode(response.responseText);
                      
                       
                       this.windowAlreadyOpen(19).close();
                       var window = this.windowAlreadyOpen(7);  
                       var form =  window.down('form').getForm();
                       if (obj.elevator_type ==='cargo'){
                           form.findField("CargoElevatorId").setValue(obj.elevator_id); 
                           form.findField("CargoElevator").setValue(obj.brand +'  г.п. '+obj.elevators_load +' ('+ obj.elevators_count+' шт.)'); 
                       }else{
                           form.findField("PassangerElevatorId").setValue(obj.elevator_id); 
                           form.findField("PassangerElevator").setValue(obj.brand +'  г.п. '+obj.elevators_load +' ('+ obj.elevators_count+' шт.)'); 
                       }
                       
                       
                        
                    }
                });

        
        //this.getStore('Contacts.Agents').reload();
  },
  
  newAgent:function(){
      var view = Ext.widget('NewAgent'); 
  },        
  saveAgent:function(button){
      var win = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
        Ext.Ajax.request({
            url: 'index.php?r=contacts/AddAgent', 
            params: { 
                NAME:values['name'],
                PHONE:values['phone'],
                AGENT_TYPE:values['AGENT_TYPE'],
                COMPANY_ID:values['company']
            },
            headers: {
                'Accept': 'application/json'
            },
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                console.log(record);
                
            }
        });
        this.windowAlreadyOpen(18).close();
        var window = this.windowAlreadyOpen(13);
        this.getStore('Contacts.Agents').reload();
  },
  
  addOwnerToLot:function(button){
          
//          var grid = this.getGridlots();
//          if (grid.getSelectionModel().hasSelection()) {
//            var row = grid.getSelectionModel().getSelection()[0]],
//            lot_id = row.get('ID')
//         }

        var win = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
        console.log('addOwnersToLot'+values['lot_id']);
          var view = Ext.widget('AddOwnersToLot');
           //widget('AddBuilding').show();
          var form =  view.down('form').getForm();
          form.findField("lot_id").setValue(values['lot_id']);  
          
  },  
  removeOwnerFromLot:function(button){
          var id = 0;  
          var grid = this.getOwnerlist();
          if (grid.getSelectionModel().hasSelection()) {
            var row = grid.getSelectionModel().getSelection()[0]
            id = row.get('ID')
          }
       Ext.Ajax.request({
            url: 'index.php?r=contacts/RemoveOwnerToLot', 
            params: { 
                ROW_ID:id,
            },
            headers: {
                'Accept': 'application/json'
            },
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                console.log(record);
            }
        }); 
        this.getStore('Contacts.LotOwners').reload(); 
     //delete:'index.php?r=contacts/RemoveOwnerToLot'  
  },    
  OwnersToLotDone:function(){
          console.log('OwnersToLotDone done');
           this.windowAlreadyOpen(13).close();
            var window = this.windowAlreadyOpen(7);
            var form =  window.down('form').getForm();
            form.findField("owner").setValue('Заполнен'); 
            
          
//          var window = this.windowAlreadyOpen(11);
//           //widget('AddBuilding').show();
//          var form =  window.down('form').getForm();
//          form.findField("owners").setValue('заполнен');
          
  },
  updateInfo:function(grid, record){
            console.log('Grid id - '+record.get('ID'));
             var params = {'LOT_ID':record.get('ID'),
                       'limit':	25,
                       'page':	1,
                       'start':0
                    };
        
//        
        this.getStore('Lots.LotRooms_1').getProxy().extraParams = params;
        this.getStore('Lots.LotRooms_1').reload(); 
//        
        this.getStore('Contacts.LotOwners').getProxy().extraParams = params;
        this.getStore('Contacts.LotOwners').reload(); 
        
        this.getStore('Rooms.LotRooms').getProxy().extraParams = params;
        this.getStore('Rooms.LotRooms').reload(); 
        
        
        
             
             
  },
          
addRoom:function(){
    
    var lot_id = 0;
          var grid = this.getGridlots();
          if (grid.getSelectionModel().hasSelection()) {
            var row = grid.getSelectionModel().getSelection()[0]
            lot_id = row.get('ID')
         }
          
          if (lot_id ==0){
              Ext.MessageBox.alert('Внимание!', 'Выберите лот из списка!');
          }else{
            var view = Ext.widget('AddRoom');
            var form =  view.down('form').getForm();
            form.findField("lot_id").setValue(lot_id);  
            
          }
    
    
        
  }, 
saveRoom:function(button){
       var win = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
        Ext.Ajax.request({
            url: 'index.php?r=room/AddRoom', 
            params: { 
                LOT_ID:values['lot_id'],
                AREA:values['area'],
                ROOM_LOT:values['room_lot'],
                FLOOR:values['floor'],
                READY_DATE:values['ready_date'],
                PLANING_ID:values['planing_id'],
                FINISHING_ID:values['finishing_id'],
                CONTRACT_TYPE_ID:values['contract_type_id'],
                BROKER_ID:values['broker_id'],
                ADDRESS:values['address'],
                RENT_PRICE:values['rent_price'],
                RENT_CURR_ID:values['rent_curr_id'],
                RENT_WITH_NDS:values['rent_with_nds'],
                OPERATING_COSTS:values['operating_costs'],
                UTILITY_BILLS_ID:values['utility_bills_id'],
                DEPOSIT_ID:values['deposit_id'],
                PREPAYD_VALUE:values['prepayd_value'],
                RENT_TYPE_ID:values['rent_type_id'],
                OUR_PERCENT:values['our_percent'],
                OwnerContrIdRent:values['OwnerContrIdRent'], 
                SALE_PRICE:values['sale_price'],
                SALE_CURR_ID:values['sale_curr_id'],
                SALE_WITH_NDS:values['sale_with_nds'],
                OwnerContrIdSale:values['OwnerContrIdSale'], 
                COMMENT:values['comment']
            },
            headers: {
                'Accept': 'application/json'
            },
            scope : this,
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                console.log(record);
                
                this.getStore('Rooms.LotRooms').reload();
                
            }
        });
        //this.windowAlreadyOpen(15).close();
        //var window = this.windowAlreadyOpen(13);
        
  },          
  
  RoomOwnerContrRent:function(){
      
     console.log('RoomOwnerContrRent');
     var view = Ext.widget('ownercontract'); 
      var form =  view.down('form').getForm();
          form.findField("contrType").setValue('rent');  
          
      
  },
  RoomOwnerContrSale:function(){
      console.log('RoomOwnerContrSale');
      var view = Ext.widget('ownercontract'); 
      var form =  view.down('form').getForm();
          form.findField("contrType").setValue('sale');  
  },
  ContractSave:function(button){
      var win = button.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();


        if (form.getForm().isValid()){
            if ( values['contrType'] == 'sale'){
                
                Ext.Ajax.request({
                    url: 'index.php?r=contacts/SaveOwnerContract', 
                    params: { 
                        CONTARCT_NUM:values['CONTARCT_NUM'],
                        DATE_OF_SIGNING:values['DATE_OF_SIGNING'],
                        DATE_OF_END:values['DATE_OF_END'],
                        CONTRACT_TYPE_ID:values['CONTRACT_TYPE_ID'],
                        BROKER_ID:values['broker_id'],
                        PECRENT_FROM_CONTRACT:values['PECRENT_FROM_CONTRACT'],
                        NOTE:values['NOTE'],
                        OWNER_ID:values['owner'],
                        IS_RENT:'0',
                    },
                    headers: {
                        'Accept': 'application/json'
                    },
                    scope : this,
                    success: function(response){
                        // responseText should be in json format
                        record = response.responseText;
                        var obj = Ext.decode(response.responseText);
                       this.windowAlreadyOpen(17).close();
                       var window = this.windowAlreadyOpen(16);
                       var form =  window.down('form').getForm();
                       form.findField("OwnerContrIdSale").setValue(obj.owner_contract); 
                       var outText = obj.contract_num;
                       if (obj.contract_dt_sg!=''){
                           outText = outText + ' подписан '+obj.contract_dt_sg;
                       }
                       form.findField("OwnerContrSale").setValue(outText ); 
                        
                    }
                });

            } else {
                console.log('ContractSave RENT');
                
                //owner_contract_id = getRes();
                
                 Ext.Ajax.request({
                             url: 'index.php?r=contacts/SaveOwnerContract', 
                             params: { 
                                 CONTARCT_NUM:values['CONTARCT_NUM'],
                                 DATE_OF_SIGNING:values['DATE_OF_SIGNING'],
                                 DATE_OF_END:values['DATE_OF_END'],
                                 CONTRACT_TYPE_ID:values['CONTRACT_TYPE_ID'],
                                 BROKER_ID:values['broker_id'],
                                 PECRENT_FROM_CONTRACT:values['PECRENT_FROM_CONTRACT'],
                                 NOTE:values['NOTE'],
                                 OWNER_ID:values['owner'],
                                 IS_RENT:'1',
                             },
                             headers: {
                                 'Accept': 'application/json'
                             },
                             scope : this,
                             success: function(response){
                                 // responseText should be in json format
                                 record = response.responseText;
                                 var obj = Ext.decode(response.responseText);
                                this.windowAlreadyOpen(17).close();
                                var window = this.windowAlreadyOpen(16);
                                var form =  window.down('form').getForm();
                                form.findField("OwnerContrIdRent").setValue(obj.owner_contract); 
                                var outText = obj.contract_num;
                                if (obj.contract_dt_sg!=''){
                                    outText = outText + ' подписан '+obj.contract_dt_sg;
                                }
                                form.findField("OwnerContrRent").setValue(outText ); 

                             },
                             
                         });
            }
        
        }else{
             Ext.Msg.alert('Invalid Data', 'Please correct form errors.')
        }
        
  },
  
  DoSearch:function(button){
        
      var win = button.up('panel'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
        var tabs = this.getTabs();
        var params = {'subway_id':[values['subway_id']],
                      'PRICE_FROM':values['PRICE_FROM'],
                      'PRICE_TO':values['PRICE_TO'],
                      'rent_curr_id':values['rent_curr_id'],
                      'PRICE_TYPE':values['PRICE_TYPE'],
                      'AREA_FROM':values['AREA_FROM'],
                      'AREA_TO':values['AREA_TO'],

                       'limit':	25,
                       'page':	1,
                       'start':0
                    };
        
        this.getStore('Lots').getProxy().extraParams = params;
        this.getStore('Lots').reload(); 
        
        tabs.setActiveTab(1);
        
  },
   
    getDataForCard: function(item) {
        
       var params = {'LOT_ID':item,
                       'limit':	25,
                       'page':	1,
                       'start':0
                    }, cardStore;
        console.log('addTab ID '+item); 
        
        cardStore = this.getStore('Lots.LotRooms_1');
        cardStore.getProxy().extraParams = params;
        cardStore.reload();
        return cardStore.proxy.reader.jsonData;
        
    },        
  addTab:function(grid, record){
        
        var items = [],
            item,
            cardAddres,
            cardStore,
            title;
        
        item =  record.get('ID')
      
        cardStore = this.getStore('Lots.LotRooms_1');
      
        if(cardStore.lastOptions) {
        var recdata  = cardStore.proxy.reader.jsonData;
        //var recdata =  this.getDataForCard(item);
               cardAddres = recdata.ADDRESS;
               title =  record.get('LOT') + ' '+ recdata.ADDRESS;
               var tabs = this.getTabs();   

              var tab = tabs.add(Ext.create("PYA.view.lot.TabView",{
                         //inTab: true,    
                        active: record,
                        title: title,
                      //  id:item,
                        closable:true,
                        data: recdata,
                        //store:'Lots.LotRooms_1',
                        //html:'html1'    

                    }));
              tabs.setActiveTab(tab);
            
        }
        
       
  }        

          

//    
    
});