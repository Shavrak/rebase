Ext.define('PYA.controller.Lots', {
    extend: 'Ext.app.Controller',
    views: [
        'lot.List',
        'lot.Search',
        'lot.DetailPanel',
        'lot.LotLayout',
        'Address.Addresses',
        'Address.AddressList',
        'Address.Landmarks',
        'Address.AddressLandmark',
        'Forms.Lots.LotAdd',
        
    ],
     stores: ['Lots','ComonInfo', 'Rooms.LotRooms'],
     models: ['Lot','LotRooms'],
   refs: [
        { ref: 'tabs', selector: 'LotLayout' }
    ],   
  init: function() {
      // console.log('Initialized Users! This happens before the Application launch function is called');
        this.control({
            'searchlot button[action=search]': {
                click: this.lotSearch
            },
           'lotlist': {
                scope:this,
                itemclick: this.lotClick,
                itemdblclick: this.addTab,
                addlot1: this.addLot
                
            },
            'lotlist toolbar [action=addlot]': {
                click: this.addLot
            },
            'AddLot button[action=AddressChoise]': {
                click: this.AddressChoise
            }
            ,
            'AddLot button[action=LandmarkChoise]': {
                click: this.LandmarkChoise
            }
        });
        

    },
  addLot:function(){
      
      console.log('I click  AddLot');
      var view = Ext.widget('AddLot');
  },     
  AddressChoise:function(){
      console.log('I click  AddressChoise');
      var view = Ext.widget('addresses');
  },  
  LandmarkChoise:function(){
      console.log('I click  AddressChoise');
      var view = Ext.widget('landmarks');
  },          
  lotClick: function(grid, record) {
         //var view1 = Ext.widget('yamap'); 
       //  console.log('I click '+ record.get('ID'));
         this.getComonInfoStore().reload({params: { ID:record.get('ID')}});   
        
    },
  lotSearch : function(button) {
      //console.log('I click SEARCH');
      var win    = button.up('panel'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
        
        form.submit();
//     this.getLotsStore().reload({params: { ADDRESS:values['ADDRESS']
//                                          ,METRO_SATATION:values['METRO_SATATION']
//                                          ,DIRECTON:values['DIRECTON']  
//                                          ,PRICE_FROM:values['PRICE_FROM']
//                                          ,PRICE_TO:values['PRICE_TO']
//                                          ,CURRENCY:values['CURRENCY']
//                                          ,AREA_FROM:values['AREA_FROM']
//                                          ,AREA_TO:values['AREA_TO']
//                                          ,CLASS_ID:values['CLASS_ID']
//                                          ,SEARCH:values['YES']
//                                          
//                                        }});   
        console.log('I click SEARCH '+values['DIRECTON']);
//
//    record.set(values);
//    win.close();
//    this.getUsersStore().sync();
    }    
    ,editUser: function(grid, record) {
        
         var view = Ext.widget('searchlot');
         view.down('form').loadRecord(record);
    },
    addTab: function(grid, record) {
                var LotLayou = Ext.widget('LotLayout');
                var tabs = this.getTabs();

            var Lot = Ext.ModelManager.getModel('LotRooms');
            this.fireEvent('tap');
            
             // console.log("Loaded lot 1: "); 
//                LotR.load(1, {
//                    
//                    success: function() {
//                        console.log("Loaded lot 1: ");
//                    }
//                });
        
            title = record.get('NAME');
            item = this.getTabByTitle(title);
            if (!item) {
                    var html1 = 'test';
                        var params = {'LOT':record.get('ID')};
                        this.getStore('Rooms.LotRooms').getProxy().extraParams = params;
                        var storetab =  this.getStore('Rooms.LotRooms').load();

                    if (record.get('ID')==1){
                      html1 = 'Tverskaya 16'+
                              '<br><img src="app/store/img/Tverskaya16.jpg" width="200">'+
                               '<img src="app/store/img/41.jpg" width="200">'+
                               '<img src="app/store/img/43.jpg" width="200">';    
                    } else {
                        html1 = 'Izvestiya hall'+
                              '<br><img src="app/store/img/Izvestiya.jpg" width="200">'+
                              '<img src="app/store/img/31.jpg" width="200">';        
                    }
                    
                  var tab = tabs.add(Ext.create("PYA.view.lot.TabView",{
                    title: record.get('NAME'),
                    //id:record.get('ID'),
                    closable:true,
                    store:storetab,
                    html:html1
                    
                }));
                tabs.setActiveTab(tab);
            }
            tabs.setActiveTab(item);
//            var params = {'LOT':record.get('ID')};
//            this.getStore('LotRooms').getProxy().extraParams = params;
//            this.getStore('LotRooms').load();
                
//              if (!this.getTabByTitle(record.get('NAME'))) {
//                var count = LotLayou.items.getCount();
//                var tab = tabs.add(Ext.create("PYA.view.lot.TabView",{
//                    title: record.get('NAME'),
//                    closable:true,
//                    //html: 'Html ' + record.get('ID')
//                }));
//                
//                tabs.setActiveTab(tab);
//                }
        
//        var id = '3';
//        var cls = "PYA.view.lot.TabView";
//        var tabs = this.getTabs();
//        var tab = tabs.child('#' + id);
// 
//        if (!tab) {
//            tab = tabs.add(Ext.create(cls, {
//                itemId: id,
//                title: 'text'
//            }));
//        }
//        tabs.setActiveTab(tab);
    },
      getTabByTitle: function(title) {
        var tabs = this.getTabs();
        var index = tabs.items.findIndex('title', title);
        return (index < 0) ? null : tabs.items.getAt(index);
    }
    
    
});