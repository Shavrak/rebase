Ext.define('PYA.controller.Grouptabs', {
    extend: 'Ext.app.Controller',
    views: [
      'Handbooks.HdbLayout',
      'Handbooks.Handbooks',
      'Handbooks.wHndbValue',
      'Handbooks.HandbooksAdd',
      'Handbooks.HandbooksValues'
      
    ],
      stores: ['Handbooks.Handbooks','Handbooks.HandbooksGroups','Handbooks.HandbooksValues','Handbooks.HandbooksCategory'],
      models: ['Handbook.Handbook','Handbook.HandbookGroup','Handbook.HandbookValue'],
       refs: [
        { ref : 'hndbValues',       selector: 'hndbValues'},
        { ref : 'hndb',       selector: 'hndb'},
        
        
        
        
   ],
  init: function() {
        this.control({
//            'handbooksedit button[action=save]': {
//                click: this.updateHadbooks
//            },
            
            
            'hndb button[action=new]': {
                click: this.newHadbook
            },
            'hndb': {
                itemclick: this.loadValues,
                itemdblclick: this.editHadbooks
            },
            'hndbValues': {
                itemdblclick: this.editHadbooksValue
            },
            
            'handbooksadd button[action=create]': {
                click: this.saveHandbook
            },
            'whndbvalue button[action=save]': {
                click: this.saveHandbookValue
            },
            
            'hndbValues toolbar [action=newValue]': {
                //itemclick: this.loadValues,
                click: this.newHadbooksValue
            },
            'hndbValues toolbar [action=delValue]': {
                //itemclick: this.loadValues,
                click: this.delHadbooksValue
            },
            

        });

        
    },
    newHadbook: function(){
        var view = Ext.widget('handbooksadd');
    },  
    saveHandbook: function (button) {
        var win = button.up('window'),
            form = win.down('form'),
            values = form.getValues();
    
    if (values['HANDBOOK_ID']==0){
    /*создаю*/
           Ext.Ajax.request({
            url: 'index.php?r=handbooks/HandbooksCreate', 
            params: { 
                HANDBOOK: values['HANDBOOK'],
                H_GROUP_ID:[values['H_GROUP_ID']],
            },
            headers: {
                'Accept': 'application/json'
            },
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                console.log(record);
            }
        }); 
     /*редактирую*/
    }else{
           Ext.Ajax.request({
            url: 'index.php?r=handbooks/HandbooksEdit', 
            params: { 
                HANDBOOK_ID: values['HANDBOOK_ID'],
                HANDBOOK: values['HANDBOOK'],
                H_GROUP_ID:[values['H_GROUP_ID']],
            },
            headers: {
                'Accept': 'application/json'
            },
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                console.log(record);
            }
        }); 
      }  
        
        this.getStore('Handbooks.Handbooks').reload();
        win.close();


    },        
    loadValues: function(grid, record) {
        /*****************************/
        ///ПЕРЕСМОТРЕТЬ ЭТОТ МЕТОД
        /*****************************/
        var params = {'handbook_id':record.get('ID')};
        
        this.getStore('Handbooks.HandbooksValues').getProxy().extraParams = params;
        //load
        this.getStore('Handbooks.HandbooksValues').reload();
    },
     editHadbooks: function() {
          var hndb_id = 0;
          var grid = this.getHndb();
          if (grid.getSelectionModel().hasSelection()) {
            var row = grid.getSelectionModel().getSelection()[0];
            hndb_id = row.get('ID')
         }
            var view = Ext.widget('handbooksadd');
            var form =  view.down('form').getForm();
            form.findField("HANDBOOK_ID").setValue(hndb_id);  
            form.findField("HANDBOOK").setValue(row.get('HANDBOOK'));  
            form.findField("H_GROUP_ID").setValue(row.get('H_GROUP_ID'));  
    },        

    newHadbooksValue:function(){
          var hndb_id = 0;
          var grid = this.getHndb();
          if (grid.getSelectionModel().hasSelection()) {
            var row = grid.getSelectionModel().getSelection()[0];
            hndb_id = row.get('ID')
         }
         
            if (hndb_id!==0){
                var view = Ext.widget('whndbvalue');
                var form =  view.down('form').getForm();
                form.findField("HANDBOOK_ID").setValue(hndb_id);  
            }else{
                Ext.MessageBox.alert('Внимание!', 'Необходимо выбрать справочник, пежде чем созадть значение по нему!');
            }  

        
         
  
    },
     editHadbooksValue:function(){
       
         var value_id = 0;
          var HndbValueGrid = this.getHndbValues();
          if (HndbValueGrid.getSelectionModel().hasSelection()) {
            var hndbValRow = HndbValueGrid.getSelectionModel().getSelection()[0];
               value_id = hndbValRow.get('ID');
         }
            var view = Ext.widget('whndbvalue');
            var form =  view.down('form').getForm();
            form.findField("VALUE_ID").setValue(value_id);  
            form.findField("H_VALUE").setValue(hndbValRow.get('VARCHAR_VALUE'));  
            
    },  
    saveHandbookValue: function (button) {
       console.log('saveHandbookValue');
        var win = button.up('window'),
            form = win.down('form'),
            values = form.getValues();
    
    if (values['VALUE_ID']==0){
    /*создаю*/
           Ext.Ajax.request({
            url: 'index.php?r=handbooks/HandbooksValueCreate', 
            params: { 
                HANDBOOK_ID: values['HANDBOOK_ID'],
                H_VALUE: values['H_VALUE'],
            },
            headers: {
                'Accept': 'application/json'
            },
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                console.log(record);
            }
        }); 
     /*редактирую*/
    }else{
           Ext.Ajax.request({
            url: 'index.php?r=handbooks/HandbooksValueEdit', 
            params: { 
                VALUE_ID: values['VALUE_ID'],
                H_VALUE: values['H_VALUE'],
                
            },
            headers: {
                'Accept': 'application/json'
            },
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                console.log(record);
            }
        }); 
      }  
        
        this.getStore('Handbooks.HandbooksValues').reload();
        win.close();


    },          
            
    delHadbooksValue:function(){
         var value_id = 0;
          var HndbValueGrid = this.getHndbValues();
          if (HndbValueGrid.getSelectionModel().hasSelection()) {
              var hndbValRow = HndbValueGrid.getSelectionModel().getSelection()[0];
               value_id = hndbValRow.get('ID');
         }
         
            if (value_id!==0){
                
                 Ext.MessageBox.confirm('Подтверждение', 'Точно удаляем?', function(btn){
                    if(btn == 'yes' ){
                        Ext.Ajax.request({
                            url: 'index.php?r=handbooks/HandbooksValueDelete', 
                            params: { 
                                VALUE_ID: value_id,

                            },
                            headers: {
                                'Accept': 'application/json'
                            },
                            success: function(response){
                                // responseText should be in json format
                                record = response.responseText;
                                console.log(record);
                            }
                        }); 
                       Ext.getStore('Handbooks.HandbooksValues').reload();     
                    }
                });
            }else{
                Ext.MessageBox.alert('Внимание!', 'Необходимо выбрать запись для удаления!');
            }
         
         console.log(' delete id '+ value_id);
    },        
             
     
    
    
});