Ext.define('PYA.store.Lots.LotStatus', {
    extend: 'Ext.data.Store',
    // model: 'PYA.model.Address.Street',
     fields: ['ID', 'VARCHAR_VALUE'],
     autoLoad: true,
       //
       pageSize: 25,
       
    proxy: {
        type: 'ajax',
         api: {
                  read:'index.php?r=lot/lotstatus'
            },
        reader: {
            type: 'json',
            root: 'vls',
            successProperty: 'success'
        },
        
    }
});