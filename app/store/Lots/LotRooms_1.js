Ext.define('PYA.store.Lots.LotRooms_1', {
    extend: 'Ext.data.Store',
     model: 'PYA.model.LotRooms_1',
     autoLoad: false,
      
    proxy: {
        type: 'ajax',
         api: {
                //read: 'data/LotRooms.json'
                 read:'index.php?r=lot/LotCard'
                // update: 'data/updateUsers.json'
            },
        reader: {
            type: 'json',
            root: 'ROOMS',
            successProperty: 'success'
        },
        extraParams : {
            LOT_ID: 0,
        }
    }
});