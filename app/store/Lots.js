Ext.define('PYA.store.Lots', {
    extend: 'Ext.data.Store',
     model: 'PYA.model.Lot',
     autoLoad: true,
       //
       //pageSize: 10, 
    proxy: {
        type: 'ajax',
         api: {
                //read: 'data/lots.json'
                    read:'index.php?r=lot/lotlist'
                // update: 'data/updateUsers.json'
            },
        reader: {
            type: 'json',
            root: 'lots',
            successProperty: 'success'
        }
    }
});