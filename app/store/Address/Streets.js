Ext.define('PYA.store.Address.Streets', {
    extend: 'Ext.data.Store',
     model: 'PYA.model.Address.Street',
     autoLoad: true,
       //
       pageSize: 25,
       
    proxy: {
        type: 'ajax',
         api: {
                  read:'index.php?r=address/liststreets'
            },
        reader: {
            type: 'json',
            root: 'vls',
            successProperty: 'success'
        },
        extraParams : {
            handbook_id: 1,
        }
    }
});