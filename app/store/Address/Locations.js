Ext.define('PYA.store.Address.Locations', {
    extend: 'Ext.data.Store',
    // model: 'PYA.model.Address.Street',
     fields: ['ID', 'VARCHAR_VALUE'],
     autoLoad: true,
       //
       pageSize: 25,
       
    proxy: {
        type: 'ajax',
         api: {
                  read:'index.php?r=address/listLocations'
            },
        reader: {
            type: 'json',
            root: 'vls',
            successProperty: 'success'
        },
        
    }
});