Ext.define('PYA.store.Address.MapAddresses', {
    extend: 'Ext.data.Store',
    // model: 'PYA.model.Address.Street',
     fields: ['geoCodeAddr', 'title'],
     autoLoad: true,
     data  : [
                
                {geoCodeAddr: 'Россия, Москва, Тверская 17', title: 'Тверская 17'},
                {geoCodeAddr: 'Россия, Москва, Тверская 16',    title: 'Тверская 16'},
            ]
    
       //
//       pageSize: 25,
//       
//    proxy: {
//        type: 'ajax',
//         api: {
//                  read:'index.php?r=address/listDirectons'
//            },
//        reader: {
//            type: 'json',
//            root: 'vls',
//            successProperty: 'success'
//        },
//        
//    }
});