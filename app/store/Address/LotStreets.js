/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('PYA.store.Address.LotStreets', {
    extend: 'Ext.data.Store',
     model: 'PYA.model.Address.LotStreet',
     autoLoad: true,
     autoSync: true,
      //
      pageSize: 25, 
    proxy: {
        type: 'ajax',
         api: {
                //read: 'data/lots.json'
                   read:'index.php?r=address/liststreetsoflot',
                   update: 'index.php?r=address/addstreettolot',
                   create: 'index.php?r=handbooks/handbookscreate',
            },
        reader: {
            type: 'json',
            root: 'lotstreets',
            successProperty: 'success'
        },
        extraParams : {
            LOT_ID: 6,
        }
    }
});