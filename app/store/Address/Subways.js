Ext.define('PYA.store.Address.Subways', {
    extend: 'Ext.data.Store',
    // model: 'PYA.model.Address.Street',
     fields: ['ID', 'VARCHAR_VALUE'],
     autoLoad: true,
       //
       pageSize: 25,
       
    proxy: {
        type: 'ajax',
         api: {
                  read:'index.php?r=address/listsubway'
            },
        reader: {
            type: 'json',
            root: 'vls',
            successProperty: 'success'
        },
        
    }
});