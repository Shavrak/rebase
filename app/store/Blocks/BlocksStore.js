Ext.define('PYA.store.Blocks.BlocksStore', {
    extend: 'Ext.data.Store',
     model: 'PYA.model.Blocks.BlockModel',
     autoLoad: true,
      
    proxy: {
        type: 'ajax',
         api: {
                //read: 'data/LotRooms.json'
                 read:'index.php?r=blocks/list'
                // update: 'data/updateUsers.json'
            },
        reader: {
            type: 'json',
            root: 'blocks',
            successProperty: 'success'
        },
        extraParams : {
            LOT_ID: 0,
        }
    }
});