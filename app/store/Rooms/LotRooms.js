Ext.define('PYA.store.Rooms.LotRooms', {
    extend: 'Ext.data.Store',
     model: 'PYA.model.LotRooms',
     autoLoad: true,
      
    proxy: {
        type: 'ajax',
         api: {
                //read: 'data/LotRooms.json'
                 read:'index.php?r=room/roomslist'
                // update: 'data/updateUsers.json'
            },
        reader: {
            type: 'json',
            root: 'rooms',
            successProperty: 'success'
        },
        extraParams : {
            LOT_ID: 0,
        }
    }
});