Ext.define('PYA.store.Handbooks.Elevators', {
    extend: 'Ext.data.Store',
     fields: ['ID', 'VARCHAR_VALUE'],
     autoLoad: true,
       //
       pageSize: 25,
       
    proxy: {
        type: 'ajax',
         api: {
                  read:'index.php?r=handbooks/handbooksvalue'
            },
        reader: {
            type: 'json',
            root: 'vls',
            successProperty: 'success'
        },
        extraParams : {
            handbook_id: 112,
        }
    }
});


