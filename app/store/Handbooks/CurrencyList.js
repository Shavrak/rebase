Ext.define('PYA.store.Handbooks.CurrencyList', {
    extend: 'Ext.data.Store',
     model: 'PYA.model.Handbook.HandbookValue',
     autoLoad: true,
       //
       pageSize: 25,
       
    proxy: {
        type: 'ajax',
         api: {
                  read:'index.php?r=handbooks/handbooksvalue'
            },
        reader: {
            type: 'json',
            root: 'vls',
            successProperty: 'success'
        },
        extraParams : {
            handbook_id: 102,
        }
    }
});


