Ext.define('PYA.store.Handbooks.PriceType', {
     extend: 'Ext.data.Store',
     fields: ['ID', 'TYPE_NAME'],
     autoLoad: true,
       //
       pageSize: 25,
       
    proxy: {
        type: 'ajax',
         api: {
                  read:'index.php?r=handbooks/pricetype'
            },
        reader: {
            type: 'json',
            root: 'vls',
            successProperty: 'success'
        },
        extraParams : {
            handbook_id: 110,
        }
    }
});


