Ext.define('PYA.store.Handbooks.HandbooksCategory', {
    extend: 'Ext.data.Store',
    fields: ['id', 'name'],
    data: [
        {id: '1', name: 'Общие справочники'},
        {id: '2', name: 'Специальные справочники'}
    ]
});