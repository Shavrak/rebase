Ext.define('PYA.store.Handbooks.HandbooksGroups', {
    extend: 'Ext.data.Store',
     model: 'PYA.model.Handbook.HandbookGroup',
    //autoLoad: true,
       //
       //pageSize: 10, 
    proxy: {
        type: 'ajax',
         api: {
                //read: 'data/lots.json'
                    read:'index.php?r=room/indexjson'
                // update: 'data/updateUsers.json'
            },
        reader: {
            type: 'json',
            root: 'grps',
            successProperty: 'success'
        }
    }
});