Ext.define('PYA.store.Handbooks.Handbooks', {
    extend: 'Ext.data.Store',
     model: 'PYA.model.Handbook.Handbook',
     autoLoad: true,
     autoSync: true,
      //
      pageSize: 25, 
    proxy: {
        type: 'ajax',
         api: {
                //read: 'data/lots.json'
                   read:'index.php?r=handbooks/handbookslist',
            },
        reader: {
            type: 'json',
            root: 'hndb',
            successProperty: 'success'
        }
    }
});