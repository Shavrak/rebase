Ext.define('PYA.store.Users.WholeUserList', {
    extend: 'Ext.data.Store',
    // model: 'PYA.model.Address.Street',
     fields: ['ID', 'NAME'],
     autoLoad: true,
       //
       pageSize: 25,
       
    proxy: {
        type: 'ajax',
         api: {
                  read:'index.php?r=users/wholeuserlist'
            },
        reader: {
            type: 'json',
            root: 'users',
            successProperty: 'success'
        },
        
    }
});