Ext.define('PYA.store.Elevators.Elevator', {
    extend: 'Ext.data.Store',
     model: 'PYA.model.Elevator.Elevator',
     autoLoad: true,
     autoSync: true,
      //
      pageSize: 25, 
    proxy: {
        type: 'ajax',
         api: {
                //read: 'data/lots.json'
                   read:'index.php?r=elevator/elevatorslist',
//                   update: 'index.php?r=handbooks/handbooksedit',
//                   create: 'index.php?r=handbooks/handbookscreate',
            },
        reader: {
            type: 'json',
            root: 'elevator',
            successProperty: 'success'
        }
    }
});