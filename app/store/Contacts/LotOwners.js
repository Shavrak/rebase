Ext.define('PYA.store.Contacts.LotOwners', {
    extend: 'Ext.data.Store',
    // model: 'PYA.model.Address.Street',
     fields: ['ID', 'NAME','PHONE','COMPANY_NAME','COMPANY_DESC'],
     autoLoad: true,
       //
       pageSize: 25,
       
    proxy: {
        type: 'ajax',
         api: {
                  read:'index.php?r=contacts/ListOwnersOfLot',
                 
            },
        reader: {
            type: 'json',
            root: 'own',
            successProperty: 'success'
        },
        extraParams : {
            LOT_ID: 6,
        }
        
    }
});