Ext.define('PYA.store.Contacts.Owners', {
    extend: 'Ext.data.Store',
    // model: 'PYA.model.Address.Street',
     fields: ['ID', 'NAME','PHONE','COMPANY_NAME','COMPANY_DESC'],
     autoLoad: true,
       //
       pageSize: 25,
       
    proxy: {
        type: 'ajax',
         api: {
                  read:'index.php?r=contacts/OwnerList'
            },
        reader: {
            type: 'json',
            root: 'own',
            successProperty: 'success'
        },
        
    }
});