Ext.define('PYA.store.Contacts.Companies', {
    extend: 'Ext.data.Store',
    // model: 'PYA.model.Address.Street',
     fields: ['ID', 'NAME','PHONE','DESCRIPTION','ADDRESS'],
     autoLoad: true,
       //
       pageSize: 25,
       
    proxy: {
        type: 'ajax',
         api: {
                  read:'index.php?r=contacts/companieslist'
            },
        reader: {
            type: 'json',
            root: 'company',
            successProperty: 'success'
        },
        
    }
});