Ext.define('PYA.store.ComonInfo', {
    extend: 'Ext.data.Store',
     model: 'PYA.model.Lot',
     autoLoad: true,
      
    proxy: {
        type: 'ajax',
         api: {
                read: 'data/ComonInfo.json'
                // update: 'data/updateUsers.json'
            },
        reader: {
            type: 'json',
            root: 'lots',
            successProperty: 'success'
        }
    }
});