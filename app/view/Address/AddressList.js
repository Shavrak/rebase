Ext.define('PYA.view.Address.AddressList' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.addresslist',

    // title: 'Лоты',
    //region: 'center',
    store: 'Address.LotStreets',
    height:241,
    
    initComponent: function() {
        this.addEvents(
            /**
             * @event rowdblclick
             * Fires when a row is double clicked
             * @param {FeedViewer.FeedGrid} this
             * @param {Ext.data.Model} model
             */
            'rowdblclick',
            /**
             * @event select
             * Fires when a grid row is selected
             * @param {FeedViewer.FeedGrid} this
             * @param {Ext.data.Model} model
             */
            'select'
        );
//        this.store = {
//            fields: ['name', 'email'],
//            data  : [
//                {name: 'Ed',    email: 'ed@sencha.com'},
//                {name: 'Tommy', email: 'tommy@sencha.com'}
//            ]
//        };

        this.columns = [
            {header: 'ID',  dataIndex: 'ID',   width:35},
            {header: 'Адрес',  dataIndex: 'ADDRESS',  flex: 1},
            
        ];
        
       this.tbar =  [{
                    text: 'Удалить',
                    iconCls: 'add16',
                    action:'dellAddres'
                }];
            
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            pageSize: 10,
            displayInfo: true,
            displayMsg: 'Показано записей {0} - {1} из {2}',
            emptyMsg: "Нет записей",
            });
        
        this.callParent(arguments);
        this.on('selectionchange', this.onSelect, this);
    },
    
    onRowDblClick: function(view, record, item, index, e) {
        console.log('I onRowDblClick ');     
        
        //this.fireEvent('rowdblclick', this, this.store.getAt(index));
        Ext.widget('LotLayout');
    },
    /**
     * React to a grid item being selected
     * @private
     * @param {Ext.model.Selection} model The selection model
     * @param {Array} selections An array of selections
     */
    onSelect: function(model, selections){
        var selected = selections[0];
        if (selected) {
            this.fireEvent('select', this, selected);
        }
    },
    

               
    
});