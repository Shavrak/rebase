Ext.define('PYA.view.Address.AddressLandmark', {
    extend: 'Ext.window.Window',
    alias: 'widget.addresseslandmark',
    title: 'Адреса и ориентир',
    layout: 'fit',
    autoShow: true,
    
    //store:'Handbooks.HandbooksCategory',
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                name:'addresseslandmark',
                width:900,
                //height:280,
                height:500,
                 layout:'hbox',
                bodyPadding:  '5 5 5 5',
                
                defaults: { 
                    xtype: 'container', 
                    layout: { 
                        type: 'hbox', 
                        align: 'middle'     
                    } 
                },
                items: [{
                         xtype: 'fieldset',
                          title: 'Адрес',
                        layout: 'vbox',
                        align: 'stretch',
                        bodyPadding:  '5 5 0',
                        items: [
                          {
                            xtype: 'fieldcontainer',
                            fieldLabel: 'Адрес',
                            layout: 'hbox',
                            labelWidth: 50,
                            combineErrors: true,
                            defaults: {
                                hideLabel: 'true'
                            },
                            items: [{

                                xtype: 'combobox',
                                name: 'address',
                                fieldLabel: 'Адрес',
                                store: this.store,
                                queryMode: 'local',
                                displayField: 'name',
                                valueField: 'id',
                                flex: 2,
                                emptyText: 'Адрес',
                                allowBlank: false


                            }, {
                                xtype: 'button',
                                text: 'Выбрать',
                                margins: '0 0 0 6',
                            }]
                        },


                            {xtype:'addresslist',
                            width:400},
                            {
                            height:200, 
                            width:400,
                            xtype: 'gmappanel',
                            fieldLabel: 'Карта',
                            center: {
                                geoCodeAddr: '4 Yawkey Way, Boston, MA, 02215-3409, USA',
                                marker: {title: 'Fenway Park'}
                            },
                            markers: [{
                                lat: 42.339641,
                                lng: -71.094224,
                                title: 'Boston Museum of Fine Arts',
                                listeners: {
                                    click: function(e){
                                        Ext.Msg.alert('It\'s fine', 'and it\'s art.');
                                    }
                                }
                            },{
                                lat: 42.339419,
                                lng: -71.09077,
                                title: 'Northeastern University'
                            }]
                        }
                        ]
                },{
                        
                //second column
                layout: 'vbox',
                xtype: 'fieldset',
                title: 'Ориентиры',
                bodyPadding:  '5 5 5 5',
                   items: [
                       Ext.create('Ext.ux.form.field.BoxSelect', {
                    //fieldLabel: 'Метро',
                      displayField: 'name',
                      valueField: 'id',
                      width: 400,
                      //labelWidth: 130,
                      name:'subway_id',
                      emptyText: 'Метро',
                      store: Ext.create('Ext.data.Store', {
                              fields: ['id', 'name'],
                              data : [
                                  {"id":"1", "name":"Тверская"},
                                  {"id":"2", "name":"Пушкинская"},
                                  {"id":"3", "name":"Чеховская"}
                                  //...
                              ]
                          }),
                      queryMode: 'local'
                  }),
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        fieldLabel: 'От метро',
                        combineErrors: false,
                        defaults: {
                            hideLabel: true
                        },
                        items: [
                           {
                               name : 'sbw_min_walk',
                               xtype: 'numberfield',
                               width: 48,
                               value:0,
                               margin: '0 5 0 5',   
                               allowBlank: false
                           },
                           {
                               xtype: 'displayfield',
                               value: 'минут пешком'
                           },
                           {
                               name : 'sbw_min_auto',
                               xtype: 'numberfield',
                               width: 48,
                               value:0,
                               margin: '0 5 0 5',
                               allowBlank: false
                           },
                           {
                               xtype: 'displayfield',
                               value: 'минут авто'
                           }
                        ]
                    },
                    Ext.create('Ext.ux.form.field.BoxSelect', {
                    //fieldLabel: 'ЖД станция',
                      displayField: 'name',
                      valueField: 'id',
                      width: 400,
                      //labelWidth: 130,
                      name:'railway_id',
                      emptyText: 'ЖД станция',
                      store: Ext.create('Ext.data.Store', {
                              fields: ['id', 'name'],
                              data : [
                                  {"id":"1", "name":"Сосенки"},
                                  {"id":"2", "name":"Калачаевская"},
                                  {"id":"3", "name":"Лось"}
                                  //...
                              ]
                          }),
                      queryMode: 'local'
                  }),
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        fieldLabel: 'От станции',
                        combineErrors: false,
                        defaults: {
                            hideLabel: true
                        },
                        items: [
                           {
                               name : 'railway_min_walk',
                               xtype: 'numberfield',
                               width: 48,
                               margin: '0 5 0 5',
                               value:0,
                               allowBlank: false
                           },
                           {
                               xtype: 'displayfield',
                               value: 'минут пешком'
                           },
                           {
                               name : 'railway_min_auto',
                               xtype: 'numberfield',
                               width: 48,
                               value:0,
                               margin: '0 5 0 5',
                               allowBlank: false
                           },
                           {
                               xtype: 'displayfield',
                               value: 'минут авто'
                           }
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                       // fieldLabel: 'Деловой район',
                        combineErrors: false,
                      
                        items: [
                           {  
                            xtype: 'combobox',
                            name: 'district',
                            //fieldLabel: 'Деловой район',
                            store: this.store,
                            queryMode: 'local',
                            displayField: 'name',
                            valueField: 'id',
                            flex: 2,
                            emptyText: 'Деловой район',
                            allowBlank: false,
                            margin: '0 5 0 5',
                        
                           },
                           {
                            xtype: 'combobox',
                            name: 'direction',
                          //  fieldLabel: 'Направление',
                            store: this.store,
                            queryMode: 'local',
                            displayField: 'name',
                            valueField: 'id',
                            flex: 2,
                            emptyText: 'Направление',
                            allowBlank: false,
                            margin: '0 5 0 5',
                           },
                           
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                       // fieldLabel: 'Расположение',
                        combineErrors: false,
                       
                        items: [
                           {  
                            xtype: 'combobox',
                            name: 'location',
                            
                            store: this.store,
                            queryMode: 'local',
                            displayField: 'name',
                            valueField: 'id',
                            flex: 2,
                            emptyText: 'Расположение',
                            allowBlank: false,
                            margin: '0 5 0 5',
                        
                           },
                           {
                            xtype: 'checkbox',
                            name: 'address',
                            fieldLabel: 'За МКАД',
                            text:'За МКАД',
                            flex: 2,
                            margin: '0 5 0 5',
                           },
                           
                        ]
                    },
                    {
                        xtype: 'htmleditor',
                        name: 'description_loaction',
                      //  fieldLabel: 'Описание',
                        emptyText: 'Описание расположение',
                        margin: '5 5 5 5',
                        height: 270,
                        width : 350,
                        anchor: '100%'
                    }
                   ]  
                }//end second column
            ]     
            },
            
        ];
        this.buttons = [

            {
                text: 'Выбрать',
                handler: this.choseHand   
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    },
    choseHand:function(){
     var win    = this.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
      console.log('click choseHand '+values['lot_id']);
    }
});