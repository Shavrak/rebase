Ext.define('PYA.view.Address.Addresses', {
    extend: 'Ext.window.Window',
    alias: 'widget.addresses',
    title: 'Адреса',
    layout: 'fit',
    autoShow: true,
    store: 'Address.Streets',
    id:9,
    
    //store:'Handbooks.HandbooksCategory',
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                name:'addresses',
                width:500,
                //height:280,
                height:500,
                layout: 'form',
                bodyPadding:  '5 5 0',
                
                items: [
                    {
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Адрес',
                    layout: 'hbox',
                    labelWidth: 50,
                    combineErrors: true,
                    defaults: {
                        hideLabel: 'true'
                    },
                    items: [
                        {
                    
                        xtype: 'hidden',
                        name: 'lot_id',
                        //value:'1',
                        allowBlank: false
                    },
                        {
                    
                        xtype: 'combo',
                        name: 'address',
                        fieldLabel: 'Адрес',
                        store: this.store,
                        //queryMode: 'remote',
                        displayField: 'ADDRESS',
                        valueField: 'ID',
                        //typeAhead: false,
                        //hideTrigger:true,
                        flex: 2,
                            

                        listConfig: {
                               loadingText: 'Поиск...',

                               // Custom rendering template for each item
                               getInnerTpl: function() {
                                   return '{ADDRESS}';
                               }
                           },
                           pageSize: 10
                    
                        
                    }, {
                        xtype: 'button',
                        text: 'Выбрать',
                        //action:'addStreet',
                        handler: this.choseHand,
                        margins: '0 0 0 6',
                    }]
                },
                    
                    
                    {xtype:'addresslist'},
                    {
                    height:200,    
                    xtype: 'gmappanel',
                    fieldLabel: 'Карта',
                    zoomLevel: 10,
    		    gmapType: 'map',
                    //gmapType: 'panorama',
                    //mt:'m',
                    center: {
                        geoCodeAddr: 'Россия, Москва, Тверская 16 ',
                        marker: {title: '1905 года'}
                    },
                    markers: 'Address.MapAddresses',
//                    [
//                        {geoCodeAddr: 'Россия, Москва, Тверская 16 ',
//                         title: 'Тверская 16',   
//                        },
//                        {
//                        lat: 42.339641,
//                        lng: -71.094224,
//                        title: 'Boston Museum of Fine Arts',
//                        listeners: {
//                            click: function(e){
//                                Ext.Msg.alert('It\'s fine', 'and it\'s art.');
//                            }
//                        }
//                    },{
//                        lat: 42.339419,
//                        lng: -71.09077,
//                        title: 'Northeastern University'
//                    }
                   //]
                }
                ]
            },
            
        ];
        this.buttons = [

            {
                text: 'Выбрать',
                action: 'chose',
                
                
                //handler: this.choseHand    
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];
        
        this.callParent(arguments);
        
    },
    listeners: {
        afterrender: function(){
        this.loadGrid();
        },
    },
    
    choseHand:function(){
     var win    = this.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
       
       
         Ext.Ajax.request({
            url: 'index.php?r=address/AddStreetToLot', 
            params: { 
                LOT_ID: values['lot_id'],
                ADDRESS_ID : values['address'],
            },
            headers: {
                'Accept': 'application/json'
            },
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                
         
          var params = {'LOT_ID':values['lot_id'],
                       'limit':	25,
                       'page':	1,
                       'start':0
                    };
        
        var GreedStreet = Ext.widget('addresslist');
        GreedStreet.getStore('Address.LotStreets').getProxy().extraParams = params;
        GreedStreet.getStore('Address.LotStreets').reload();
               
            }
            
        }); 
    },
    loadGrid:function(){
        
       
        var  form   = this.down('form'),
        record = form.getRecord(),
        values = form.getValues();

        
        var params = {'LOT_ID':values['lot_id'],
                           'limit': 25,
                           'page':1,
                           'start':0
                    };
        
        var GreedStreet = Ext.widget('addresslist');
        GreedStreet.getStore('Address.LotStreets').getProxy().extraParams = params;
        //load
        GreedStreet.getStore('Address.LotStreets').reload();
        
        
    },
     addresDone:function(){
        action:'chose';
        
     }
    
    
});