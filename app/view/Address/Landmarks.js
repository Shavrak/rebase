Ext.define('PYA.view.Address.Landmarks', {
    extend: 'Ext.window.Window',
    alias: 'widget.landmarks',
    title: 'Ориентиры',
    layout: 'hbox',
    autoShow: true,
    id:10,
    
    //store:'Handbooks.HandbooksCategory',
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                name:'landmarks',
                width:600,
                height:350,
                layout: 'form',
                bodyPadding:  '5 5 0',
                items: [
                    {
                    
                        xtype: 'hidden',
                        name: 'lot_id',
                        // value:'1',
                        allowBlank: false
                    },  
                    Ext.create('Ext.ux.form.field.BoxSelect', {
                     fieldLabel: 'Метро',
                     displayField: 'VARCHAR_VALUE',
                      valueField: 'ID',
                      queryMode: 'remote',
                      triggerOnClick: false,
                      name:'subway_id',
                      emptyText: 'Метро',
                      store:'Address.Subways',
                      listConfig: {
                               loadingText: 'Поиск...',

                               // Custom rendering template for each item
                               getInnerTpl: function() {
                                   return '{VARCHAR_VALUE}';
                               }
                           },
                           pageSize: 10
                  }),
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        fieldLabel: 'От метро',
                        combineErrors: false,
                        defaults: {
                            hideLabel: true
                        },
                        items: [
                           {
                               name : 'sbw_min_walk',
                               xtype: 'numberfield',
                               width: 48,
                               value:0,
                               margin: '0 5 0 5',   
                               allowBlank: false
                           },
                           {
                               xtype: 'displayfield',
                               value: 'минут пешком'
                           },
                           {
                               name : 'sbw_min_auto',
                               xtype: 'numberfield',
                               width: 48,
                               value:0,
                               margin: '0 5 0 5',
                               allowBlank: false
                           },
                           {
                               xtype: 'displayfield',
                               value: 'минут авто'
                           }
                        ]
                    },
                    Ext.create('Ext.ux.form.field.BoxSelect', {
                    fieldLabel: 'ЖД станция',
                      displayField: 'VARCHAR_VALUE',
                      valueField: 'ID',
                      //width: 300,
                      //labelWidth: 130,
                      name:'railway_id',
                      emptyText: 'ЖД станция',
                      store:'Address.Railways',
                      listConfig: {
                               loadingText: 'Поиск...',

                               // Custom rendering template for each item
                               getInnerTpl: function() {
                                   return '{VARCHAR_VALUE}';
                               }
                           },
                           pageSize: 10
                  }),
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        fieldLabel: 'От станции',
                        combineErrors: false,
                        defaults: {
                            hideLabel: true
                        },
                        items: [
                           {
                               name : 'railway_min_walk',
                               xtype: 'numberfield',
                               width: 48,
                               margin: '0 5 0 5',
                               value:0,
                               allowBlank: false
                           },
                           {
                               xtype: 'displayfield',
                               value: 'минут пешком'
                           },
                           {
                               name : 'railway_min_auto',
                               xtype: 'numberfield',
                               width: 48,
                               value:0,
                               margin: '0 5 0 5',
                               allowBlank: false
                           },
                           {
                               xtype: 'displayfield',
                               value: 'минут авто'
                           }
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        fieldLabel: 'Деловой район',
                        combineErrors: false,
                      
                        items: [
                           {  
                            xtype: 'combobox',
                            name: 'district',
                            //queryMode: 'local',
                            displayField: 'VARCHAR_VALUE',
                            valueField: 'ID',
                            flex: 2,
                            emptyText: 'Деловой район',
                            allowBlank: false,
                            margin: '0 5 0 5',
                            store:'Address.Districts',
                            listConfig: {
                                     loadingText: 'Поиск...',

                                     // Custom rendering template for each item
                                     getInnerTpl: function() {
                                         return '{VARCHAR_VALUE}';
                                     }
                                 },
                             pageSize: 10
                        
                           },
                           {
                            xtype: 'combobox',
                            name: 'direction',
                            fieldLabel: 'Направление',
                            //queryMode: 'local',
                            displayField: 'VARCHAR_VALUE',
                            valueField: 'ID',
                            flex: 2,
                            emptyText: 'Направление',
                            allowBlank: false,
                            margin: '0 5 0 5',
                            store:'Address.Directons',
                            listConfig: {
                                     loadingText: 'Поиск...',

                                     // Custom rendering template for each item
                                     getInnerTpl: function() {
                                         return '{VARCHAR_VALUE}';
                                     }
                                 },
                             pageSize: 10
                           },
                           
                        ]
                    },
                    {
                        xtype: 'fieldcontainer',
                        layout: 'hbox',
                        fieldLabel: 'Расположение',
                        combineErrors: false,
                       
                        items: [
                           {  
                            xtype: 'combobox',
                            name: 'location',
                           // queryMode: 'local',
                            displayField: 'VARCHAR_VALUE',
                            valueField: 'ID',
                            flex: 2,
                            emptyText: 'Расположение',
                            allowBlank: false,
                            margin: '0 5 0 5',
                            store:'Address.Locations',
                            listConfig: {
                                     loadingText: 'Поиск...',

                                     // Custom rendering template for each item
                                     getInnerTpl: function() {
                                         return '{VARCHAR_VALUE}';
                                     }
                                 },
                             pageSize: 10
                        
                           },
                           {
                            xtype: 'checkbox',
                            name: 'za_mkad',
                            fieldLabel: 'За МКАД',
                            text:'За МКАД',
                            flex: 2,
                            margin: '0 5 0 5',
                           },
                           
                        ]
                    },
                    {
                        xtype: 'htmleditor',
                        name: 'description_loaction',
                        fieldLabel: 'Описание',
                        emptyText: 'Описание расположение',
                        margin: '5 5 5 5',
                        height: 170,
                        anchor: '100%'
                    }
                ]
            },
            
        ];
        this.buttons = [

            {
                text: 'Выбрать',
                action:'chose'
                
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    },
 
});