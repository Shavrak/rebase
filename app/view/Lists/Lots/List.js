Ext.define('PYA.view.Lists.Lots.List' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridlots',

    // title: 'Лоты',
    region: 'center',
    store: 'Lots',
    
    initComponent: function() {
        this.addEvents(
            /**
             * @event rowdblclick
             * Fires when a row is double clicked
             * @param {FeedViewer.FeedGrid} this
             * @param {Ext.data.Model} model
             */
            'rowdblclick',
            /**
             * @event select
             * Fires when a grid row is selected
             * @param {FeedViewer.FeedGrid} this
             * @param {Ext.data.Model} model
             */
            'select'
        );
//        this.store = {
//            fields: ['name', 'email'],
//            data  : [
//                {name: 'Ed',    email: 'ed@sencha.com'},
//                {name: 'Tommy', email: 'tommy@sencha.com'}
//            ]
//        };

        this.columns = [
            {header: 'ID',  dataIndex: 'ID',   width:35},
            {header: 'Фото', dataIndex: 'NAME', width:160,  renderer:this.renderPICS},
            {header: 'Комплекс', dataIndex: 'LOT',  width:250,  renderer:this.renderObject},
            //{header: 'Адрес', dataIndex: 'ADDRESS', flex: 1},
            
            {header: 'Ставка', dataIndex: 'PRICE_RUB',  width:90, renderer:this.renderRENT_PRICES},
            {header: 'НДС', dataIndex: 'RENT_WITH_NDS',  width:50},
            {header: 'Площадь', dataIndex: 'AREA',  width:60},
            //{header: 'Метро', dataIndex: 'METRO_SATATION',  width:75},
           // {header: 'Направление', dataIndex: 'DIRECTON',  width:100},
            {header: 'Класс', dataIndex: 'BUILDING_CLASS',  width:50},
            {header: 'Готовность', dataIndex: 'READY_VALUE',  width:100},
            {header: 'Дата',  dataIndex: 'LAST_DATE',  width:100},
            
        ];
        
        this.tbar =  [{
                    text: 'Добавить здание',
                    iconCls: 'icon-save',
                    action:'addbuilding'
                },'-',{
                    text: 'Листовка',
                    iconCls: 'icon-booklet',
                    action:'booklet'
                }];
            
            
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            pageSize: 10,
            displayInfo: true,
            displayMsg: 'Показано записей {0} - {1} из {2}',
            emptyMsg: "Нет записей",
            });
        
        this.callParent(arguments);
        this.on('selectionchange', this.onSelect, this);
    },
    
    onRowDblClick: function(view, record, item, index, e) {
        console.log('I onRowDblClick ');     
        
        //this.fireEvent('rowdblclick', this, this.store.getAt(index));
        Ext.widget('LotLayout');
    },
    /**
     * React to a grid item being selected
     * @private
     * @param {Ext.model.Selection} model The selection model
     * @param {Array} selections An array of selections
     */
    onSelect: function(model, selections){
        var selected = selections[0];
        if (selected) {
            this.fireEvent('select', this, selected);
        }
    },
    
    renderObject: function(value, p, record){
        
       
         return Ext.String.format(
               '<p> <b>Лот </b>:       {1}</p> \n\
                <p><b> Название </b>:  {2} </p> \n\
                <p><b> Адрес </b>:     {3} </p> \n\
                <p><b> Метро  </b>:    {4} </p>\n\
                <p><b> Направление  </b>: {5} </p>\n\
                <p><b> Диапазон площадей </b>: {6} </p>',
            value, record.data.LOT,
                   record.data.NAME,  
                   record.data.ADDRESS, 
                   record.data.METRO_SATATION,
                   record.data.DIRECTON,
                   record.data.ROOMS_AREA_DIAPAZON
    )
    },
    renderRENT_PRICES: function(value, p, record){
         return Ext.String.format(
               '<p><b> {1}</b> RUB</p> \n\
                <p><b> {2} </b> USD</p> \n\
                <p><b> {3} </b> EUR</p> ',
            value, record.data.PRICE_RUB,
                   record.data.PRICE_USD,  
                   record.data.PRICE_EUR
                   
    )}
    ,
    renderPICS: function(value, p, record){
         return Ext.String.format(
               '<img src="{1}" width="150">',
            value, record.data.LOT_PIC
    )}
    
    
               
    
});