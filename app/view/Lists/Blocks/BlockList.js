Ext.define('PYA.view.Lists.Blocks.BlockList' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.blockslist',
    // title: 'Лоты',
    region: 'center',
    store: 'Blocks.BlocksStore',
    
    initComponent: function() {
        this.addEvents(
            /**
             * @event rowdblclick
             * Fires when a row is double clicked
             * @param {FeedViewer.FeedGrid} this
             * @param {Ext.data.Model} model
             */
            'rowdblclick',
            /**
             * @event select
             * Fires when a grid row is selected
             * @param {FeedViewer.FeedGrid} this
             * @param {Ext.data.Model} model
             */
            'select'
        );
//        this.store = {
//            fields: ['name', 'email'],
//            data  : [
//                {name: 'Ed',    email: 'ed@sencha.com'},
//                {name: 'Tommy', email: 'tommy@sencha.com'}
//            ]
//        };

        this.columns = [
            {header: 'ID',  dataIndex: 'ID',   width:35},
            {header: 'Дата',  dataIndex: 'LAST_DATE',  width:70},
            {header: 'Адрес', dataIndex: 'ROOM_LOT',  width:250,  renderer:this.renderAddress},
            {header: 'Отделка', dataIndex: 'FINISHING',  width:150, renderer:this.renderFinishing},
            {header: 'Этаж', dataIndex: 'FLOOR_NUMBER',  width:55},
            {header: 'Площадь', dataIndex: 'AREA',   width:75, renderer:this.renderArea },
            {header: 'Ставка', dataIndex: 'RENT_PRICES',  width:90, renderer:this.renderRENT_PRICES},
            {header: 'НДС', dataIndex: 'WITH_NDS',  width:40},
            {header: 'Наш %', dataIndex: 'OUR_PERCENT',   width:50},
            {header: 'Коммунальные', dataIndex: 'OPERATING_COSTS',  width:200,  renderer:this.renderPrice},
            {header: 'Фото', dataIndex: 'ROOM_PIC',  width:150, renderer:this.renderPICS},
            {header: 'Брокер', dataIndex: 'BROKER',  width:100},
            {header: 'Готовность', dataIndex: 'READY_DATE',  width:100},
          
        ];
        
//        this.tbar =  [{
//                    text: 'Добавить лот',
//                    iconCls: 'add16',
//                   action:'addlot'
//                }];
            
            
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            pageSize: 10,
            displayInfo: true,
            displayMsg: 'Показано записей {0} - {1} из {2}',
            emptyMsg: "Нет записей",
            });
        
        this.callParent(arguments);
        //this.on('selectionchange', this.onSelect, this);
    },
    
//    onRowDblClick: function(view, record, item, index, e) {
//        console.log('I onRowDblClick ');     
//        //this.fireEvent('rowdblclick', this, this.store.getAt(index));
//        Ext.widget('LotLayout');
//    },
    /**
     * React to a grid item being selected
     * @private
     * @param {Ext.model.Selection} model The selection model
     * @param {Array} selections An array of selections
     */
    onSelect: function(model, selections){
        var selected = selections[0];
        if (selected) {
            this.fireEvent('select', this, selected);
        }
    },
    renderFinishing: function(value, p, record){
         return Ext.String.format(
               '<p> <b>Отделка </b>: {1}</p> \n\
                <p><b> Планировка </b>: {2} </p> ',
            value, record.data.FINISHING,
                   record.data.PLANING
                   
    )},
    renderArea: function(value, p, record){
         return Ext.String.format(
               '<p> {1} кв.м. </p>',
            value, record.data.AREA
                   
                   
    )},
    renderAddress: function(value, p, record){
         return Ext.String.format(
               '<p> <b>Лот(блока) </b>:{1}</p> \n\
                <p><b> Адрес </b>:     {2} </p> \n\
                <p><b> Метро  </b>:    {3} </p>\n\
                <p><b> Направление  </b>: {4} </p>',
            value, record.data.ROOM_LOT,
                   record.data.ADDRESS, 
                   record.data.METRO_SATATION,
                   record.data.DIRECTON
       )},
   renderPrice: function(value, p, record){
         return Ext.String.format(
               '<p><b>Экспл. расходы</b>: {1}</p> \n\
                <p><b> Коммунальные </b>: {2} </p> \n\
                <p><b> Депозит </b>: {3} </p> \n\
                <p><b> Предоплата  </b>: {4} </p>\n\
                <p><b> Договор </b>: {5} </p>',
            value, record.data.OPERATING_COSTS,
                   record.data.UTILITY_BILLS,  
                   record.data.DEPOSITS, 
                   record.data.PREPAYD_VALUE,
                   record.data.CONTARCT_TYPE
    )},
    renderRENT_PRICES: function(value, p, record){
         return Ext.String.format(
               '<p><b> {1}</b> RUB</p> \n\
                <p><b> {2} </b> USD</p> \n\
                <p><b> {3} </b> EUR</p> ',
            value, record.data.RENT_PRICES_RUB,
                   record.data.RENT_PRICES_USD,  
                   record.data.RENT_PRICES_EUR
                   
    )}
    ,
    renderPICS: function(value, p, record){
         return Ext.String.format(
               '<img src="{1}" width="150">',
            value, record.data.ROOM_PIC
    )}            
               
    
});