Ext.define('PYA.view.Panels.Lots.RoomsPanel' ,{
    extend: 'Ext.panel.Panel',
    alias: 'widget.roomspanel',
    //store: 'Contacts.LotOwners',
    layout: {
            type: 'vbox',
            align: 'stretch'
        },
    initComponent: function() {

        this.items=[{
            flex: 1,
            xtype:'gridrooms',
        }]
        this.tbar =  [{
                    text: 'Добавить блок',
                    iconCls: 'icon-add',
                    action:'addRoom'
                }];
        this.callParent();
    }
});