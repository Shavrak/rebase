Ext.define('PYA.view.Panels.Lots.Infrastructure' ,{
    extend: 'Ext.panel.Panel',
    alias: 'widget.infrastructure',
    //store: 'ComonInfo',
    
    initComponent: function() {
        //this.tpl = new Ext.XTemplate('Title:{name}<br/>');
    //    this.html = this.startingMarkup;
        this.items=[{ 
            xtype:'dataview',
            store: this.store,
            itemSelector: 'active',
            html:'this',
            tpl: new Ext.XTemplate(
                      '<tpl for=".">',
                        
                          '<br><b>Метро :</b>{METRO_SATATION}',
                          '<br><b>Направление :</b>{DIRECTON}',
                        
                      '</tpl>'
            )
            
        }]
        this.tbar =  [{
                    text: 'Добавить информацию',
                    iconCls: 'add16',
                    action:'addInfrastructure'
                }];
        this.callParent();
    }
});