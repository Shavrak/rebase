Ext.define('PYA.view.Panels.Lots.ComonInfo' ,{
    extend: 'Ext.panel.Panel',
    alias: 'widget.comoninfopanel',
    store: 'Contacts.LotOwners',
    
    initComponent: function() {
        //this.tpl = new Ext.XTemplate('Title:{name}<br/>');
    //    this.html = this.startingMarkup;
        this.items=[{ 
            xtype:'dataview',
            store: this.store,
            itemSelector: 'active',
            tpl: ['<tpl for=".">',
                          '<br><b>Собственник :</b>{NAME}',
                          '<br><b>Компания :</b>{COMPANY_NAME}',
                      '</tpl>'],
            
            
        }]
        this.tbar =  [{
                    text: 'Добавить информацию',
                    iconCls: 'icon-add',
                    action:'addCommonInf'
                }];
        this.callParent();
    }
});