Ext.define('PYA.view.Panels.Blocks.BlocksSearchPan', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.searchblocks',
    layout: 'fit',
    title:'Объекты',
    autoShow: true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: true,
                url:'index.php?r=room/indexjson',
                layout:'hbox',
                //method:'POST',
                fieldDefaults: {
                    labelAlign: 'right',
                    labelWidth: 82,
                  //  msgTarget: 'side'
                },
                 defaults: {
                    border: false,
                    xtype: 'panel',
                    flex: 1,
                    layout: 'anchor'
                },
                items: [
//                        {
//                    items:[ 
//                            
//                            
//                             Ext.create('Ext.ux.form.field.BoxSelect', {
//                              fieldLabel: 'Направление block',
//                                displayField: 'name',
//                                valueField: 'abbr',
//                                width: 300,
//                                //labelWidth: 130,
//                                name:'DIRECTON',
//                                delimiter : "|",
//                                emptyText: 'Направление',
//                                multiSelect : true,
//                                store: Ext.create('Ext.data.Store', {
//                                        fields: ['abbr', 'name'],
//                                        data : [
//                                            {"abbr":"AL", "name":"Север"},
//                                            {"abbr":"AK", "name":"Запад"},
//                                            {"abbr":"AZ", "name":"Восток"}
//                                            //...
//                                        ]
//                                    }),
//                                queryMode: 'local'
//                            }),
//                         ]}
                       ,{
//                    columnWidth:.3,
//                    layout: 'column',
//                     bodyStyle:'padding:5px 0 5px 5px',
                    
                    items:[ 
                            {xtype:'textfield', fieldLabel: 'ставка от', name: 'PRICE_FROM', emptyText: 'от' },
                            {xtype:'textfield', fieldLabel: 'ставка до', name: 'PRICE_TO', emptyText: 'до'},
                            {        xtype: 'combobox',
                                    name: 'rent_curr_id',
                                    typeAhead: true,
                                    selectOnFocus: true,
                                    fieldLabel: 'Валюта',
                                    //queryMode: 'local',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',
                                    value:'4930',
                                    //flex: 3,
                                    // width:75,
                                    emptyText: 'Валюта',
                                    allowBlank: false,
                                    //margin: '0 5 0 5',
                                    store:'Handbooks.CurrencyList',

                                }
                         ]},{
//                    columnWidth:.3,
//                    layout: 'column',
//                     bodyStyle:'padding:5px 0 5px 5px',
                    items:[ 
                            {xtype:'textfield', fieldLabel: 'площадь от', name: 'AREA_FROM', emptyText: 'от' },
                            {xtype:'textfield', fieldLabel: 'площадь до', name: 'AREA_TO', emptyText: 'до',},
//                            {         xtype: 'combo',
//                                    //width:450,
//                                    store: Ext.create('Ext.data.Store', {
//                                        fields: ['id', 'name'],
//                                        data : [
//                                            {"id":"1", "name":"A+"},
//                                            {"id":"2", "name":"A"},
//                                            {"id":"3", "name":"B"}
//                                            //...
//                                        ]
//                                    }),
//                                    displayField: 'name',
//                                    valueField: 'id',
//                                    typeAhead: true,
//                                    editable:true,
//                                    mode:'local',
//                                    name:'CLASS_ID',
//                                    forceSelection: true,
//                                    triggerAction: 'all',
//                                    fieldLabel: 'Класс',
//                                    emptyText: 'Класс',
//                                    selectOnFocus: true
//
//                                }
                         ]}
                     ]
                }];

        this.buttons = [
            {
                text: 'Искать',
                action: 'search'
            }
        ];

        this.callParent(arguments);
    }
});