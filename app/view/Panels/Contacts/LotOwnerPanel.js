Ext.define('PYA.view.Panels.Contacts.LotOwnerPanel' ,{
    extend: 'Ext.panel.Panel',
    alias: 'widget.lotownerpanel',
     store: 'Contacts.LotOwners',
    
    initComponent: function() {
        //this.tpl = new Ext.XTemplate('Title:{name}<br/>');
    //    this.html = this.startingMarkup;
        this.items=[{ 
            xtype:'dataview',
            store: this.store,
            itemSelector: 'active',
            html:'<h2>Собственники</h2>',
            tpl: new Ext.XTemplate(
                      '<tpl for=".">',
                          '<br><b>Собственник :</b>{NAME}',
                          '<br><b>Телефон :</b>{PHONE}',
                          '<br><b>Компания :</b>{COMPANY_NAME}',
                          '<br><b>Описание :</b>{COMPANY_DESC}',
                          '<hr>',
                      '</tpl>'
            )
            
        }];
        this.tbar =  [{
                    text: 'Новый собственник',
                    iconCls: 'icon-user-add',
                    action:'newOwner'
                }
                ,'-',
                {
                    text: 'Новая компания',
                    iconCls: 'icon-add',
                    action:'newCompany'
                }
            ];
        this.callParent();
    }
});

