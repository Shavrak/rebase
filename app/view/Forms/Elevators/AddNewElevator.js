Ext.define('PYA.view.Forms.Elevators.AddNewElevator', {
    extend: 'Ext.window.Window',
    alias: 'widget.NewElevator',
    title: 'Лифт',
    layout: 'fit',
    autoShow: true,
    id:19,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                name:'newowner',
                width:500,
                height:180,
                //height:500,
                layout: 'form',
                bodyPadding:  '5 5 0',
                items: [ {  
                            name : 'elevator_type', //cargo or passanger
                            xtype: 'hidden',
                            
                           },
                         {xtype: 'combobox',
                            name : 'brand_id',
                            fieldLabel: 'Производитель',
                            store: 'Handbooks.Elevators',
                            width:70,  
                            queryMode: 'local',
                            displayField: 'VARCHAR_VALUE',
                            valueField: 'ID'},   
                         {   emptyText: 'Кол-во', 
                            fieldLabel: 'Кол-во',
                            name : 'elevators_count',
                            xtype: 'textfield',
                            margin: '0 5 5 5',
                            allowBlank: false
                           },
                        {   emptyText: 'г/п', 
                            fieldLabel: 'г/п',
                            name : 'elevators_load',
                            xtype: 'textfield',
                            margin: '0 5 5 5',
                            allowBlank: false
                           },
                           

                        
                ]
            },
            
        ];
        this.buttons = [

            {
                text: 'Сохранить',
                action: 'saveElevator',
                //handler: this.choseHand    
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];
        
        this.callParent(arguments);
        
    },
    
    
});

