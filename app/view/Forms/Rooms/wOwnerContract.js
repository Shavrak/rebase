Ext.define('PYA.view.Forms.Rooms.wOwnerContract', {
    extend: 'Ext.window.Window',
    alias: 'widget.ownercontract',
    title: 'Договра и собственники',
    layout: 'fit',
    autoShow: true,
    //store: 'Contacts.Owners',
    id:17,
    
    //store:'Handbooks.HandbooksCategory',
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                name:'ownercontract',
                width:500,
                height:330,
                //height:500,
                layout: 'form',
                bodyPadding:  '5 5 0',
                items: [
                      {xtype:'hidden',
                       name:'contrType',
                       value:0
                      },  
                     {
                        xtype: 'combo',
                        name: 'owner',
                        fieldLabel: 'Собственники',
                        store: 'Contacts.Owners',
                        displayField: 'NAME',
                        valueField: 'ID',
                        allowBlank: false,
                        flex: 2,
                        listConfig: {
                               loadingText: 'Поиск...',
                               // Custom rendering template for each item
                               getInnerTpl: function() {
                                   return '{NAME} из компании  {COMPANY_NAME}';
                               }
                           },
                           pageSize: 10
                    
                        
                    },
                         {   emptyText: 'Номер договора', 
                            fieldLabel: 'Номер договора',
                            name : 'CONTARCT_NUM',
                            xtype: 'textfield',
                            margin: '0 5 5 5',
                            allowBlank: false
                           },

                        {   emptyText: 'Дата подписания', 
                            fieldLabel: 'Дата подписания',
                            name : 'DATE_OF_SIGNING',
                            xtype: 'textfield',
                            margin: '0 5 5 5',
                            allowBlank: false
                           },
                           {   emptyText: 'Дата окончания', 
                            fieldLabel: 'Дата окнчания',
                            name : 'DATE_OF_END',
                            xtype: 'textfield',
                            margin: '0 5 5 5',
                            allowBlank: false
                           },
                           {
                            xtype: 'combobox',
                            name: 'broker_id',
                            displayField: 'NAME',
                            valueField: 'ID',
                            fieldLabel: 'Брокер',
                            emptyText: 'Брокер',
                            allowBlank: false,
                            margin: '0 5 5 5',
                            store:'Users.WholeUserList',
                            listConfig: {
                                     loadingText: 'Поиск...',

                                     // Custom rendering template for each item
                                     getInnerTpl: function() {
                                         return '{NAME}';
                                     }
                                 },
                             pageSize: 10

                        },        

                        {
                        xtype: 'combo',
                        name: 'CONTRACT_TYPE_ID',
                        fieldLabel: 'Тип договора',
                        store: 'Handbooks.OwnerContractType',
                        //queryMode: 'remote',
                        displayField: 'VARCHAR_VALUE',
                        valueField: 'ID',
                        flex: 2,
                    },
                   { emptyText: 'Процент с договора', 
                     fieldLabel: 'Процент с договора',
                     name : 'PECRENT_FROM_CONTRACT',
                     xtype: 'textfield',
                     margin: '0 5 5 5',
                     allowBlank: false
                   },
                    {
                        xtype: 'htmleditor',
                        name: 'NOTE',
                        fieldLabel: 'Примечание',
                        emptyText: 'Примечание',
                        margin: '5 5 5 5',
                        height: 100,
                        anchor: '100%'
                    }        
                    
                 
                    
                ]
            },
            
        ];
        this.tbar =  [{
                    text: 'Новый собственник',
                    iconCls: 'icon-user-add',
                    action:'newOwner'
                }
                ,'-',
                {
                    text: 'Новая компания',
                    iconCls: 'icon-add',
                    action:'newCompany'
                }
            ];
        this.buttons = [

            {
                text: 'Сохранить',
                action: 'saveContract',
                //handler: this.choseHand    
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];
        
        this.callParent(arguments);
        
    },
    
    
});