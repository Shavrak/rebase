
Ext.define('PYA.view.Forms.Rooms.AddRoom', {
    extend: 'Ext.window.Window',
    alias: 'widget.AddRoom',
    title: 'Информация о блоке',
    layout: 'fit',
    autoShow: true,
    id:16,
    //store:'Elevators.Elevator',
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                bodyStyle: {
                    background: 'none',
                    padding: '10px',
                    border: '0'
                },
                items:[
               {xtype: 'fieldset',
                layout: 'hbox',
                align: 'stretch',
                bodyPadding:  '0 0 0 0',
                items: [
                    //col 1
                     {xtype: 'fieldset',
                      layout: 'vbox',
                      title:'Информация о блоке',
                      align: 'stretch',
                      bodyPadding:  '0 0 0 0',
                      items: [
                                  {
                                       xtype: 'hidden',
                                       name: 'lot_id',
                                       allowBlank: false
                                   },  
                                   {
                                       xtype: 'hidden',
                                       name: 'room_id',
                                       allowBlank: false
                                   },
                                   {   emptyText: 'Лот блока', 
                                        fieldLabel: 'Лот',
                                        name : 'room_lot',
                                        xtype: 'textfield',
                                        margin: '0 5 5 5',
                                        tooltip: 'Лот',
                                        allowBlank: false
                                       },
                                    {   emptyText: 'Площадь', 
                                        fieldLabel: 'Площадь',
                                        name : 'area',
                                        xtype: 'textfield',
                                        margin: '0 5 5 5',
                                        tooltip: 'Лот',
                                        allowBlank: false
                                       },
                                       {
                                        xtype: 'textfield',
                                        name : 'floor',
                                        emptyText:'Этаж',
                                         margin: '0 5 5 5',
                                        allowBlank: false,
                                        fieldLabel: 'Этаж'
                                    },
                                    {
                                        xtype: 'datefield',
                                        margin: '0 5 5 5',
                                        startDay:1,
                                        name : 'ready_date',
                                        format: 'd.m.Y',
                                        fieldLabel: 'Готовность',
                                        emptyText:'Готовность',
                                    },
                                    {
                                        xtype: 'combobox',
                                        name: 'planing_id',
                                        displayField: 'VARCHAR_VALUE',
                                        valueField: 'ID',
                                        fieldLabel: 'Палнировка',
                                        emptyText: 'Палнировка',
                                        allowBlank: false,
                                        margin: '0 5 5 5',
                                        store:'Handbooks.Planning',

                                    },
                                    {
                                        xtype: 'combobox',
                                        name: 'finishing_id',
                                        displayField: 'VARCHAR_VALUE',
                                        valueField: 'ID',
                                        fieldLabel: 'Отделка',
                                        emptyText: 'Отделка',
                                        allowBlank: false,
                                        margin: '0 5 5 5',
                                        store:'Handbooks.Finishing',
                                     
                                    },        
                                    {
                                        xtype: 'combobox',
                                        name: 'contract_type_id',
                                        displayField: 'VARCHAR_VALUE',
                                        valueField: 'ID',
                                        fieldLabel: 'Договор',
                                        emptyText: 'Договор',
                                        allowBlank: false,
                                        margin: '0 5 5 5',
                                        store:'Handbooks.ContractType',

                                    },   
                                    {
                                        xtype: 'combobox',
                                        name: 'broker_id',
                                        displayField: 'NAME',
                                        valueField: 'ID',
                                        fieldLabel: 'Брокер',
                                        emptyText: 'Брокер',
                                        allowBlank: false,
                                        margin: '0 5 5 5',
                                        store:'Users.WholeUserList',
                                        listConfig: {
                                                 loadingText: 'Поиск...',

                                                 // Custom rendering template for each item
                                                 getInnerTpl: function() {
                                                     return '{NAME}';
                                                 }
                                             },
                                         pageSize: 10

                                    }, 
                                    {   xtype: 'combo',
                                        name: 'address',
                                        fieldLabel: 'Адрес',
                                        store: 'Address.Streets',
                                        displayField: 'ADDRESS',
                                        valueField: 'ID',
                                        margin: '0 5 5 5',
                                        flex: 2,
                                        listConfig: {
                                               loadingText: 'Поиск...',
                                               getInnerTpl: function() {
                                                   return '{ADDRESS}';
                                               }
                                           },
                                           pageSize: 10


                                    },        

                     ]}//'vbox col 1 end'
                      // vbox col 2  
                    ,{xtype: 'fieldset',
                      layout: 'vbox',
                      align: 'stretch',
                      title: 'Аренда субаренда',
                       bodyPadding:  '5 5 0',
                      //bodyPadding:  '0 0 0 0',
                      items: [
                                  {
                                xtype: 'fieldcontainer',
                                layout: 'hbox',
                                fieldLabel: 'Ставка',
                                combineErrors: false,
                                items: [
                                   {  
                                    xtype: 'textfield',
                                    name: 'rent_price',
                                    //queryMode: 'local',
                                    flex: 3,
                                     width:100,
                                    emptyText: 'Базовая ставка',
                                    allowBlank: false,
                                    margin: '0 5 0 5',
                                   },
                                   {
                                    xtype: 'combobox',
                                    name: 'rent_curr_id',
                                    //fieldLabel: 'Валюта',
                                    //queryMode: 'local',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',
                                    flex: 3,
                                     width:75,
                                    emptyText: 'Валюта',
                                    allowBlank: false,
                                    margin: '0 5 0 5',
                                    store:'Handbooks.CurrencyList',
                             
                                   },
                                   {
                                    xtype: 'combobox',
                                    name: 'rent_with_nds',
                                    //queryMode: 'local',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',
                                    flex: 3,
                                     width:75,
                                    emptyText: 'НДС',
                                    allowBlank: false,
                                    margin: '0 5 0 5',
                                    store:'Handbooks.WithNDS',
                             
                                   },
                                ]
                            }, 
                           {  
                            xtype: 'textfield',
                            name: 'operating_costs',
                            //queryMode: 'local',
                            flex: 2,
                            fieldLabel: 'Эксплутационные расходы',
                            emptyText: 'Эксплутационные расходы',
                            allowBlank: false,
                            margin: '0 5 0 5',
                           },        
                           {
                            xtype: 'combobox',
                            name: 'utility_bills_id',
                            //queryMode: 'local',
                            displayField: 'VARCHAR_VALUE',
                            valueField: 'ID',
                            flex: 2,
                            fieldLabel:  'Коммунальные',
                            emptyText: 'Коммунальные',
                            allowBlank: false,
                            margin: '0 5 0 5',
                            store:'Handbooks.UnityBills',
                           },     
                           {
                            xtype: 'combobox',
                            name: 'deposit_id',
                            //queryMode: 'local',
                            displayField: 'VARCHAR_VALUE',
                            valueField: 'ID',
                            flex: 2,
                            fieldLabel:  'Депозит',
                            emptyText: 'Депозит',
                            allowBlank: false,
                            margin: '0 5 0 5',
                            store:'Handbooks.Deposit',
                           },     
                           {  
                            xtype: 'textfield',
                            name: 'prepayd_value',
                            //queryMode: 'local',
                            flex: 2,
                            fieldLabel: 'Предоплата',
                            emptyText: 'Предоплата',
                            allowBlank: false,
                            margin: '0 5 0 5',
                           },    
                           {
                            xtype: 'combobox',
                            name: 'rent_type_id',
                            //queryMode: 'local',
                            displayField: 'VARCHAR_VALUE',
                            valueField: 'ID',
                            flex: 2,
                            fieldLabel:  'Тип аренды',
                            emptyText: 'Тип аренды',
                            allowBlank: false,
                            margin: '0 5 0 5',
                            store:'Handbooks.RentType',
                           },
                           {  
                            xtype: 'textfield',
                            name: 'our_percent',
                            //queryMode: 'local',
                            flex: 2,
                            fieldLabel: 'Наш процент',
                            emptyText: 'Наш процент',
                            allowBlank: false,
                            margin: '0 5 0 5',
                           },
                           {xtype: 'fieldcontainer',
                            layout: 'hbox',
                            margin: '5 0 5 0',
                            //fieldLabel: 'От метро',
                            align: 'stretch',
                            combineErrors: false,
                            items: [
                                {
                                    name : 'OwnerContrIdRent',
                                    xtype: 'hidden',
                                    value:'0'
                                },
                               {
                                   fieldLabel: 'Собственник и договр',
                                   name : 'OwnerContrRent',
                                   xtype: 'textfield',
                                   emptyText:'Собственник и договр',
                                   margin: '0 5 0 5',
                                   allowBlank: false
                               },
                               {
                                xtype: 'button',
                                text: 'Выбрать',
                                action:'RoomOwnerContrRent',
                                margins: '0 0 0 6',
                               }
                            ]
                        },
                     ]}//vbox col 2 end 
                ]} //'hbox'  
            
                ,{xtype: 'fieldset',
                      layout: 'vbox',
                      align: 'stretch',
                      title: 'Продажа',
                       bodyPadding:  '5 5 0',
                      //bodyPadding:  '0 0 0 0',
                      items: [
            
                {
                    xtype: 'fieldcontainer',
                    layout: 'hbox',
                    fieldLabel: 'Продажа',
                    combineErrors: false,
                    items: [
                       {  
                        xtype: 'textfield',
                        name: 'sale_price',
                        //queryMode: 'local',
                        flex: 3,
                        width:100,
                        emptyText: 'Стоимость кв.м',
                        allowBlank: false,
                        margin: '0 5 0 5',
                       },
                       {
                        xtype: 'combobox',
                        name: 'sale_curr_id',
                        //fieldLabel: 'Валюта',
                        //queryMode: 'local',
                        displayField: 'VARCHAR_VALUE',
                        valueField: 'ID',
                        flex: 3,
                        width:75,
                        emptyText: 'Валюта',
                        allowBlank: false,
                        margin: '0 5 0 5',
                        store:'Handbooks.CurrencyList',

                       },
                       {
                        xtype: 'combobox',
                        name: 'sale_with_nds',
                        //queryMode: 'local',
                        displayField: 'VARCHAR_VALUE',
                        valueField: 'ID',
                        flex: 3,
                        width:75,
                        emptyText: 'НДС',
                        allowBlank: false,
                        margin: '0 5 0 5',
                        store:'Handbooks.WithNDS',

                       },
                    ]
                },      

               {xtype: 'fieldcontainer',
                layout: 'hbox',
                margin: '5 0 5 0',
                //fieldLabel: 'От метро',
                align: 'stretch',
                combineErrors: false,
                items: [
                     {
                        name : 'OwnerContrIdSale',
                        xtype: 'hidden',
                        value:'0'
                    },
                   {
                       fieldLabel: 'Собственник и договр',
                       name : 'OwnerContrSale',
                       xtype: 'textfield',
                       emptyText:'Собственник и договр',
                       margin: '0 5 0 5',
                       allowBlank: false
                   },
                   {
                    xtype: 'button',
                    text: 'Выбрать',
                    action:'RoomOwnerContrSale',
                    margins: '0 0 0 6',
                   }
                ]
            }]
            }//vbox end
             ,{
                xtype: 'htmleditor',
                name: 'comment',
              //  fieldLabel: 'Описание',
                emptyText: 'Комментарий к блоку',
                margin: '5 5 5 5',
                height: 100,
                width : 750,
                flex: 2,
                anchor: '100%'
            }
             ]//root fieldset
            }
        ];
        this.buttons = [
            {
                text: 'Сохранить',
                action: 'saveroom'
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];
        this.callParent(arguments);
    }
});

