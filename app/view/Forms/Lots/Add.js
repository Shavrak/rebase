/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('PYA.view.Forms.Lots.Add', {
    extend: 'Ext.window.Window',
    alias: 'widget.AddBuilding',
    title: 'Новое здание',
    layout: 'fit',
    autoShow: true,
    id:8,
    //store:'Elevators.Elevator',
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                bodyStyle: {
                    background: 'none',
                    padding: '10px',
                    border: '0'
                },
                items:[
            {xtype: 'fieldset',
             layout: 'vbox',
             align: 'stretch',
             bodyPadding:  '0 0 0 0',
             items: [
                            {

                                   xtype: 'hidden',
                                   name: 'lot_id',
                                   allowBlank: false
                               },  
                                {   emptyText: 'Лот', 
                                    fieldLabel: 'Лот',
                                    name : 'lot',
                                    xtype: 'textfield',
                                    margin: '0 5 5 5',
                                    tooltip: 'Лот',
                                    allowBlank: false
                                   },
                                   {
                                    xtype: 'textfield',
                                    name : 'name',
                                    emptyText:'Название',
                                     margin: '0 5 0 5',
                                    allowBlank: false,
                                    fieldLabel: 'Название'
                                },
                               {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                margin: '5 0 5 0',
                                //fieldLabel: 'От метро',
                                align: 'stretch',
                                combineErrors: false,
                                items: [
                                   
                                   {
                                       fieldLabel: 'Адрес',
                                       name : 'address',
                                       xtype: 'textfield',
                                       emptyText:'Адрес',
                                       
                                       margin: '0 5 0 5',
                                       allowBlank: false
                                   },
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'AddressChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                            },
                            {   xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                   {   fieldLabel: 'Ориентир', 
                                       name : 'landmarks',
                                       xtype: 'textfield',
                                       
                                       emptyText:'Ориентир',
                                       margin: '0 5 0 5',   
                                       allowBlank: false
                                   },
                                   
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'LandmarkChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                                },
                                {
                                    xtype: 'combobox',
                                    name: 'broker_id',
                                    displayField: 'NAME',
                                    valueField: 'ID',
                                    fieldLabel: 'Брокер',
                                    emptyText: 'Брокер',
                                    allowBlank: false,
                                    margin: '0 5 5 5',
                                    store:'Users.WholeUserList',
                                    listConfig: {
                                             loadingText: 'Поиск...',

                                             // Custom rendering template for each item
                                             getInnerTpl: function() {
                                                 return '{NAME}';
                                             }
                                         },
                                     pageSize: 10
                                   
                                },
                                
                                {
                                    xtype: 'combobox',
                                    name : 'lot_status_id',
                                    fieldLabel: 'Состояние',
                                    emptyText:'Состояние',
                                    store: 'Lots.LotStatus',
                                    margin: '0 5 5 5',
                                    queryMode: 'local',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID'
                                },
                                {
                                    xtype: 'datefield',
                                    margin: '0 5 0 5',
                                    startDay:1,
                                    name : 'create_date',
                                    format: 'd.m.Y',
                                    fieldLabel: 'Поступление',
                                    emptyText:'Поступление',
                                },
                  ]
            }]//root fieldset
            }
        ];
        this.buttons = [
            {
                text: 'Сохранить',
                action: 'savelot'
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];
        this.callParent(arguments);
    }
});

