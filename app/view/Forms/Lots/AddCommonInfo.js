/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('PYA.view.Forms.Lots.AddCommonInfo', {
    extend: 'Ext.window.Window',
    alias: 'widget.AddCommonInfo',
    title: 'Общая информация',
    layout: 'fit',
    autoShow: true,
    id:11,
    //store:'Elevators.Elevator',
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                bodyStyle: {
                    background: 'none',
                    padding: '10px',
                    border: '0'
                },
                items:[
                {xtype: 'fieldset',
                 layout: 'vbox',
                 align: 'stretch',
                 bodyPadding:  '0 0 0 0',
                 items: [
                             {
                                   xtype: 'hidden',
                                   name: 'lot_id',
                                   allowBlank: false
                               },
                               
                               {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                margin: '5 0 5 0',
                                //fieldLabel: 'От метро',
                                align: 'stretch',
                                combineErrors: false,
                                items: [
                                   
                                   {
                                       fieldLabel: 'Собственники',
                                       name : 'owners',
                                       xtype: 'textfield',
                                       emptyText:'Собственники',
                                       
                                       margin: '0 5 0 5',
                                       allowBlank: false
                                   },
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'OwnersChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                            },
                               {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                margin: '5 0 5 0',
                                //fieldLabel: 'От метро',
                                align: 'stretch',
                                combineErrors: false,
                                items: [
                                   
                                   {
                                       fieldLabel: 'Девелопер/застройщик',
                                       name : 'developer',
                                       xtype: 'textfield',
                                       emptyText:'Девелопер/застройщик',
                                       
                                       margin: '0 5 0 5',
                                       allowBlank: false
                                   },
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'DeveloperChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                            },
                            {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                margin: '5 0 5 0',
                                //fieldLabel: 'От метро',
                                align: 'stretch',
                                combineErrors: false,
                                items: [
                                   
                                   {
                                       fieldLabel: 'Инвестор',
                                       name : 'investor',
                                       xtype: 'textfield',
                                       emptyText:'Инвестор',
                                       
                                       margin: '0 5 0 5',
                                       allowBlank: false
                                   },
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'InvestorChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                            },
                            {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                margin: '5 0 5 0',
                                //fieldLabel: 'От метро',
                                align: 'stretch',
                                combineErrors: false,
                                items: [
                                   
                                   {
                                       fieldLabel: 'Эксклюзивн.агент',
                                       name : 'XclusivAgent',
                                       xtype: 'textfield',
                                       emptyText:'Эксклюзивн.агент',
                                       
                                       margin: '0 5 0 5',
                                       allowBlank: false
                                   },
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'XclusivAgentChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                            },
                            {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                margin: '5 0 5 0',
                                //fieldLabel: 'От метро',
                                align: 'stretch',
                                combineErrors: false,
                                items: [
                                   
                                   {
                                       fieldLabel: 'Ко-эксклюзивн.агент',
                                       name : 'CoXclusivAgent',
                                       xtype: 'textfield',
                                       emptyText:'Ко-эксклюзивн.агент',
                                       
                                       margin: '0 5 0 5',
                                       allowBlank: false
                                   },
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'CoXclusivAgentChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                            },
                            {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                margin: '5 0 5 0',
                                //fieldLabel: 'От метро',
                                align: 'stretch',
                                combineErrors: false,
                                items: [
                                   
                                   {
                                       fieldLabel: 'Управляющая компания',
                                       name : 'ManageCompany',
                                       xtype: 'textfield',
                                       emptyText:'Управляющая компания',
                                       
                                       margin: '0 5 0 5',
                                       allowBlank: false
                                   },
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'ManageCompanyChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                            },
                            
                            {
                                xtype: 'fieldcontainer',
                                layout: 'hbox',
                                fieldLabel: 'Стоимость проекта',
                                combineErrors: false,

                                items: [
                                   {  
                                    xtype: 'textfield',
                                    name: 'district',
                                    //queryMode: 'local',
                                    flex: 2,
                                    emptyText: 'Стоимость проекта',
                                    allowBlank: false,
                                    margin: '0 5 0 5',
                                   },
                                   {
                                    xtype: 'combobox',
                                    name: 'direction',
                                    fieldLabel: 'Валюта',
                                    //queryMode: 'local',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',
                                    flex: 2,
                                    emptyText: 'Валюта',
                                    allowBlank: false,
                                    margin: '0 5 0 5',
                                    store:'Handbooks.CurrencyList',
                             
                                   },

                                ]
                            },
                            
                                
                  ]
            }]//root fieldset
            }
        ];
        this.buttons = [
            {
                text: 'Сохранить',
                //action: 'saveComonn'
                handler:this.testFn
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];
        this.callParent(arguments);
    },
    testFn: function(){
       var win    = this.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
      console.log('lot_id '+values['lot_id']);   
    }
});

