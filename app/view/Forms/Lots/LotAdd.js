// /scripts/app/view/user/Add.js
Ext.define('PYA.view.Forms.Lots.LotAdd', {
    extend: 'Ext.window.Window',
    alias: 'widget.AddBuilding',
    title: 'Новое здание',
    layout: 'fit',
    autoShow: true,
    id:7,
    //store:'Elevators.Elevator',
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                bodyStyle: {
                    background: 'none',
                    padding: '10px',
                    border: '0'
                },
                items:[
            {xtype: 'fieldset',
             layout: 'hbox',
             align: 'stretch',
             bodyPadding:  '0 0 0 0',
             items: [
                    { xtype: 'fieldset',
                     layout: 'vbox',
                     align: 'stretch',
                     bodyPadding:  '0 0 0',
                     items: [
                        
                       {xtype: 'fieldset',
                        title: 'Общие данные',
                        layout: 'vbox',
                        align: 'stretch',
                        bodyPadding:  '5 5 0',
                        items: [
                               {   emptyText: 'Лот', 
                                    name : 'lot_id',
                                    xtype: 'hidden',
                                    margin: '0 5 0 5',   
                                    allowBlank: false
                                   },  
                               {   emptyText: 'Лот', 
                                    name : 'lot',
                                    xtype: 'textfield',
                                    margin: '0 5 0 5',   
                                    allowBlank: false
                                   }, 
                               {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                margin: '5 0 5 0',
                                //fieldLabel: 'От метро',
                                align: 'stretch',
                                combineErrors: false,
                                items: [
                                   
                                   {
                                      // fieldLabel: 'Адрес',
                                       name : 'address',
                                       xtype: 'textfield',
                                       emptyText:'Адрес',
                                       margin: '0 5 0 5',
                                       allowBlank: false
                                   },
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'AddressChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                            },
                            {   xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                combineErrors: false,
                                items: [
                                   {  // fieldLabel: 'Ориентир', 
                                       name : 'landmarks',
                                       xtype: 'textfield',
                                       emptyText:'Ориентир',
                                       margin: '0 5 0 5',   
                                       allowBlank: false
                                   },
                                   
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'LandmarkChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                                },
                                {
                                    xtype: 'textfield',
                                    name : 'lot_name',
                                    emptyText:'Название',
                                    margin: '0 5 2 5',
                                    //fieldLabel: 'Название'
                                },
                                {
                                    xtype: 'combobox',
                                    name: 'broker_id',
                                   // queryMode: 'local',
                                    displayField: 'NAME',
                                    valueField: 'ID',
                                    flex: 2,
                                    emptyText: 'Брокер',
                                    allowBlank: false,
                                    margin: '0 5 2 5',
                                    store:'Users.WholeUserList',
                                    listConfig: {
                                             loadingText: 'Поиск...',

                                             // Custom rendering template for each item
                                             getInnerTpl: function() {
                                                 return '{NAME}';
                                             }
                                         },
                                     pageSize: 10
                                    
                                    
                                    
                                },
                                {
                                    xtype: 'combobox',
                                    name : 'lot_status_id',
                                    //fieldLabel: 'Состояние',
                                    emptyText:'Состояние',
                                    margin: '0 5 2 5',
                                    store: 'Handbooks.BuldingActivePassiv',
                                    queryMode: 'local',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',
                                },
                                {
                                    xtype: 'datefield',
                                    name : 'create_date',
                                    format: 'd.m.Y',
                                    margin: '0 5 2 5',
                                    //fieldLabel: 'Поступление'
                                    emptyText:'Поступление',
                                },
                                
                      ]
                  },//fieldset
                    { xtype: 'fieldset',
                      title: 'Входные данные',
                      layout: 'vbox',
                      //align: 'stretch',
                      bodyPadding:  '5 5 0',
                      items: [
                              { xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                   {   emptyText: 'Собственник', 
                                       name : 'owner',
                                       xtype: 'textfield',
                                       margin: '0 5 0 5',   
                                       allowBlank: true
                                   },
                                   
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'OwnerChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                                },  
                                {   xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                
                                combineErrors: false,
                                items: [
                                   {   emptyText: 'Девелопер', 
                                       name : 'developer',
                                       xtype: 'combo',
                                       margin: '0 5 0 5',   
                                       store: 'Contacts.Companies',
                                       displayField: 'NAME',
                                       valueField: 'ID',
                                       allowBlank: true,
                                       listConfig: {
                                            loadingText: 'Поиск...',
                                            // Custom rendering template for each item
                                            getInnerTpl: function() {
                                                return '<b>{NAME}</b>, тел:{PHONE}, адрес:<b>{ADDRESS}</b>';
                                            }
                                        },
                                        pageSize: 10
                                   },
                                   
                                   {
                                    xtype: 'button',
                                    text: 'Новый',
                                    action:'DeveloperChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                                },
                               {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                   {   emptyText: 'Инвестор', 
                                       name : 'investor',
                                       xtype: 'combo',
                                       margin: '0 5 0 5', 
                                       store: 'Contacts.Companies',
                                       displayField: 'NAME',
                                       valueField: 'ID',
                                       allowBlank: true,
                                       listConfig: {
                                            loadingText: 'Поиск...',
                                            // Custom rendering template for each item
                                            getInnerTpl: function() {
                                                return '<b>{NAME}</b>, тел:{PHONE}, адрес:<b>{ADDRESS}</b>';
                                            }
                                        },
                                        pageSize: 10
                                   },
                                   
                                   {
                                    xtype: 'button',
                                    text: 'Новый',
                                    action:'InvestorChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                                },
                                {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                   {   emptyText: 'Стоимость', 
                                       name : 'project_cost',
                                       xtype: 'textfield',
                                       margin: '0 5 0 5',   
                                       allowBlank: true
                                   },
                                   
                                 {xtype: 'combobox',
                                  name : 'curr_proj_cost',
                                   //fieldLabel: 'Валюта',
                                    //queryMode: 'local',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',
                                    flex: 3,
                                     width:75,
                                    emptyText: 'Валюта',
                                    allowBlank: false,
                                    margin: '0 5 0 5',
                                    store:'Handbooks.CurrencyList',}
                                ]
                                },
                                {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                    {
                                        xtype: 'combo',
                                        name: 'exclusive_agent',
                                        emptyText: 'Экскл. агент',
                                         store:'Contacts.Agents',
                                        displayField: 'NAME',
                                        valueField: 'ID',
                                        margin: '0 5 0 5',   
                                        flex: 2,
                                        listConfig: {
                                               loadingText: 'Поиск...',
                                               // Custom rendering template for each item
                                               getInnerTpl: function() {
                                                   return '{NAME} из компании  {COMPANY_NAME}';
                                               }
                                           },
                                           pageSize: 10

                                   },
                                   {
                                    xtype: 'button',
                                    text: 'Новый',
                                    action:'ExclAgentChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                                },
                                {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                    {
                                        xtype: 'combo',
                                        name: 'coexclusive_agent',
                                        emptyText: 'Ко-эксл. агент',
                                         store:'Contacts.Agents',
                                        displayField: 'NAME',
                                        valueField: 'ID',
                                        margin: '0 5 0 5',   
                                        flex: 2,
                                        listConfig: {
                                               loadingText: 'Поиск...',
                                               // Custom rendering template for each item
                                               getInnerTpl: function() {
                                                   return '{NAME} из компании  {COMPANY_NAME}';
                                               }
                                           },
                                           pageSize: 10

                                   },
                                   
                                   {
                                    xtype: 'button',
                                    text: 'Новый',
                                    action:'CoExclAgentChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                                },
                                {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                   {   emptyText: 'Управ. компания', 
                                       name : 'manage_company',
                                       xtype: 'combo',
                                       margin: '0 5 0 5',   
                                       allowBlank: true,
                                       store: 'Contacts.Companies',
                                       displayField: 'NAME',
                                       valueField: 'ID',
                                       listConfig: {
                                            loadingText: 'Поиск...',
                                            // Custom rendering template for each item
                                            getInnerTpl: function() {
                                                return '<b>{NAME}</b>, тел:{PHONE}, адрес:<b>{ADDRESS}</b>';
                                            }
                                        },
                                        pageSize: 10
                                   },
                                   
                                   {
                                    xtype: 'button',
                                    text: 'Новый',
                                    action:'ManageCompanyChoise',
                                    margins: '0 0 0 6',
                                   }
                                ]
                                },
                          ]
                     },//fieldset
                     ]},
                 //second column
                 { xtype: 'fieldset',
                   layout: 'vbox',
                   align: 'stretch',
                     //bodyPadding:  '5 5 0',
                   items: [
                     //second colum
                     { xtype: 'fieldset',
                      title: 'Данные по зданию',
                      layout: 'vbox',
                      align: 'stretch',
                      bodyPadding:  '5 5 0',
                      items: [
                          { xtype: 'fieldcontainer',
                            layout: 'hbox',
                            align: 'stretch',
                            //fieldLabel: 'От метро',
                            combineErrors: false,
                            items: [
                               {xtype: 'combobox',
                                name : 'ready_id',
                                emptyText:'Готовность',
                                margins: '0 5 0 0',
                                store: 'Handbooks.BuldingReady',
                                displayField: 'VARCHAR_VALUE',
                                valueField: 'ID',},

                               {
                                    xtype: 'datefield',
                                    name : 'ready_dt',
                                    format: 'd.m.Y',
                                    emptyText: 'Дата готовности'
                                },
                            ]
                            },
                            {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                   {xtype: 'combobox',
                                    name : 'type_id',
                                    emptyText:'Тип здания',
                                    margins: '0 5 0 0',
                                    store: 'Handbooks.BuldingType',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',},
                                   
                                   {xtype: 'combobox',
                                    name : 'class_id',
                                    emptyText:'Класс',
                                    margins: '0 5 0 0',
                                    store: 'Handbooks.BuldingClass',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',},
                                ]
                                },
                          
                      ]},//fieldset
                      { xtype: 'fieldset',
                      title: 'Площадь',
                      layout: 'vbox',
                      //align: 'stretch',
                      bodyPadding:  '5 5 0',
                      items: [
                          {           
                            emptyText: 'Общая', 
                            name : 'comon_area',
                            xtype: 'textfield',
                            margin: '5 5 5 5',   
                            width:'200',
                            allowBlank: true
                           },
                          { xtype: 'fieldcontainer',
                            layout: 'hbox',
                            align: 'stretch',
                            //fieldLabel: 'От метро',
                            combineErrors: false,
                            items: [
                               {           
                                emptyText: 'офисная', 
                                name : 'office_area',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },

                               {           
                                emptyText: 'торговая', 
                                name : 'trade_area',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },
                            ]
                            },
                            {   xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                               {           
                                emptyText: 'гостиничная', 
                                name : 'guest_area',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },

                               {           
                                emptyText: 'развлекательная', 
                                name : 'intertaiment_area',
                                xtype: 'textfield',
                                margin: '0 5 0 5',  
                                width:'50',
                                allowBlank: true
                               },
                            ]
                            },
                            {   xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                               {           
                                emptyText: 'апартаменты', 
                                name : 'apartment_area',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },

                               {           
                                emptyText: 'жилая', 
                                name : 'live_area',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },
                            ]
                            },
                            {   xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                               {           
                                emptyText: 'складская', 
                                name : 'wh_area',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },

                               {           
                                emptyText: 'производтсвення', 
                                name : 'industrial_area',
                                xtype: 'textfield',
                                margin: '0 5 0 5',  
                                width:'50',
                                allowBlank: true
                               },
                            ]
                            },
                          
                      ]},// fieldset
                      { xtype: 'fieldset',
                      title: 'Количество этажей',
                      layout: 'vbox',
                      align: 'stretch',
                      //bodyPadding:  '5 5 0',
                      items: [
                          { xtype: 'fieldcontainer',
                            layout: 'hbox',
                            align: 'stretch',
                            //fieldLabel: 'От метро',
                            combineErrors: false,
                            items: [
                               {           
                                emptyText: 'Наземных', 
                                name : 'upground_count',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },
                               {           
                                emptyText: 'Подземных', 
                                name : 'undeground_count',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },
                            ]
                            },
                           { xtype: 'fieldcontainer',
                            layout: 'hbox',
                            align: 'stretch',
                            //fieldLabel: 'От метро',
                            combineErrors: false,
                            items: [
                               {           
                                fieldLabel: 'Мансарда', 
                                name : 'is_mansard',
                                xtype: 'checkbox',
                                margin: '0 5 0 5',   
                                
                               },
                               {           
                                fieldLabel: 'Цоколь', 
                                name : 'is_basement',
                                xtype: 'checkbox',
                                margin: '0 5 0 5',   
                                
                               },
                            ]
                            },
                            { xtype: 'fieldcontainer',
                            layout: 'hbox',
                            align: 'stretch',
                            //fieldLabel: 'От метро',
                            combineErrors: false,
                            items: [
                               {           
                                emptyText: 'Год строительства', 
                                name : 'build_year',
                                xtype: 'datefield',
                                format: 'd.m.Y',
                                margin: '0 5 0 5',   
                                
                               },
                               {           
                                emptyText: 'Год реконструкции', 
                                name : 'reconstruction_year',
                                xtype: 'datefield',
                                format: 'd.m.Y',
                                margin: '0 5 0 5',   
                                
                               },
                            ]
                            },
                            { xtype: 'fieldcontainer',
                            layout: 'hbox',
                            align: 'stretch',
                            //fieldLabel: 'От метро',
                            combineErrors: false,
                            items: [
                               {           
                                emptyText: 'Госкоммисия', 
                                name : 'state_commision',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },
                               {           
                                emptyText: 'Свидетельство', 
                                name : 'certificate',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },
                            ]
                            },
                          
                      ]},// fieldset
                  
                      
                  
                    ]  //   end container item
                   },    //  end second column
                   //thrid column
                 { xtype: 'fieldset',
                   layout: 'vbox',
                   align: 'stretch',
                     //bodyPadding:  '5 5 0',
                   items: [
                     //thrid colum
                     { xtype: 'fieldset',
                      title: 'Технические характеристики',
                      layout: 'vbox',
                      align: 'stretch',
                      //bodyPadding:  '5 5 0',
                      items: [
                          { xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                    {   emptyText: 'Пассажирсике лифты', 
                                       name : 'PassangerElevatorId',
                                       xtype: 'hidden',
                                       margin: '0 5 0 5',   
                                       allowBlank: true
                                   },
                                   {   emptyText: 'Пассажирсике лифты', 
                                       name : 'PassangerElevator',
                                       xtype: 'textfield',
                                       margin: '0 5 0 5',   
                                       allowBlank: true
                                   },
                                   
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'PassangerElevator',
                                    margins: '0 0 0 6',
                                   }
                                ]
                                },
                                { xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                    {   emptyText: 'Грузовые лифты', 
                                       name : 'CargoElevatorId',
                                       xtype: 'hidden',
                                       margin: '0 5 0 5',   
                                       allowBlank: true
                                   },
                                   {   emptyText: 'Грузовые лифты', 
                                       name : 'CargoElevator',
                                       xtype: 'textfield',
                                       margin: '0 5 0 5',   
                                       allowBlank: true
                                   },
                                   
                                   {
                                    xtype: 'button',
                                    text: 'Выбрать',
                                    action:'CargoElevator',
                                    margins: '0 0 0 6',
                                   }
                                ]
                                },
                                {           
                                emptyText: 'Высот. потолка в чистом виде', 
                                name : 'ceiling_height',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },
                          { xtype: 'fieldcontainer',
                            layout: 'hbox',
                            align: 'stretch',
                            //fieldLabel: 'От метро',
                            combineErrors: false,
                            items: [
                               {           
                                emptyText: 'Шаг колон', 
                                name : 'column_grid',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },
                               {           
                                emptyText: 'Глубина этажа', 
                                name : 'floor_depth',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },
                            ]
                            },
                           
                           
                            { xtype: 'fieldcontainer',
                            layout: 'hbox',
                            align: 'stretch',
                            //fieldLabel: 'От метро',
                            combineErrors: false,
                            items: [
                               {           
                                emptyText: 'Макс. площадь этажа', 
                                name : 'max_floor_area',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },
                               {           
                                emptyText: 'Мин. блок', 
                                name : 'min_block',
                                xtype: 'textfield',
                                margin: '0 5 0 5',   
                                width:'50',
                                allowBlank: true
                               },
                            ]
                            },
                          
                      ]},// fieldset
                  
                      { xtype: 'fieldset',
                      title: 'Аренда',
                      layout: 'vbox',
                      align: 'stretch',
                      //bodyPadding:  '5 5 0',
                      items: [
                                {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                combineErrors: false,
                                items: [
                                   {   emptyText: 'Ставка базовая', 
                                       name : 'rent_price',
                                       xtype: 'textfield',
                                       margin: '0 5 0 5',   
                                       flex: 2,
                                       allowBlank: true
                                   },
                                   
                                 {
                                    xtype: 'combobox',
                                    name: 'rent_curr_id',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',
                                    flex: 3,
                                     width:75,
                                    emptyText: 'Валюта',
                                    allowBlank: false,
                                    margin: '0 5 0 5',
                                    store:'Handbooks.CurrencyList',
                             
                                   },
                                   {
                                    xtype: 'combobox',
                                    name: 'rent_with_nds',
                                    //queryMode: 'local',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',
                                    flex: 3,
                                     width:75,
                                    emptyText: 'НДС',
                                    allowBlank: false,
                                    margin: '0 5 0 5',
                                    store:'Handbooks.WithNDS',
                             
                                   },
                                ]
                                },
                                
                                { xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                  
                                           
                                  {  
                                    xtype: 'textfield',
                                    name: 'operating_costs',
                                    //queryMode: 'local',
                                    flex: 2,
                                    //fieldLabel: 'Эксплутационные расходы',
                                    emptyText: 'Эксплутационные расходы',
                                    allowBlank: false,
                                    margin: '0 5 0 5',
                                   },          
                                   {
                                    xtype: 'combobox',
                                    name: 'operating_curr_id',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',
                                    flex: 3,
                                     width:75,
                                    emptyText: 'Валюта',
                                    allowBlank: false,
                                    margin: '0 5 0 5',
                                    store:'Handbooks.CurrencyList',
                             
                                   },
                                  
                                ]
                                },
                                
                          { xtype: 'fieldcontainer',
                            layout: 'hbox',
                            align: 'stretch',
                            combineErrors: false,
                            items: [
                               {
                                    xtype: 'combobox',
                                    name: 'utility_bills_id',
                                    //queryMode: 'local',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',
                                    flex: 2,
                                   // fieldLabel:  'Коммунальные',
                                    emptyText: 'Коммунальные',
                                    allowBlank: false,
                                    margin: '0 5 0 5',
                                    store:'Handbooks.UnityBills',
                                   },  
                               {
                                xtype: 'combobox',
                                name: 'deposit_id',
                                //queryMode: 'local',
                                displayField: 'VARCHAR_VALUE',
                                valueField: 'ID',
                                flex: 2,
                               // fieldLabel:  'Депозит',
                                emptyText: 'Депозит',
                                allowBlank: false,
                                margin: '0 5 0 5',
                                store:'Handbooks.Deposit',
                               },     
                               
                            ]
                            },
                           
                           
                            { xtype: 'fieldcontainer',
                            layout: 'hbox',
                            align: 'stretch',
                            //fieldLabel: 'От метро',
                            combineErrors: false,
                            items: [
                               {  
                                xtype: 'textfield',
                                name: 'prepayd_value',
                                //queryMode: 'local',
                                flex: 2,
                               // fieldLabel: 'Предоплата',
                                emptyText: 'Предоплата',
                                allowBlank: false,
                                margin: '0 5 0 5',
                               },  
                               {
                                xtype: 'combobox',
                                name: 'rent_type_id',
                                //queryMode: 'local',
                                displayField: 'VARCHAR_VALUE',
                                valueField: 'ID',
                                flex: 2,
                                //fieldLabel:  'Тип аренды',
                                emptyText: 'Тип аренды',
                                allowBlank: false,
                                margin: '0 5 0 5',
                                store:'Handbooks.RentType',
                               },
                            ]
                            },
                          
                      ]},// fieldset
                      { xtype: 'fieldset',
                      title: 'Продажа',
                      layout: 'vbox',
                      align: 'stretch',
                      //bodyPadding:  '5 5 0',
                      items: [  {   emptyText: 'Продажа блоками', 
                                       name : 'sale_bloks',
                                       xtype: 'textfield',
                                       margin: '0 5 0 5',   
                                       allowBlank: true
                                   },
                                {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                   {  
                                        xtype: 'textfield',
                                        name: 'sale_price',
                                        //queryMode: 'local',
                                        flex: 3,
                                        width:100,
                                        emptyText: 'Стоимость кв.м',
                                        allowBlank: false,
                                        margin: '0 5 0 5',
                                       },
                                       {
                                        xtype: 'combobox',
                                        name: 'sale_curr_id',
                                        //fieldLabel: 'Валюта',
                                        //queryMode: 'local',
                                        displayField: 'VARCHAR_VALUE',
                                        valueField: 'ID',
                                        flex: 3,
                                        width:75,
                                        emptyText: 'Валюта',
                                        allowBlank: false,
                                        margin: '0 5 0 5',
                                        store:'Handbooks.CurrencyList',

                                       },
                                       {
                                        xtype: 'combobox',
                                        name: 'sale_with_nds',
                                        //queryMode: 'local',
                                        displayField: 'VARCHAR_VALUE',
                                        valueField: 'ID',
                                        flex: 3,
                                        width:75,
                                        emptyText: 'НДС',
                                        allowBlank: false,
                                        margin: '0 5 0 5',
                                        store:'Handbooks.WithNDS',

                                       },
                                   
                                   
                                   
                                 
                                ]
                                },
                                {xtype: 'fieldcontainer',
                                layout: 'hbox',
                                align: 'stretch',
                                //fieldLabel: 'От метро',
                                combineErrors: false,
                                items: [
                                   {   emptyText: 'Стоимость объекта', 
                                       name : 'object_cost',
                                       xtype: 'textfield',
                                       margin: '0 5 0 5',   
                                       width:125,
                                       allowBlank: true
                                   },
                                   
                                 {
                                        xtype: 'combobox',
                                        name: 'object_curr_id',
                                        //fieldLabel: 'Валюта',
                                        //queryMode: 'local',
                                        displayField: 'VARCHAR_VALUE',
                                        valueField: 'ID',
                                        flex: 3,
                                        width:75,
                                        emptyText: 'Валюта',
                                        allowBlank: false,
                                        margin: '0 5 0 5',
                                        store:'Handbooks.CurrencyList',

                                       },
                                       {
                                        xtype: 'combobox',
                                        name: 'object_with_nds',
                                        //queryMode: 'local',
                                        displayField: 'VARCHAR_VALUE',
                                        valueField: 'ID',
                                        flex: 3,
                                        width:75,
                                        emptyText: 'НДС',
                                        allowBlank: false,
                                        margin: '0 5 0 5',
                                        store:'Handbooks.WithNDS',

                                       },
                                ]
                                },
                                
                                
                          
                      ]},// fieldset
                     ]  //   end container item
                  },    //  end thrid column
                 ]
              
              
            }]//root fieldset
            }
        ];
        this.buttons = [
            {
                text: 'Сохранить',
                action: 'savelot'
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];
        this.callParent(arguments);
    }
});
 

