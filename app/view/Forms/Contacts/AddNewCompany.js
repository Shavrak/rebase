Ext.define('PYA.view.Forms.Contacts.AddNewCompany', {
    extend: 'Ext.window.Window',
    alias: 'widget.AddNewCompany',
    title: 'Создать новую компанию',
    layout: 'fit',
    autoShow: true,
    //store: 'Contacts.Owners',
    id:15,
    
    //store:'Handbooks.HandbooksCategory',
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                name:'newowner',
                width:500,
                height:180,
                //height:500,
                layout: 'form',
                bodyPadding:  '5 5 0',
                items: [
                         {   emptyText: 'Название', 
                            fieldLabel: 'Название',
                            name : 'name',
                            xtype: 'textfield',
                            margin: '0 5 5 5',
                            allowBlank: false
                           },
                        {   emptyText: 'Телефон', 
                            fieldLabel: 'Телефон',
                            name : 'phone',
                            xtype: 'textfield',
                            margin: '0 5 5 5',
                            allowBlank: false
                           },

                        {
                    
                        xtype: 'combo',
                        name: 'address',
                        fieldLabel: 'Адрес',
                        store: 'Address.Streets',
                        //queryMode: 'remote',
                        displayField: 'ADDRESS',
                        valueField: 'ID',
                        //typeAhead: false,
                        //hideTrigger:true,
                        flex: 2,
                            

                        listConfig: {
                               loadingText: 'Поиск...',
                               // Custom rendering template for each item
                               getInnerTpl: function() {
                                   return '{ADDRESS}';
                               }
                           },
                           pageSize: 10
                    
                        
                    },
                    {
                        xtype: 'htmleditor',
                        name: 'description',
                        fieldLabel: 'Описание',
                        emptyText: 'Описание',
                        margin: '5 5 5 5',
                        height: 100,
                        anchor: '100%'
                    }        
                    
                 
                    
                ]
            },
            
        ];
        this.buttons = [

            {
                text: 'Сохранить',
                action: 'saveCompany',
                //handler: this.choseHand    
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];
        
        this.callParent(arguments);
        
    },
    
    
});