Ext.define('PYA.view.Forms.Contacts.AddNewAgent', {
    extend: 'Ext.window.Window',
    alias: 'widget.NewAgent',
    title: 'Создать нового агента',
    layout: 'fit',
    autoShow: true,
    id:18,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                name:'newowner',
                width:500,
                height:180,
                //height:500,
                layout: 'form',
                bodyPadding:  '5 5 0',
                items: [
                         {   emptyText: 'Имя', 
                            fieldLabel: 'Имя',
                            name : 'name',
                            xtype: 'textfield',
                            margin: '0 5 5 5',
                            allowBlank: false
                           },
                        {   emptyText: 'Телефон', 
                            fieldLabel: 'Телефон',
                            name : 'phone',
                            xtype: 'textfield',
                            margin: '0 5 5 5',
                            allowBlank: false
                           },
                           {xtype: 'combobox',
                            name : 'AGENT_TYPE',
                            fieldLabel: 'Тип',
                            store: Ext.create('Ext.data.Store', {
                                  fields: ['id', 'name'],
                                  data : [
                                      {"id":"1", "name":"Эксклюзивный"},
                                      {"id":"2", "name":"Ко-эксклюзивный"},

                                  ]
                              }),
                            width:70,  
                            value:"1",
                            queryMode: 'local',
                            displayField: 'name',
                            valueField: 'id'},

                        {
                            xtype: 'combo',
                            name: 'company',
                            fieldLabel: 'Компания',
                            store: 'Contacts.Companies',
                            displayField: 'NAME',
                            valueField: 'ID',
                            flex: 2,
                            listConfig: {
                                   loadingText: 'Поиск...',
                                   // Custom rendering template for each item
                                   getInnerTpl: function() {
                                       return '<b>{NAME}</b>, тел:{PHONE}, адрес:<b>{ADDRESS}</b>';
                                   }
                               },
                               pageSize: 10
                        }
                ]
            },
            
        ];
        this.buttons = [

            {
                text: 'Сохранить',
                action: 'saveAgent',
                //handler: this.choseHand    
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];
        
        this.callParent(arguments);
        
    },
    
    
});

