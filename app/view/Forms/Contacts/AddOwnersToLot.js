Ext.define('PYA.view.Forms.Contacts.AddOwnersToLot', {
    extend: 'Ext.window.Window',
    alias: 'widget.AddOwnersToLot',
    title: 'Собственники',
    layout: 'fit',
    autoShow: true,
    store: 'Contacts.Owners',
    id:13,
    
    //store:'Handbooks.HandbooksCategory',
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                name:'owners',
                width:500,
                height:280,
                //height:500,
                layout: 'form',
                bodyPadding:  '5 5 0',
                
                items: [
                    {
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Собственник',
                    layout: 'hbox',
                    labelWidth: 50,
                    combineErrors: true,
                    defaults: {
                        hideLabel: 'true'
                    },
                    items: [
                        {
                    
                        xtype: 'hidden',
                        name: 'lot_id',
                        //value:'1',
                        allowBlank: false
                    },
                      {
                        xtype: 'combo',
                        name: 'owner',
                        fieldLabel: 'Собственники',
                        store: this.store,
                        displayField: 'NAME',
                        valueField: 'ID',
                        flex: 2,
                        listConfig: {
                               loadingText: 'Поиск...',
                               // Custom rendering template for each item
                               getInnerTpl: function() {
                                   return '{NAME} из компании  {COMPANY_NAME}';
                               }
                           },
                           pageSize: 10
                        
                    }, {
                        xtype: 'button',
                        text: 'Выбрать',
                        //action:'addStreet',
                        handler: this.choseHand,
                        margins: '0 0 0 6',
                    }]
                },
                    
                    
                 {xtype:'ownerlist'},
                    
                ]
            },
            
        ];
        
         this.tbar =  [{
                    text: 'Новый собственник',
                    iconCls: 'icon-user-add',
                    action:'newOwner'
                }
                ,'-',
                {
                    text: 'Новая компания',
                    iconCls: 'icon-add',
                    action:'newCompany'
                }
            ];
        this.buttons = [

            {
                text: 'Выбрать',
                action: 'chose',
                
                
                //handler: this.choseHand    
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];
        
        this.callParent(arguments);
        
    },
    listeners: {
        //afterrender: function(){
        afterComponentLayout: function(){    
        this.loadGrid();
        },
    },
    
    choseHand:function(){
     var win    = this.up('window'),
        form   = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();
       
       
         Ext.Ajax.request({
            url: 'index.php?r=contacts/AddOwnerToLot', 
            params: { 
                LOT_ID: values['lot_id'],
                OWNER_ID : values['owner'],
            },
            headers: {
                'Accept': 'application/json'
            },
            success: function(response){
                // responseText should be in json format
                record = response.responseText;
                
         
          var params = {'LOT_ID':values['lot_id'],
                       'limit':	25,
                       'page':	1,
                       'start':0
                    };
        
        var GreedStreet = Ext.widget('ownerlist');
        GreedStreet.getStore('Contatcs.LotOwners').getProxy().extraParams = params;
        GreedStreet.getStore('Contatcs.LotOwners').reload();
               
            }
            
        }); 
    },
    loadGrid:function(){
        
       
       //var win    = this.up('window'),
    var form   = this.down('form'),
        record = form.getRecord(),
        values = form.getValues();
        
        console.log('loadGrid afterrender '+values['lot_id']);
        
        var params = {'LOT_ID':values['lot_id'],
                           'limit': 25,
                           'page':1,
                           'start':0
                    };
        
        var GreedStreet = Ext.widget('ownerlist');
        GreedStreet.getStore('Contatcs.LotOwners').getProxy().extraParams = params;
        //load
        GreedStreet.getStore('Contatcs.LotOwners').reload();
        
        
    },
     addresDone:function(){
        action:'chose';
        
     }
    
    
});