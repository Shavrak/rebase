/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('PYA.view.SearchPanel.ComonPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.comonsearch',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: true,
                url:'index.php?r=room/indexjson',
                layout:'hbox',
                //method:'POST',
                fieldDefaults: {
                    labelAlign: 'right',
                    labelWidth: 82,
                  //  msgTarget: 'side'
                },
                 defaults: {
                    border: false,
                    xtype: 'panel',
                    flex: 1,
                    layout: 'anchor'
                },
                items: [{
//                    columnWidth:.3,
//                    layout: 'column',
//                     bodyStyle:'padding:5px 0 5px 5px',
                    items:[ 
                            
                            
//                             Ext.create('Ext.ux.form.field.BoxSelect', {
//                              fieldLabel: 'Направление',
//                                displayField: 'name',
//                                valueField: 'abbr',
//                                width: 300,
//                                //labelWidth: 130,
//                                name:'DIRECTON',
//                                delimiter : "|",
//                                emptyText: 'Направление',
//                                multiSelect : true,
//                                store: Ext.create('Ext.data.Store', {
//                                        fields: ['abbr', 'name'],
//                                        data : [
//                                            {"abbr":"AL", "name":"Север"},
//                                            {"abbr":"AK", "name":"Запад"},
//                                            {"abbr":"AZ", "name":"Восток"}
//                                            //...
//                                        ]
//                                    }),
//                                queryMode: 'local'
//                            }),
                            Ext.create('Ext.ux.form.field.BoxSelect', {
                                fieldLabel: 'Метро',
                                displayField: 'VARCHAR_VALUE',
                                 valueField: 'ID',
                                 queryMode: 'remote',
                                 triggerOnClick: false,
                                 name:'subway_id',
                                 width: 300,
                                 emptyText: 'Метро',
                                 store:'Address.Subways',
                                 listConfig: {
                                          loadingText: 'Поиск...',

                                          // Custom rendering template for each item
                                          getInnerTpl: function() {
                                              return '{VARCHAR_VALUE}';
                                          }
                                      },
                                      pageSize: 10
                             })
                         ]}
                       ,{
//                    columnWidth:.3,
//                    layout: 'column',
//                     bodyStyle:'padding:5px 0 5px 5px',
                    
                    items:[ 
                            {xtype:'textfield', fieldLabel: 'цена от', name: 'PRICE_FROM', emptyText: 'от' },
                            {xtype:'textfield', fieldLabel: 'цена до', name: 'PRICE_TO', emptyText: 'до'},
                            {
                                    xtype: 'combobox',
                                    name: 'rent_curr_id',
                                    typeAhead: true,
                                    selectOnFocus: true,
                                    fieldLabel: 'Валюта',
                                    //queryMode: 'local',
                                    displayField: 'VARCHAR_VALUE',
                                    valueField: 'ID',
                                    
                                    // width:75,
                                    emptyText: 'Валюта',
                                    allowBlank: false,
                                    //margin: '0 5 0 5',
                                    store:'Handbooks.CurrencyList',
                             
                                   },
                               {
                                    xtype: 'combobox',
                                    name: 'PRICE_TYPE',
                                    typeAhead: true,
                                    selectOnFocus: true,
                                    fieldLabel: 'Вид цены',
                                    displayField: 'TYPE_NAME',
                                    valueField: 'ID',
                                    emptyText: 'Вид цены',
                                    allowBlank: false,
                                    store:'Handbooks.PriceType',
                             
                                   },    
//                            {         xtype: 'combo',
//                                    //width:450,
//                                   store: Ext.create('Ext.data.Store', {
//                                        fields: ['id', 'name'],
//                                        data : [
//                                            {"id":"1", "name":"RUB"},
//                                            {"id":"2", "name":"USD"},
//                                            {"id":"3", "name":"EUR"}
//                                            
//                                        ]
//                                    }),
//                                    displayField: 'name',
//                                    valueField: 'id',
//                                    typeAhead: true,
//                                    editable:true,
//                                    mode:'local',
//                                    value:"1",
//                                    name:'CURRENCY',
//                                    forceSelection: true,
//                                    triggerAction: 'all',
//                                    fieldLabel: 'Валюта',
//                                    selectOnFocus: true
//
//                                }
                         ]},{
//                    columnWidth:.3,
//                    layout: 'column',
//                     bodyStyle:'padding:5px 0 5px 5px',
                    items:[ 
                            {xtype:'textfield', fieldLabel: 'площадь от', name: 'AREA_FROM', emptyText: 'от' },
                            {xtype:'textfield', fieldLabel: 'площадь до', name: 'AREA_TO', emptyText: 'до',},
                            {         xtype: 'combo',
                                    //width:450,
                                    store: Ext.create('Ext.data.Store', {
                                        fields: ['id', 'name'],
                                        data : [
                                            {"id":"1", "name":"A+"},
                                            {"id":"2", "name":"A"},
                                            {"id":"3", "name":"B"}
                                            //...
                                        ]
                                    }),
                                    displayField: 'name',
                                    valueField: 'id',
                                    typeAhead: true,
                                    editable:true,
                                    mode:'local',
                                    name:'CLASS_ID',
                                    forceSelection: true,
                                    triggerAction: 'all',
                                    fieldLabel: 'Класс',
                                    emptyText: 'Класс',
                                    selectOnFocus: true

                                }
                         ]}
                     ]
                }];

        this.buttons = [
            {
                text: 'Искать',
                action: 'search'
            }
        ];

        this.callParent(arguments);
    }
});
