// /scripts/app/view/user/Add.js
Ext.define('PYA.view.Elevators.Elevator', {
    extend: 'Ext.window.Window',
    alias: 'widget.elevator',
    title: 'Лифт',
    layout: 'fit',
    autoShow: true,
    store:'Elevators.Elevator',
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                bodyStyle: {
                    background: 'none',
                    padding: '10px',
                    border: '0'
                },
                items: [
                    
                    {
                        xtype: 'combobox',
                        name : 'elevator_brand_id',
                        fieldLabel: 'Марка',
                        store: this.store,
                        queryMode: 'local',
                        displayField: 'name',
                        valueField: 'id'
                    },
                    {
                        xtype: 'textfield',
                        name : 'elevator_count',
                        fieldLabel: 'Кол-во'
                    },
                    {
                        xtype: 'textfield',
                        name : 'elevator_load',
                        fieldLabel: 'г/п'
                    },
                ]
            }
        ];
        this.buttons = [
            {
                text: 'Выбрать',
                action: 'create'
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];
        this.callParent(arguments);
    }
});
 

