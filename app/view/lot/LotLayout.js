Ext.define('PYA.view.lot.LotLayout' ,{
    extend: 'Ext.tab.Panel',
    alias: 'widget.LotLayout',

    // title: 'Лоты',
    layout: 'border',
    border: false,
    //xtype: 'tabpanel',
    activeTab: 0,
   
     //itemId: 'tabs',
    
    initComponent: function() {
         this.items=[
             
                    {
                    title: 'Объекты',
                    xtype:'panel',
                    style: 'padding: 10px;',
                    border: false,
                    layout: 'border',
                    items:[
                         {  
                            region:'north'
                            ,height:150
                            ,xtype: 'searchlot'
                          }
                          
                        ,{ 
                           region:'center'
                          ,xtype: 'lotlist'

                        }
                        
                       ,{
                           xtype: 'tabpanel',
                           activeTab: 0,
                           region:'south',
                           autoScroll : 1,
                           height:200,
                           items: [
                               {
                               title: 'Инфраструктура',
                               xtype : 'detailpanel'
                              }
                              ,
                              {
                               title: 'Общая информация',
                               html: '<div class="portlet-content">\n\
                                   <h1>The third tab group only has a single entry.<br>This is to test the tab being tagged with both "first" and "last" classes to ensure rounded corners are applied top and bottom</h1>\n\
                                   <h1>The third tab group only has a single entry.<br>This is to test the tab being tagged with both "first" and "last" classes to ensure rounded corners are applied top and bottom</h1>\n\
                                   <h1>The third tab group only has a single entry.<br>This is to test the tab being tagged with both "first" and "last" classes to ensure rounded corners are applied top and bottom</h1>\n\
                                   <h1>The third tab group only has a single entry.<br>This is to test the tab being tagged with both "first" and "last" classes to ensure rounded corners are applied top and bottom</h1>\n\
                                   <h1>The third tab group only has a single entry.<br>This is to test the tab being tagged with both "first" and "last" classes to ensure rounded corners are applied top and bottom</h1>\n\
                                   <h1>The third tab group only has a single entry.<br>This is to test the tab being tagged with both "first" and "last" classes to ensure rounded corners are applied top and bottom</h1>\n\
                                   <h1>The third tab group only has a single entry.<br>This is to test the tab being tagged with both "first" and "last" classes to ensure rounded corners are applied top and bottom</h1>\n\
                                   <h1>The third tab group only has a single entry.<br>This is to test the tab being tagged with both "first" and "last" classes to ensure rounded corners are applied top and bottom</h1>\n\
                                   <h1>The third tab group only has a single entry.<br>This is to test the tab being tagged with both "first" and "last" classes to ensure rounded corners are applied top and bottom</h1>\n\
                                   <h1>The third tab group only has a single entry.<br>This is to test the tab being tagged with both "first" and "last" classes to ensure rounded corners are applied top and bottom</h1>\n\
                                   <h1>The third tab group only has a single entry.<br>This is to test the tab being tagged with both "first" and "last" classes to ensure rounded corners are applied top and bottom</h1>\n\
                                   <h1>The third tab group only has a single entry.<br>This is to test the tab being tagged with both "first" and "last" classes to ensure rounded corners are applied top and bottom</h1>\n\
                                    \n\
                                  </div>'
                               }
                         ]
                        }
                    ]
                }
               
      ];
       
        this.callParent(arguments);
    }
         
     
});