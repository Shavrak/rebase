Ext.define('PYA.view.lot.DetailPanel' ,{
    extend: 'Ext.panel.Panel',
    alias: 'widget.detailpanel',
    //store: 'ComonInfo',
    
    initComponent: function() {
        //this.tpl = new Ext.XTemplate('Title:{name}<br/>');
    //    this.html = this.startingMarkup;
        this.items=[{ 
            xtype:'dataview',
            store: this.store,
            itemSelector: 'active',
            html:'this',
            tpl: new Ext.XTemplate(
                      '<tpl for=".">',
                        
                          '<br><b>Метро :</b>{METRO_SATATION}',
                          '<br><b>Направление :</b>{DIRECTON}',
                        
                      '</tpl>'
            )
            
        }]
        this.callParent();
    }
});

