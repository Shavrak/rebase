Ext.define('PYA.view.lot.TabView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.tabsecond',
    
    initComponent: function() {
        //this.tpl = new Ext.XTemplate('Title:{name}<br/>');
    //    this.html = this.startingMarkup;
      // console.log('TabView '+this.getID);
        
        
        Ext.apply(this, {
            dockedItems: [this.createToolbar()],
             tpl: new Ext.create('Ext.XTemplate',
                     '<h2>Lot {LOT}</h2>',
                     '<h2>Адрес : {ADDRESS}</h2>',
                     '<table width="500" border="1">',
                     '<tr><th align="center">#</th><th align="center">Этаж</th>\n\
                        <th align="center">Площадь (кв.м.)</th><th align="center">Брокер</th><th align="center">Стоимость</th>\n\
                      </tr>',
                      '<tpl for="ROOMS">',
                          '<tr>', 
                          '<td align="center">{ID}</td>',
                          '<td align="center">{FLOOR_NUMBER}</td>',
                          '<td align="center">{AREA}</td>',
                          '<td align="center">{BROKER}</td>',
                          '<td align="center">{RENT_PRICES_RUB}  руб.<br>  {RENT_PRICES_USD}  usd.<br>  {RENT_PRICES_EUR}  eur.   </td>',
                          '</tr>',
                      '</tpl>',
                      '</table>'
             )
        });
        this.callParent(arguments);
    

        
    },
            
    /**
     * Create the top toolbar
     * @private
     * @return {Ext.toolbar.Toolbar} toolbar
     */
    createToolbar: function(){
        var items = [],
            config = {};
        if (!this.inTab) {
            items.push({
                scope: this,
                handler: this.openTab,
                text: 'View in new tab',
                iconCls: 'tab-new'
            }, '-');
        }
        else {
            config.cls = 'x-docked-noborder-top';
        }
        items.push({
            scope: this,
            handler: this.goToPost,
            text: 'Go to post',
            iconCls: 'post-go'
        });
        config.items = items;
        return Ext.create('widget.toolbar', config);
    },       
    
});

