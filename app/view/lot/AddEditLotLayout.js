Ext.define('PYA.view.lot.AddEditLotLayout' ,{
    extend: 'Ext.tab.Panel',
    alias: 'widget.AddEditLotLayout',
    layout: 'border',
    border: false,
    activeTab: 0,
    initComponent: function() {
         this.items=[
                    {
                    title: 'Поиск',
                    xtype:'panel',
                    style: 'padding: 10px;',
                    border: false,
                    layout: 'border',
                    items:[
                        { 
                           region:'center'
                          ,xtype: 'comonsearch'
                          //,html:'Поисковые параметры'

                        }]},
             
                    {
                    title: 'Объекты',
                    xtype:'panel',
                    id:1,
                    style: 'padding: 10px;',
                    border: false,
                    layout: 'border',
                    items:[
                        { 
                           region:'center'
                          ,xtype: 'gridlots'

                        }
                        ,{ 
                           region:'east'
                          ,width:300
                          ,xtype: 'lotownerpanel'
                          ,autoScroll : 1
                          

                        }
                       ,{
                           xtype: 'tabpanel',
                           activeTab: 0,
                           region:'south',
                           autoScroll : 1,
                           height:300,
                           
                           items: [
                              {
                               title: 'Помещения',
                               xtype : 'roomspanel'
                              }
                              ,{
                               title: 'Общая информация',
                               xtype : 'comoninfopanel',
                               }
                               
                              ,
                               , {
                               title: 'Инфраструктура',
                               xtype : 'infrastructure'
                              }
                              
                         ]
                        }
                    ]
                }
               
      ];
       
        this.callParent(arguments);
    }
         
     
});