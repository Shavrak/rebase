Ext.define('PYA.view.Handbooks.HandbooksGroups' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.HanbdbooksGroups',

    // title: 'Лоты',
    //region: 'center',
    store: 'HandbooksGroups',
    
    initComponent: function() {
//        this.store = {
//            fields: ['name', 'email'],
//            data  : [
//                {name: 'Ed',    email: 'ed@sencha.com'},
//                {name: 'Tommy', email: 'tommy@sencha.com'}
//            ]
//        };
            // ID, GROUP_NAME, LAST_DATE, LAST_UID
        this.columns = [
                   
            {header: 'ID',  dataIndex: 'ID',   width:45},
            {header: 'Группа',  dataIndex: 'GROUP_NAME',  width:70},
            {header: 'Дата', dataIndex: 'LAST_DATE', flex: 1},
            {header: 'Кто', dataIndex: 'USER_NAME', flex: 1}
            
            
        ];
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            pageSize: 10,
            displayInfo: true,
            displayMsg: 'Показано записей {0} - {1} из {2}',
            emptyMsg: "Нет записей",
            });

        this.callParent(arguments);
    }
});