Ext.define('PYA.view.Handbooks.Handbooks' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.hndb',
    store: 'Handbooks.Handbooks',
    
    initComponent: function() {
            // ID, HANDBOOK, H_GROUP_ID, LAST_DATE, LAST_UID    
        this.columns = [
            {header: 'ID',  dataIndex: 'ID',   width:45},
            {header: 'Справочник',  dataIndex: 'HANDBOOK', width:375,},
            
            {header: 'Кто', dataIndex: 'USER_NAME', width: 1},
            //{header: 'Дата', dataIndex: 'LAST_DATE',width:120, xtype: 'datecolumn', format: "d.m.Y"},
            {header: 'Группа', dataIndex: 'H_GROUP_ID',  width:1},
        ];
        
        this.tbar = [
                ,{
                 text: 'Новый '
                 ,tooltip: 'Новый '
                 ,iconCls: 'icon-add'
                 ,action: 'new'
                }
//               ,'-'
//              ,{
//                 text: 'Удалить '
//                 ,tooltip: 'Удалить '
//                 ,iconCls: 'icon-delete'
//                 ,action: 'delHndb'
//                }

                ];
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            pageSize: 10,
            displayInfo: true,
            displayMsg: 'Показано {0} - {1} из {2}',
            emptyMsg: "Нет записей",
            });

        this.callParent(arguments);
    }
});