Ext.define('PYA.view.Handbooks.HandbooksValues' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.hndbValues',
    // title: 'Лоты',
    //region: 'center',
    store: 'Handbooks.HandbooksValues',
    
    initComponent: function() {
            // ID, HANDBOOK_ID, INDEX_ID, VARCHAR_VALUE, INT_VALUE, DATE_VALUE, ORDER_ID, LAST_DATE, LAST_UID 
        this.columns = [
            {header: 'Порядковый',  dataIndex: 'ID',   width:75},
            {header: 'Значение строка',  dataIndex: 'VARCHAR_VALUE',  flex: 1}
//            {header: 'Значение число', dataIndex: 'INT_VALUE',  width:45},
//            {header: 'Значение дата', dataIndex: 'DATE_VALUE', flex: 1},
//            {header: 'Порядок', dataIndex: 'ORDER_ID', flex: 1},
//            {header: 'Дата', dataIndex: 'LAST_DATE', flex: 1},
//            {header: 'Кто', dataIndex: 'LAST_UID', flex: 1}
            
            
        ];
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            pageSize: 10,
            displayInfo: true,
            displayMsg: 'Показано {0} - {1} из {2}',
            emptyMsg: "Нет записей",
            });
            
            this.tbar = [
                ,{
                 text: 'Создать '
                 ,tooltip: 'Создать '
                 ,iconCls: 'icon-add'
                 ,action: 'newValue'
                }
               ,'-'
              ,{
                 text: 'Удалить '
                 ,tooltip: 'Удалить '
                 ,iconCls: 'icon-delete'
                 ,action: 'delValue'
                }

                
            ];    

        this.callParent(arguments);
    }
});