// /scripts/app/view/user/Add.js
Ext.define('PYA.view.Handbooks.HandbooksAdd', {
    extend: 'Ext.window.Window',
    alias: 'widget.handbooksadd',
    title: 'Создать справочник',
    layout: 'fit',
    autoShow: true,
    store:'Handbooks.HandbooksCategory',
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                bodyStyle: {
                    background: 'none',
                    padding: '10px',
                    border: '0'
                },
                items: [
                    {
                        xtype: 'hidden',
                        name : 'HANDBOOK_ID',
                        value:0
                    },
                    {
                        xtype: 'textfield',
                        name : 'HANDBOOK',
                        fieldLabel: 'Справочник'
                    },
                    {
                        xtype: 'combobox',
                        name : 'H_GROUP_ID',
                        fieldLabel: 'Группа',
                        store: this.store,
                        queryMode: 'local',
                        displayField: 'name',
                        valueField: 'id'
                    },
                ]
            }
        ];
        this.buttons = [
            {
                text: 'Save',
                action: 'create'
            },
            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];
        this.callParent(arguments);
    }
});
 

