Ext.define('PYA.view.Handbooks.wHndbValue', {
    extend: 'Ext.window.Window',
    alias: 'widget.whndbvalue',
    title: 'Значение справочника',
    layout: 'fit',
    autoShow: true,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                name:'wHndbvalue',
                items: [
                    {
                        xtype: 'hidden',
                        name : 'VALUE_ID',
                        value:0
                    },
                    {
                        xtype: 'hidden',
                        name : 'HANDBOOK_ID',
                        value:0
                    },
                    {
                        xtype: 'textfield',
                        name : 'H_VALUE',
                        fieldLabel: 'Значение',
                    },
                    
                ]
            }
        ];
        this.buttons = [

            {
                text: 'Сохранить',
                action: 'save'
            },
            {
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});