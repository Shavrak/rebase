<?php
/* @var $this RebaselotController */
/* @var $model REBASELOT */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'rebaselot-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
		<?php echo $form->error($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LOT'); ?>
		<?php echo $form->textField($model,'LOT',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'LOT'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'NAME'); ?>
		<?php echo $form->textField($model,'NAME',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'NAME'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LOT_STATUS_ID'); ?>
		<?php echo $form->textField($model,'LOT_STATUS_ID',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'LOT_STATUS_ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'BROKER_ID'); ?>
		<?php echo $form->textField($model,'BROKER_ID'); ?>
		<?php echo $form->error($model,'BROKER_ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CREATE_DATE'); ?>
		<?php echo $form->textField($model,'CREATE_DATE'); ?>
		<?php echo $form->error($model,'CREATE_DATE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CREATE_UID'); ?>
		<?php echo $form->textField($model,'CREATE_UID'); ?>
		<?php echo $form->error($model,'CREATE_UID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LAST_DATE'); ?>
		<?php echo $form->textField($model,'LAST_DATE'); ?>
		<?php echo $form->error($model,'LAST_DATE'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LAST_UID'); ?>
		<?php echo $form->textField($model,'LAST_UID'); ?>
		<?php echo $form->error($model,'LAST_UID'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->