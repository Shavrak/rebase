<?php
/* @var $this RebaselotController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Rebaselots',
);

$this->menu=array(
	array('label'=>'Create REBASELOT', 'url'=>array('create')),
	array('label'=>'Manage REBASELOT', 'url'=>array('admin')),
);
?>

<h1>Rebaselots</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
