<?php
/* @var $this RebaselotController */
/* @var $model REBASELOT */

$this->breadcrumbs=array(
	'Rebaselots'=>array('index'),
	$model->NAME,
);

$this->menu=array(
	array('label'=>'List REBASELOT', 'url'=>array('index')),
	array('label'=>'Create REBASELOT', 'url'=>array('create')),
	array('label'=>'Update REBASELOT', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete REBASELOT', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage REBASELOT', 'url'=>array('admin')),
);
?>

<h1>View REBASELOT #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'LOT',
		'NAME',
		'LOT_STATUS_ID',
		'BROKER_ID',
		'CREATE_DATE',
		'CREATE_UID',
		'LAST_DATE',
		'LAST_UID',
	),
)); ?>
