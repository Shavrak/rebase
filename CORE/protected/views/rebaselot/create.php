<?php
/* @var $this RebaselotController */
/* @var $model REBASELOT */

$this->breadcrumbs=array(
	'Rebaselots'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List REBASELOT', 'url'=>array('index')),
	array('label'=>'Manage REBASELOT', 'url'=>array('admin')),
);
?>

<h1>Create REBASELOT</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>