<?php
/* @var $this RebaselotController */
/* @var $data REBASELOT */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LOT')); ?>:</b>
	<?php echo CHtml::encode($data->LOT); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('NAME')); ?>:</b>
	<?php echo CHtml::encode($data->NAME); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LOT_STATUS_ID')); ?>:</b>
	<?php echo CHtml::encode($data->LOT_STATUS_ID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BROKER_ID')); ?>:</b>
	<?php echo CHtml::encode($data->BROKER_ID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CREATE_DATE')); ?>:</b>
	<?php echo CHtml::encode($data->CREATE_DATE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CREATE_UID')); ?>:</b>
	<?php echo CHtml::encode($data->CREATE_UID); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('LAST_DATE')); ?>:</b>
	<?php echo CHtml::encode($data->LAST_DATE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LAST_UID')); ?>:</b>
	<?php echo CHtml::encode($data->LAST_UID); ?>
	<br />

	*/ ?>

</div>