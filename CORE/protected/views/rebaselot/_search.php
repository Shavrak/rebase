<?php
/* @var $this RebaselotController */
/* @var $model REBASELOT */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID'); ?>
		<?php echo $form->textField($model,'ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'LOT'); ?>
		<?php echo $form->textField($model,'LOT',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NAME'); ?>
		<?php echo $form->textField($model,'NAME',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'LOT_STATUS_ID'); ?>
		<?php echo $form->textField($model,'LOT_STATUS_ID',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'BROKER_ID'); ?>
		<?php echo $form->textField($model,'BROKER_ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CREATE_DATE'); ?>
		<?php echo $form->textField($model,'CREATE_DATE'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CREATE_UID'); ?>
		<?php echo $form->textField($model,'CREATE_UID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'LAST_DATE'); ?>
		<?php echo $form->textField($model,'LAST_DATE'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'LAST_UID'); ?>
		<?php echo $form->textField($model,'LAST_UID'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->