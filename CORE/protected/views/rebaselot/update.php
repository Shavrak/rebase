<?php
/* @var $this RebaselotController */
/* @var $model REBASELOT */

$this->breadcrumbs=array(
	'Rebaselots'=>array('index'),
	$model->NAME=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List REBASELOT', 'url'=>array('index')),
	array('label'=>'Create REBASELOT', 'url'=>array('create')),
	array('label'=>'View REBASELOT', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage REBASELOT', 'url'=>array('admin')),
);
?>

<h1>Update REBASELOT <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>