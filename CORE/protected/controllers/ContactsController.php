    <?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class ContactsController extends Controller
{
    
    public function actionCompaniesList(){
        
            $model = new COMPANY;
            $criteria = new CDbCriteria;
            
            if (isset($_GET["query"])){
               $criteria->compare('NAME',$_GET["query"],true);   
            }
            //$criteria->compare('HANDBOOK_ID','3');  
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            // results per page
            //$pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
            $companyList = $model->findAll($criteria);
            
            
              if ($companyList!=NULL){  
                 foreach($companyList as $company){
                      $dat['ID']=  $company->ID;  
                      $dat['NAME']=  $company->NAME;
                      $dat['PHONE']=  $company->PHONE;
                      $dat['ADDRESS']=  $company->ADDRESSNAME->ADDRESS;
                      $dat['DESCRIPTION']=  $company->DESCRIPTION;
                      $res[] = $dat;
                    }
                     
              } else{
                      $dat['NAME']=  'Пусто';
                      $dat['PHONE']=  'Пусто';
                      $dat['DESCRIPTION']=  'Пусто';
                      $dat['ADDRESS']=  'Пусто';
                      
                      $res[] = $dat;
              }
                  
                    echo CJSON::encode(array(
                        'success' => true,
                        'company' => $res,
                        'total' => $pages->itemCount
                    ));
        
    }
    
    public function actionAddCompany(){
                $model= new  COMPANY();
                $model->NAME =$_POST['NAME'];
                $model->PHONE =$_POST['PHONE'];
                $model->ADDRESS_ID =$_POST['ADDRESS_ID'];
                $model->DESCRIPTION =$_POST['DESCRIPTION'];
                $model->CREATE_UID =Yii::app()->user->id;
                $model->CREATE_DATE =new CDbExpression('NOW()');
                $model->LAST_UID =Yii::app()->user->id;
                $model->LAST_DATE =new CDbExpression('NOW()');
                if($model->save()){
                    echo "ok";
                }  else {
                echo "ID is -".$model->ID;    
                }
    }
    
    
    public function actionOwnerList(){
            $model = new OWNERS;
            $criteria = new CDbCriteria;
             if (isset($_GET["query"])){
               $criteria->compare('NAME',$_GET["query"],true);   
            }
            
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            if (isset($_GET["limit"]))
             $pages->pageSize=$_GET["limit"];
             $pages->applyLimit($criteria);
             $owners = $model->findAll($criteria);
               
                    
              if ($owners!=NULL){  
                 foreach($owners as $owner){
                      $dat['ID']=  $owner->ID;  
                      $dat['NAME']=  $owner->NAME;
                      $dat['PHONE']=  $owner->PHONE;
                      $dat['COMPANY_NAME']=  $owner->COMPANYS->NAME;
                      $dat['COMPANY_DESC']=  $owner->COMPANYS->DESCRIPTION;
                      $res[] = $dat;
                    }
                     
              } else{
                      $dat['NAME']=  'Пусто';
                      $dat['PHONE']=  'Пусто';
                      $dat['COMPANY_NAME']=  'Пусто';
                      $dat['COMPANY_DESC']=  'Пусто';
                      
                      $res[] = $dat;
              }
                    echo CJSON::encode(array(
                        'success' => true,
                        'own' => $res,
                        'total' => $pages->itemCount
                    ));
        
        
    }
    
    
    public function actionAgentList(){
            $model = new REBASE_AGENT_CONTACTCTS;
            $criteria = new CDbCriteria;
             if (isset($_GET["query"])){
               $criteria->compare('NAME',$_GET["query"],true);   
            }
            
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            if (isset($_GET["limit"]))
             $pages->pageSize=$_GET["limit"];
             $pages->applyLimit($criteria);
             $owners = $model->findAll($criteria);
               
                    
              if ($owners!=NULL){  
                 foreach($owners as $owner){
                      $dat['ID']=  $owner->ID;  
                      $dat['NAME']=  $owner->NAME;
                      $dat['PHONE']=  $owner->PHONE;
                      $dat['COMPANY_NAME']=  $owner->COMPANYS->NAME;
                      $dat['COMPANY_DESC']=  $owner->COMPANYS->DESCRIPTION;
                      $res[] = $dat;
                    }
                     
              } else{
                      $dat['NAME']=  'Пусто';
                      $dat['PHONE']=  'Пусто';
                      $dat['COMPANY_NAME']=  'Пусто';
                      $dat['COMPANY_DESC']=  'Пусто';
                      
                      $res[] = $dat;
              }
                    echo CJSON::encode(array(
                        'success' => true,
                        'own' => $res,
                        'total' => $pages->itemCount
                    ));
        
        
    }
    
    public function actionAddAgent(){
                $model= new  REBASE_AGENT_CONTACTCTS();
                $model->NAME =$_POST['NAME'];
                $model->PHONE =$_POST['PHONE'];
                $model->AGENT_TYPE_ID =$_POST['AGENT_TYPE'];
                $model->COMPANY_ID =$_POST['COMPANY_ID'];
                $model->CREATE_UID =Yii::app()->user->id;
                $model->CREATE_DATE =new CDbExpression('NOW()');
                $model->LAST_UID =Yii::app()->user->id;
                $model->LAST_DATE =new CDbExpression('NOW()');
                if($model->save()){
                    echo "ok";
                }  else {
                echo "ID is -".$model->ID;    
                }
    }
    
      public function actionListOwnersOfLot()
        {           
            $model = new LOTOWNERS;
            $criteria = new CDbCriteria;
              $lot_id = $_GET["LOT_ID"];
            if (!$lot_id) { 
                $criteria->compare('LOT_ID','-1');
            }
            else {
              $criteria->compare('LOT_ID',$lot_id);
            }
            
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            if (isset($_GET["limit"]))
             $pages->pageSize=$_GET["limit"];
             $pages->applyLimit($criteria);
             $owners = $model->findAll($criteria);
              if ($owners!=NULL){  
                 foreach($owners as $owner){
                      $dat['ID']=  $owner->ID;  
                      $dat['NAME']=  $owner->OWNERS->NAME;
                      $dat['PHONE']=  $owner->OWNERS->PHONE;
                      $COMPANYS=COMPANY::model()->findByPk($owner->OWNERS->COMPANY_ID);
                      $dat['COMPANY_NAME']=  $COMPANYS->NAME;
                      $dat['COMPANY_DESC']=  $COMPANYS->DESCRIPTION;
                      $res[] = $dat;
                    }
                     
              } else{
                      $dat['NAME']=  'Пусто';
                      $dat['COMPANY_NAME']= 'Пусто';
                      $res[] = $dat;
              }
                  
                    echo CJSON::encode(array(
                        'success' => true,
                        'own' => $res,
                        'total' => $pages->itemCount
                    ));
          
            
         }
    
    public function actionAddOwner(){
                $model= new  OWNERS();
                $model->NAME =$_POST['NAME'];
                $model->PHONE =$_POST['PHONE'];
                $model->COMPANY_ID =$_POST['COMPANY_ID'];
                $model->CREATE_UID =Yii::app()->user->id;
                $model->CREATE_DATE =new CDbExpression('NOW()');
                $model->LAST_UID =Yii::app()->user->id;
                $model->LAST_DATE =new CDbExpression('NOW()');
                if($model->save()){
                    echo "ok";
                }  else {
                echo "ID is -".$model->ID;    
                }
    }
    
    public function actionAddOwnerToLot(){
                $model= new LOTOWNERS();
                //$model->ID = 101;
                $model->LOT_ID =$_POST['LOT_ID'];
                $model->OWNER_ID =$_POST['OWNER_ID'];
                $model->CREATE_UID =Yii::app()->user->id;
                $model->CREATE_DATE =new CDbExpression('NOW()');
                $model->LAST_UID =Yii::app()->user->id;
                $model->LAST_DATE =new CDbExpression('NOW()');
                if($model->save()){
                    echo "ok";
                }  else {
                echo "ID is -".$model->ID;    
                }
        
    }
    
    public function actionRemoveOwnerToLot(){
              //$model = new LOTOWNERS;
              $criteria = new CDbCriteria;
              //$criteria->compare('LOT_ID',$_POST['LOT_ID']);  
              //$criteria->compare('ID',$_POST['ID']);  
              $owners = LOTOWNERS::model()->deleteByPk($_POST['ROW_ID']);
              //$model->delete();
              echo "ok";
        
        
    }
    
    
    /*
     * Сохранение связи договора с собственником
     * Возащаю id данной связи, для использования для сохранения помещений
     * Данная связь может быть для операции продажи и аренды
     */
    public function actionSaveOwnerContract(){
         $contract = new REBASE_CONTRACTS();
         
         $contract_num    =$_POST['CONTARCT_NUM'];
         $contract_dt_sg  =$_POST['DATE_OF_SIGNING'];
         $contract->CONTARCT_NUM         =$contract_num;
         $contract->DATE_OF_SIGNING      =$contract_dt_sg;
         $contract->DATE_OF_END          =$_POST['DATE_OF_END'];
         $contract->CONTRACT_TYPE_ID     =$_POST['CONTRACT_TYPE_ID'];
         $contract->BROKER_ID            =$_POST['BROKER_ID'];       
         $contract->PECRENT_FROM_CONTRACT=$_POST['PECRENT_FROM_CONTRACT'];
         $contract->NOTE                 =$_POST['NOTE'];       
         $contract->CREATE_UID           =Yii::app()->user->id;
         $contract->CREATE_DATE          =new CDbExpression('NOW()');
         $contract->LAST_UID             =Yii::app()->user->id;
         $contract->LAST_DATE            =new CDbExpression('NOW()');
         $contract->save();

        
        $owner_contract = new REBASE_OWNERS_CONTRACTS();
        $owner_contract->OWNER_ID =$_POST['OWNER_ID'];
        $owner_contract->CONTRACT_ID =$contract->ID;
        $owner_contract->IS_RENT =$_POST['IS_RENT'];
        $owner_contract->CREATE_UID =Yii::app()->user->id;
        $owner_contract->CREATE_DATE =new CDbExpression('NOW()');
        $owner_contract->LAST_UID =Yii::app()->user->id;
        $owner_contract->LAST_DATE =new CDbExpression('NOW()');
        $owner_contract->save();
        $owner_contract_id = $owner_contract->ID;
        
        //$owner_contract_id = 'test';
        echo CJSON::encode(array(
                        'success' => true,
                        'owner_contract' => $owner_contract_id,
                        'contract_num' => $contract_num,
                        'contract_dt_sg'=>$contract_dt_sg,
                        
                    ));
        //echo "{success:true, data:{owner_contract:$owner_contract_id, contract_num:$contract_num, contract_dt_sg:$contract_dt_sg }}";    
    }
    
    public function actionAddOwnerToRoom(){
        
    }
    /// Инвесторы
    
    public function actionInvestorList(){
        
    }
    
    public function actionAddInvestor(){
        
    }

    public function actionAddInvestorToLot(){
        
    }

    
    /// агент
    
    
    
    
}
?>
