<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class AddressController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    
    /*Сохранение адреса лота*/  
    public function actionAddStreetToLot()
	{       
                //$iRequestBody = json_decode(file_get_contents('php://input'), true);
                $model= new LOTADDRESSES();
                //$model->ID = 101;
                $model->LOT_ID =$_POST['LOT_ID'];
                $model->ADDRESS_ID =$_POST['ADDRESS_ID'];
                $model->CREATE_UID =Yii::app()->user->id;
                $model->CREATE_DATE =new CDbExpression('NOW()');
                $model->LAST_UID =Yii::app()->user->id;
                $model->LAST_DATE =new CDbExpression('NOW()');
                if($model->save()){
                    echo "ok";
                }  else {
                echo "ID is -".$model->ID;    
                }
                
	}
        
        
    public function actionListStreetsOfLot()
        {           
            $model = new LOTADDRESSES;
            $criteria = new CDbCriteria;
            
            $lot_id = $_GET["LOT_ID"];
            if (!$lot_id) { 
                $criteria->compare('LOT_ID','-1');
            }
            else {
              $criteria->compare('LOT_ID',$lot_id);
            }

            $count=$model->count($criteria);
            $pages=new CPagination($count);
            if (isset($_GET["limit"]))
             $pages->pageSize=$_GET["limit"];
             $pages->applyLimit($criteria);
             $streets = $model->findAll($criteria);
                
                 
                    
                if ($streets!=NULL){  
                 foreach($streets as $street){
                         $dat['ID']=  $street->ID;  
                         $dat['ADDRESS']=  $street->StrName->ADDRESS;
                         $res[] = $dat;
                    }
                     
              } else{
                      $dat['NAME']=  'Пусто';
                      $dat['ADDRESS']=  'Пусто';
                      
                      $res[] = $dat;
              }
                    
                    echo CJSON::encode(array(
                        'success' => true,
                        'lotstreets' => $res,
                        'total' => $pages->itemCount
                    ));
         }
        
        
        public function actionlistDistricts()
        {           
            $model = new HANDBOOKVALUES;
            $criteria = new CDbCriteria;
            
            if (isset($_GET["query"])){
               $criteria->compare('VARCHAR_VALUE',$_GET["query"],true);   
            }
            $criteria->compare('HANDBOOK_ID','87');  
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            // results per page
            //$pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
            $handbooks = $model->findAll($criteria);
                    echo CJSON::encode(array(
                        'success' => true,
                        'vls' => $handbooks,
                        'total' => $pages->itemCount
                    )); 
         }
         
         
        public function actionlistLocations()
        {           
            $model = new HANDBOOKVALUES;
            $criteria = new CDbCriteria;
            
            if (isset($_GET["query"])){
              $criteria->compare('VARCHAR_VALUE',$_GET["query"],true);  
            }
            $criteria->compare('HANDBOOK_ID','66');  
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            // results per page
            //$pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
            $handbooks = $model->findAll($criteria);
                    echo CJSON::encode(array(
                        'success' => true,
                        'vls' => $handbooks,
                        'total' => $pages->itemCount
                    )); 
         }
        
        public function actionlistDirectons()
        {           
            $model = new HANDBOOKVALUES;
            $criteria = new CDbCriteria;
            
            if (isset($_GET["query"])){
              $criteria->compare('VARCHAR_VALUE',$_GET["query"],true);   
            }
            $criteria->compare('HANDBOOK_ID','11');  
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            // results per page
            //$pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
            $handbooks = $model->findAll($criteria);
                    echo CJSON::encode(array(
                        'success' => true,
                        'vls' => $handbooks,
                        'total' => $pages->itemCount
                    )); 
         }
         
       public function actionListStreets()
        {           
            $model = new FIASADDRESS;
            $criteria = new CDbCriteria;
            
            if (isset($_GET["query"])){
              $criteria->compare('ADDRESS',$_GET["query"],true);  
            }
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            // results per page
            //$pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
            $handbooks = $model->findAll($criteria);
                    echo CJSON::encode(array(
                        'success' => true,
                        'vls' => $handbooks,
                        'total' => $pages->itemCount
                    )); 
           }
        
        public function actionListSubway()
        {           
            $model = new HANDBOOKVALUES;
            $criteria = new CDbCriteria;
            
            if (isset($_GET["query"])){
              $criteria->compare('VARCHAR_VALUE',$_GET["query"],true);  
            }
            $criteria->compare('HANDBOOK_ID','2');  
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            // results per page
            //$pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
            $handbooks = $model->findAll($criteria);
                    echo CJSON::encode(array(
                        'success' => true,
                        'vls' => $handbooks,
                        'total' => $pages->itemCount
                    )); 
           }
           
            public function actionListRailway()
        {           
            $model = new HANDBOOKVALUES;
            $criteria = new CDbCriteria;
            
            if (isset($_GET["query"])){
              $criteria->compare('VARCHAR_VALUE',$_GET["query"],true);  
            }
            $criteria->compare('HANDBOOK_ID','101');  
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            // results per page
            //$pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
            $handbooks = $model->findAll($criteria);
                    echo CJSON::encode(array(
                        'success' => true,
                        'vls' => $handbooks,
                        'total' => $pages->itemCount
                    )); 
           }
           
           
         /*сохранение ориентиров*/
        public function actionSaveLandmarks(){
            $model = new LANDMARKS;
            $lot_id = $_POST['lot_id'];
            $subways = $_POST['subway_id'];
            $railways = $_POST['railway_id'];
            
            $sbw_min_walk = $_POST['sbw_min_walk'];
            $sbw_min_auto = $_POST['sbw_min_auto'];
            $railway_min_walk = $_POST['railway_min_walk'];
            $railway_min_auto = $_POST['railway_min_auto'];
            $district_id = $_POST['district_id'];
            $direction_id = $_POST['direction_id'];
            $location_id = $_POST['location_id'];
            $za_mkad = $_POST['za_mkad'];
            $description_loaction = $_POST['description_loaction'];
            $za_mkad_value=0;
            if ($za_mkad =='on')$za_mkad_value=1;
              
            
            
            
            $model->LOT_ID = $lot_id;
            $model->METRO_AUTO_MIN = $sbw_min_auto;
            $model->METRO_WALK_MIN = $sbw_min_walk;
            $model->RAILWAY_AUTO_MIN = $railway_min_auto;
            $model->RAILWAY_WALK_MIN = $railway_min_walk;
            $model->DIRECTION_ID = $direction_id;
            $model->BUSINESS_DISTRICT_ID = $district_id;
            $model->LOCATION_ID = $location_id;
            $model->NOTE= $description_loaction;
            $model->ZA_MKAD= $za_mkad_value;
            $model->CREATE_UID =Yii::app()->user->id;
            $model->CREATE_DATE =new CDbExpression('NOW()');
            $model->LAST_UID =Yii::app()->user->id;
            $model->LAST_DATE =new CDbExpression('NOW()');
            $model->save();
                    
            
            if (isset($subways)) {
               $metro_ids =array_map('trim',explode(",",$subways));
               $this->AddMetroToLot($metro_ids, $lot_id);
            }
            if (isset($railways)) {
             $railway_ids =array_map('trim',explode(",",$railways));
             $this->RailwayToLot($railway_ids, $lot_id);
            }
            
           echo "{success:true}";    
        }
       
        public function AddMetroToLot($metro_ids, $lot_id){
            $model = new METROSTATIONS; 
            foreach ($metro_ids as $metro_id ){
                        
                        $model->ID = NULL;
                        $model->LOT_ID = $lot_id;
                        $model->METRO_STATION_ID = $metro_id;
                        $model->CREATE_UID =Yii::app()->user->id;
                        $model->CREATE_DATE =new CDbExpression('NOW()');
                        $model->LAST_UID =Yii::app()->user->id;
                        $model->LAST_DATE =new CDbExpression('NOW()');
                        $model->isNewRecord = true;
                        $model->save();
                    }
                
        }
        
         public function RailwayToLot($railway_ids, $lot_id){
             
             foreach ($railway_ids as $railway_id ){
                 $model = new RAILWAYSTATIONS;
                 $model->LOT_ID = $lot_id;
                 $model->RAILWAY_STATION_ID = $railway_id;
                 $model->CREATE_UID =Yii::app()->user->id;
                 $model->CREATE_DATE =new CDbExpression('NOW()');
                 $model->LAST_UID =Yii::app()->user->id;
                 $model->LAST_DATE =new CDbExpression('NOW()');
                 $model->save();
             }
             
                 echo "{success:true }";    
        }
        
}
?>
