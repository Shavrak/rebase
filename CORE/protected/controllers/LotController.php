<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class LotController extends Controller
{
      public function actionTest(){
          $area_from = 10;
          $area_to   = 110;
          
          
          
          $user = Yii::app()->db->createCommand()
                ->select('lot_id')
                ->from('LOT_ROOMS_AREA')
                ->where('area between '.$area_from.' and '.$area_to)
                ->query();
            //print_r($user);
            $i=0;
            foreach($user as $usess){
                print_r($usess);
                $i++;
            }
//            
//            select lot_id from myrebase.LOT_ROOMS_AREA
//            where area_from <= 50
//             and area_to >= 110
//             union
//            select x.lot_id from myrebase.LOT_ROOMS_AREA x
//            where x.area between 50 and 110	
          
      }	
     public function actionLotList(){
            $model = new REBASELOT;
            $metro = new METROSTATIONS;
            $landmark = new LANDMARKS;
            $address = new LOTADDRESSES;
            $building_data = new LOTBUILDINGSDATA;
            $commercial = new LOTCOMMERCIALTERMS;
            $roomsarea  = new LOTROOMSAREA();
            $prices = new PRICES;
            
            $dat = null;
            
            $mt=null;
            $sbw = array();  
            $ar = array();  
            $prc = array();  
            $criteria = new CDbCriteria;
            $sbw_criteria = new CDbCriteria;
            $price_criteria = new CDbCriteria;
            
            $PRICE_FROM =null;
            $PRICE_TO =null;
            $area_from = null;
            $area_to   = null;
            $subways = null;
            if (isset($_GET["AREA_FROM"])) 
              $area_from = $_GET['AREA_FROM'];
            if (isset($_GET["AREA_TO"])) 
              $area_to   = $_GET['AREA_TO'];
            if (isset($_GET["PRICE_FROM"])) 
               $PRICE_FROM = $_GET["PRICE_FROM"];
            if (isset($_GET["PRICE_TO"])) 
                $PRICE_TO =  $_GET["PRICE_TO"];
            if (isset($_GET["subway_id"])) 
              $subways = $_GET['subway_id'];

            
             
             //логика для поиска
            if ($subways!=null) {
               $metro_ids =array_map('trim',explode(",",$subways));
               //print_r($metro_ids);  
               $sbw_criteria->addInCondition('METRO_STATION_ID',$metro_ids);
               $lot_ids = $metro->findAll($sbw_criteria);
            
               foreach($lot_ids as $l)
                   $sbw[] =  $l->LOT_ID;
                 
               }
            
            if ($PRICE_FROM!=null || $PRICE_TO!=null ) {
                 $PRICE_TYPE   = $_GET['PRICE_TYPE'];
                   if ($PRICE_TO ==null) $PRICE_TO = $PRICE_FROM;
                   if ($PRICE_FROM ==null) $PRICE_FROM = 0;
                   
                   if ($_GET['rent_curr_id']== 4930){
                     $column = 'PRICE_RUB';
                   }elseif ($_GET['rent_curr_id']== 4931){
                     $column = 'PRICE_USD';
                   }elseif ($_GET['rent_curr_id']== 4932){
                     $column = 'PRICE_EUR';
                   }
                   $price_criteria->addBetweenCondition($column, $PRICE_FROM, $PRICE_TO);
                   $price_criteria->compare('PRICE_TYPE',2);
                   $price = $prices->findAll($price_criteria);
                   
                   
                    foreach($price as $l)
                        $prc[] =  $l->LOT_ID;
                    
                  
            }
          
            if ($area_from!=null || $area_to!=null ) {  
                //помоему гавно а не код
                //но пока по другому не могу
                //это поиск по интервалам
             
               
                    $areas = Yii::app()->db->createCommand()
                     ->select('LOT_ID')
                     ->from('LOT_ROOMS_AREA')
                     ->where(array('and', 'area_from <='.$area_from, 'area_to >='.$area_to))
                     ->union('select x.lot_id from LOT_ROOMS_AREA x
                             where x.area between '.$area_from.' and '.$area_to)
                     ->query();
               
               
               /*
               
               $areas = Yii::app()->db->createCommand()
                     ->select('LOT_ID')
                     ->from('LOT_ROOMS_AREA')
                     ->where('area between '.$area_from.' and '.$area_to)
                     ->query();
      
               */
                 foreach($areas as $a)
                    $ar[] =  $a['LOT_ID'];
                 
             }
            
            
            //йобанный кастыль
              if($sbw==null ){$sbw =$prc;}
              if($prc==null ) {$prc=$sbw;}
              if($prc==null or $sbw==null) {$prc=$sbw=$ar;}
              
              
            $res_lot =  array_unique(array_intersect($sbw,$prc,$ar));
            if ($res_lot!=null){
                $criteria->addInCondition('ID',$res_lot);
                
            }
            else{
                //$criteria->addInCondition('ID',null);
                $criteria->addCondition('LOT != "Draft"');
                
            }
            //логика отображение результатов
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            // results per page
            $pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
            $lots = $model->findAll($criteria);
             if ($lots!=NULL){ 
               foreach($lots as $lot){
                        $dat['ID']=  $lot->ID;  
                        $dat['LAST_DATE'] = date("d.m.Y",strtotime($lot->LAST_DATE));
                        $roomarea  = $roomsarea->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lot->ID));    
                        $metros    = $metro->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lot->ID));    
                        $landmarks = $landmark->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lot->ID));  
                        $addresses = $address->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lot->ID));  
                        $buildings_data = $building_data->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lot->ID)); 
                        $commercials = $commercial->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lot->ID)); 
                        if ($metros!=NULL){
                            foreach ($metros as  $m)
                               $mt .=  $m->STATION_NAME->VARCHAR_VALUE .', ';
                               $dat['METRO_SATATION'] = $mt;
                               $mt='';
                        }else{
                          $dat['METRO_SATATION'] = 'Пусто';
                        }
                        
                        if(@$landmarks[0]->DIRECTION_NAME->VARCHAR_VALUE !=null){
                          $dat['DIRECTON'] = @$landmarks[0]->DIRECTION_NAME->VARCHAR_VALUE;  
                        }else{
                          $dat['DIRECTON']  = 'пусто';
                        }
                        
                        $dat['LOT']  = $lot->LOT;
                        
                        
                        $dat['NAME']=  $lot->NAME;
                        $dat['ADDRESS']  = @$addresses[0]->StrName->ADDRESS;
                        
                         if (@$commercials[0]->RentPRICES->PRICE_RUB == null) {
                             $dat['PRICE_RUB'] = 0; 
                         }else {
                             $dat['PRICE_RUB'] =$commercials[0]->RentPRICES->PRICE_RUB;
                         };
                         if (@$commercials[0]->RentPRICES->PRICE_USD == null) {
                             $dat['PRICE_USD'] = 0; 
                         }else {
                             $dat['PRICE_USD'] =$commercials[0]->RentPRICES->PRICE_USD;
                         };
                         if (@$commercials[0]->RentPRICES->PRICE_EUR == null) {
                             $dat['PRICE_EUR'] = 0; 
                         }else {
                             $dat['PRICE_EUR'] =$commercials[0]->RentPRICES->PRICE_EUR;
                         };
                        
                        
                        $dat['RENT_WITH_NDS'] = @$commercials[0]->RENT_WITH_NDS_NAME->VARCHAR_VALUE;
                        $dat['AREA'] = @$buildings_data[0]->COMMON_AREA;
                        $dat['ROOMS_AREA_DIAPAZON'] = 'от '. @$roomarea[0]->AREA_FROM .' до '. @$roomarea[0]->AREA_TO .' кв.м.';
                        $dat['READY_VALUE'] =  @$buildings_data[0]->READY_NAME->VARCHAR_VALUE;
                        $dat['BUILDING_CLASS'] =@$buildings_data[0]->CLASS_NAME->VARCHAR_VALUE; 
                        $dat['LOT_PIC'] ='app/store/img/Tverskaya16.jpg';
                        
                        
                        
                        
                        $res[] = $dat;
               }
             }else {
                $criteria1 = new CDbCriteria();
                $criteria1->order = 'ID desc'; 
                $count=$model->count($criteria1);
                $pages=new CPagination($count);
                // results per page
                $pages->pageSize=$_GET["limit"];
                $pages->applyLimit($criteria1);
                $lots = $model->findAll($criteria1);
                foreach($lots as $lot){
                        $dat['ID']=  $lot->ID;  
                        $dat['LAST_DATE'] = date("d.m.Y",strtotime($lot->LAST_DATE));
                        $roomarea = $roomsarea->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lot->ID));    
                        $metros    = $metro->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lot->ID));    
                        $landmarks = $landmark->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lot->ID));  
                        $addresses = $address->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lot->ID));  
                        $buildings_data = $building_data->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lot->ID)); 
                        $commercials = $commercial->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lot->ID)); 
                        if ($metros!=NULL){
                            foreach ($metros as  $m)
                               $mt .=  $m->STATION_NAME->VARCHAR_VALUE .', ';
                               $dat['METRO_SATATION'] = $mt;
                               $mt='';
                        }else{
                          $dat['METRO_SATATION'] = 'Пусто';
                        }
                        
                        if(@$landmarks[0]->DIRECTION_NAME->VARCHAR_VALUE !=null){
                          $dat['DIRECTON'] = @$landmarks[0]->DIRECTION_NAME->VARCHAR_VALUE;  
                        }else{
                          $dat['DIRECTON']  = 'пусто';
                        }
                        
                        $dat['LOT']  = $lot->LOT;
                        $dat['NAME']=  $lot->NAME;
                        $dat['ADDRESS']  = @$addresses[0]->StrName->ADDRESS;
                        
                         if (@$commercials[0]->RentPRICES->PRICE_RUB == null) {
                             $dat['PRICE_RUB'] = 0; 
                         }else {
                             $dat['PRICE_RUB'] =$commercials[0]->RentPRICES->PRICE_RUB;
                         };
                         if (@$commercials[0]->RentPRICES->PRICE_USD == null) {
                             $dat['PRICE_USD'] = 0; 
                         }else {
                             $dat['PRICE_USD'] =$commercials[0]->RentPRICES->PRICE_USD;
                         };
                         if (@$commercials[0]->RentPRICES->PRICE_EUR == null) {
                             $dat['PRICE_EUR'] = 0; 
                         }else {
                             $dat['PRICE_EUR'] =$commercials[0]->RentPRICES->PRICE_EUR;
                         };
                        
                        
                        $dat['RENT_WITH_NDS'] = @$commercials[0]->RENT_WITH_NDS_NAME->VARCHAR_VALUE;
                        $dat['AREA'] = @$buildings_data[0]->COMMON_AREA;
                        $dat['ROOMS_AREA_DIAPAZON'] = 'от '. @$roomarea[0]->AREA_FROM .' до '. @$roomarea[0]->AREA_TO .' кв.м.';
                        $dat['READY_VALUE'] =  @$buildings_data[0]->READY_NAME->VARCHAR_VALUE;
                        $dat['BUILDING_CLASS'] =@$buildings_data[0]->CLASS_NAME->VARCHAR_VALUE; 
                        $dat['LOT_PIC'] ='app/store/img/Buildings/Plfybt_2.jpg';
                        
                        
                        
                        
                        $res[] = $dat;
               }
             }           
            
             echo CJSON::encode(array(
                        'success' => true,
                        'lots' => $res,
                        'total' => $pages->itemCount
                    ));
            
     
    }
    
    public function actionAddLot()
	{   
        
            /*
             * Прежде удаляю черновик этого пользователя и создаю новый 
             */
            $criteria = new CDbCriteria;
            $criteria->compare('LOT_STATUS_ID','2');  
            $criteria->compare('CREATE_UID',Yii::app()->user->id);  
            $criteria->compare('LAST_UID',Yii::app()->user->id);  
            
            
            
            REBASELOT::model()->deleteAll($criteria);
            
                $model = new REBASELOT();
                $model->LOT ='Draft';
                $model->NAME ='Draft';
                $model->LOT_STATUS_ID =2;
                $model->BROKER_ID=Yii::app()->user->id;
                $model->CREATE_UID =Yii::app()->user->id;
                $model->CREATE_DATE =new CDbExpression('NOW()');
                $model->LAST_UID =Yii::app()->user->id;
                $model->LAST_DATE =new CDbExpression('NOW()');
                $model->save();
                echo "{success:true, data:{lot_id:$model->ID}}";    
                //echo "{lot: $model->ID}";    
	}
  
        /*Действие для чтения нового черновика
         * в которм получаю id здания
         */
        public function actionGetDraftLot(){
            
            $model = new REBASELOT;
            $criteria = new CDbCriteria;
                        
            $criteria->compare('LOT_STATUS_ID','2');  
            $criteria->compare('CREATE_UID',Yii::app()->user->id);  
            $criteria->compare('LAST_UID',Yii::app()->user->id);  
            $draftlot = $model->findAll($criteria);  
            $lot_id = $draftlot[0]->ID;
             
             echo "{success:true, data:{lot_id:$lot_id}}";    
            
            
        }
        
        /*Сохраняю новый лот*/
        
        
        public function actionSaveNewLot(){
            
            date_default_timezone_set('Europe/Moscow');
            $criteria           = new CDbCriteria;
            $lot_id             = $_POST['lot_id'];
            $lot                = $_POST['lot'];
            $name               = $_POST['lot_name'];
            $broker_id          = $_POST['broker_id'];
            $lot_status_id      = $_POST['lot_status_id'];
            $create_date        = date('y-m-d', strtotime($_POST['create_date']));
            ////Входные данные
            $developer_id       = $_POST['developer'];
            $investor_id        = $_POST['investor'];
            $project_cost       = $_POST['project_cost'];
            $curr_proj_cost     = $_POST['curr_proj_cost'];
            $exclusive_agent    = $_POST['exclusive_agent'];
            $coexclusive_agent  = $_POST['coexclusive_agent'];
            $manage_company     = $_POST['manage_company'];
            ////Данные по зданию
            $ready_id           = $_POST['ready_id'];
            $ready_dt           = date('y-m-d', strtotime($_POST['ready_dt']));
            $type_id            = $_POST['type_id'];
            $class_id           = $_POST['class_id'];
            ////Площадь
            $comon_area         = $_POST['comon_area'];
            $office_area        = $_POST['office_area'];
            $trade_area         = $_POST['trade_area'];
            $guest_area         = $_POST['guest_area'];
            $intertaiment_area  = $_POST['intertaiment_area'];
            $apartment_area     = $_POST['apartment_area'];
            $live_area          = $_POST['live_area'];
            $wh_area            = $_POST['wh_area'];
            $industrial_area    = $_POST['industrial_area'];
            ////Количество этажей
            $upground_count     = $_POST['upground_count'];
            $undeground_count   = $_POST['undeground_count'];
            $is_mansard         = $_POST['is_mansard'];
            $is_basement        = $_POST['is_basement'];
            $build_year         = date('y-m-d', strtotime($_POST['build_year']));
            $reconstruction_year= date('y-m-d', strtotime($_POST['reconstruction_year']));
            $state_commision    = $_POST['state_commision'];
            $certificate        = $_POST['certificate'];            
            ////Технические характеристики
            $PassangerElevatorId    = $_POST['PassangerElevatorId'];
            $CargoElevatorId        = $_POST['CargoElevatorId'];
            $ceiling_height         = $_POST['ceiling_height'];
            $column_grid            = $_POST['column_grid'];            
            $floor_depth            = $_POST['floor_depth'];            
            $max_floor_area         = $_POST['max_floor_area'];            
            $min_block              = $_POST['min_block'];           
            ////Аренда
            $rent_price             = $_POST['rent_price'];            
            $rent_curr_id           = $_POST['rent_curr_id'];            
            $rent_with_nds          = $_POST['rent_with_nds'];            
            $operating_costs        = $_POST['operating_costs'];
            $operating_curr_id      = $_POST['operating_curr_id'];
            $utility_bills_id       = $_POST['utility_bills_id'];            
            $deposit_id             = $_POST['deposit_id'];            
            $prepayd_value          = $_POST['prepayd_value'];            
            $rent_type_id           = $_POST['rent_type_id'];            
            
            ////Продажа
            $sale_bloks             = $_POST['sale_bloks'];            
            $sale_price             = $_POST['sale_price'];            
            $sale_curr_id           = $_POST['sale_curr_id'];            
            $sale_with_nds          = $_POST['sale_with_nds'];            
            
            $object_cost            = $_POST['object_cost'];            
            $object_curr_id         = $_POST['object_curr_id'];            
            $object_with_nds        = $_POST['object_with_nds'];            
            
            
            //$create_date = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($create_date, 'yyyy-MM-dd'),'short',null);
            $create_date = date('y-m-d', strtotime($create_date));
            REBASELOT::model()->updateByPk($lot_id, 
                    array('LOT'=>$lot,
                          'NAME'=>$name,
                          'BROKER_ID'=>$broker_id,
                          'LOT_STATUS_ID'=>$lot_status_id,
                          'CREATE_DATE'=>$create_date,
                   
                    ));
            
            $price = new PRICES();
            $INPUT_DATA_Model = new LOTINPUTDATA();
            //ID	int(11) PK AI
            $INPUT_DATA_Model->LOT_ID               = $lot_id;
            $INPUT_DATA_Model->DEVELOPER_ID          = $developer_id;
            $INPUT_DATA_Model->INVESTOR_ID          = $investor_id;
             //вставка цены и получение ссылки на значение
            if ($project_cost!=null){
               $INPUT_DATA_Model->PROJECT_PRICES_ID=$price->savePrice($lot_id, 
                                                            1,  
                                                             $curr_proj_cost, 
                                                             $project_cost,
                                                             Yii::app()->user->id    
                                                            );
            }
            $INPUT_DATA_Model->EXCLUSIVE_AGENT_ID   = $exclusive_agent; 
            $INPUT_DATA_Model->COEXCLUSIVE_AGENT_ID = $coexclusive_agent;
            $INPUT_DATA_Model->MANAGE_COMPANY_ID    = $manage_company;
            
            $INPUT_DATA_Model->CREATE_UID           = Yii::app()->user->id;
            $INPUT_DATA_Model->CREATE_DATE          = new CDbExpression('NOW()');
            $INPUT_DATA_Model->LAST_UID             = Yii::app()->user->id;
            $INPUT_DATA_Model->LAST_DATE            = new CDbExpression('NOW()');
            
            $INPUT_DATA_Model->save();        
            
            
            
            
            $BUILDINGS_DATA_Model =  new LOTBUILDINGSDATA();
            //ID	int(11) PK AI
            $BUILDINGS_DATA_Model->LOT_ID               = $lot_id;
            $BUILDINGS_DATA_Model->READY_ID             = $ready_id;
            $BUILDINGS_DATA_Model->READY_DATE           = $ready_dt;
            $BUILDINGS_DATA_Model->BUILDING_TYPE_ID     = $type_id;	
            $BUILDINGS_DATA_Model->BUILDING_CLASS_ID    = $class_id; 
            $BUILDINGS_DATA_Model->COMMON_AREA          = $comon_area;   
            $BUILDINGS_DATA_Model->OFFICE_AREA          = $office_area;
            $BUILDINGS_DATA_Model->TRADE_AREA           = $trade_area;
            $BUILDINGS_DATA_Model->HOTEL_AREA           = $guest_area;
            $BUILDINGS_DATA_Model->APARTAMENT_AREA      = $apartment_area; 
            $BUILDINGS_DATA_Model->LIVE_AREA            = $live_area;
            $BUILDINGS_DATA_Model->WAREHOUSE_AREA       = $wh_area;
            $BUILDINGS_DATA_Model->PRODUCTION_AREA      = $industrial_area;
            $BUILDINGS_DATA_Model->INTERTAIMENT_AREA    = $intertaiment_area;
            $BUILDINGS_DATA_Model->PURE_CELLING_HEIGHT	= $ceiling_height;
            $BUILDINGS_DATA_Model->COLUMN_SPACE         = $column_grid;
            $BUILDINGS_DATA_Model->FLOOR_DEPTH          = $floor_depth;
            $BUILDINGS_DATA_Model->FLOOR_MAX_AREA	= $max_floor_area;
            $BUILDINGS_DATA_Model->MIN_BLOCK            = $min_block;
            $BUILDINGS_DATA_Model->COUNT_UP_FLOORS	= $upground_count;
            $BUILDINGS_DATA_Model->COUNT_DW_FLOORS	= $undeground_count;
            $BUILDINGS_DATA_Model->HAVE_MANSARD         = $is_mansard;
            $BUILDINGS_DATA_Model->HAVE_BASEMENT	= $is_basement;
            $BUILDINGS_DATA_Model->DATE_BUILD           = $build_year;
            $BUILDINGS_DATA_Model->DATE_RECONSTRUCTION	= $reconstruction_year; 
            $BUILDINGS_DATA_Model->STATE_COMMISSION	= $state_commision;
            $BUILDINGS_DATA_Model->OWNERSHIP_CERT	= $certificate;
            $BUILDINGS_DATA_Model->PASSANGER_ELEVATOR_ID = $PassangerElevatorId;  
            $BUILDINGS_DATA_Model->CARGO_ELEVATOR_ID	 = $CargoElevatorId;
            
            $BUILDINGS_DATA_Model->CREATE_UID           = Yii::app()->user->id;
            $BUILDINGS_DATA_Model->CREATE_DATE          = new CDbExpression('NOW()');
            $BUILDINGS_DATA_Model->LAST_UID             = Yii::app()->user->id;
            $BUILDINGS_DATA_Model->LAST_DATE            = new CDbExpression('NOW()');
            $BUILDINGS_DATA_Model->save();
         
            $LOT_COMMERCIAL_TERMS_Model = new  LOTCOMMERCIALTERMS();
//            Columns:
//            ID	int(11) PK 
            $LOT_COMMERCIAL_TERMS_Model->LOT_ID         = $lot_id;
            
            if ($rent_price!=null){
               $LOT_COMMERCIAL_TERMS_Model->RENT_PRICES_ID=$price->savePrice($lot_id, 
                                                            4,  
                                                             $rent_curr_id, 
                                                             $rent_price,
                                                             Yii::app()->user->id    
                                                            );
            }
            $LOT_COMMERCIAL_TERMS_Model->RENT_WITH_NDS       = $rent_with_nds;
            //$LOT_COMMERCIAL_TERMS_Model->OPERATING_COSTS_ID  = $operating_costs;
            if ($operating_costs!=null){
               $LOT_COMMERCIAL_TERMS_Model->OPERATING_PRICES_ID=$price->savePrice($lot_id, 
                                                            5,  
                                                             $operating_curr_id, 
                                                             $operating_costs,
                                                             Yii::app()->user->id    
                                                            );
            }
                
            $LOT_COMMERCIAL_TERMS_Model->UTILITY_BILLS_ID    = $utility_bills_id;
            //$LOT_COMMERCIAL_TERMS_Model->UTILITY_BILLLS_PRICES	int(11) 
            //$LOT_COMMERCIAL_TERMS_Model->PAYMENT_TERMS_ID    
            $LOT_COMMERCIAL_TERMS_Model->DEPOSIT_ID           = $deposit_id;
            $LOT_COMMERCIAL_TERMS_Model->PREPAYD_VALUE        = $prepayd_value;
            //$LOT_COMMERCIAL_TERMS_Model->MIN_RENT_PERIOD_ID
            //$LOT_COMMERCIAL_TERMS_Model->PERIOD_SUBRENT	varchar(45) 
            $LOT_COMMERCIAL_TERMS_Model->RENT_TYPE_ID         = $rent_type_id;  
            $LOT_COMMERCIAL_TERMS_Model->SALE_OF_PARTS        = $sale_bloks;  
            if ($sale_price!=null){
               $LOT_COMMERCIAL_TERMS_Model->SQM_PRICES_ID=$price->savePrice($lot_id, 
                                                            6,  
                                                             $sale_curr_id, 
                                                             $sale_price,
                                                             Yii::app()->user->id    
                                                            );
            }

            if ($object_cost!=null){
               $LOT_COMMERCIAL_TERMS_Model->FULL_SALE_PRICES_ID=$price->savePrice($lot_id, 
                                                             7,  
                                                             $object_curr_id, 
                                                             $object_cost,
                                                             Yii::app()->user->id    
                                                            );
            }            
            $LOT_COMMERCIAL_TERMS_Model->SALE_WITH_NDS        = $sale_with_nds;   
            
/*            $LOT_COMMERCIAL_TERMS_Model->AB_SQM_PRICES_ID	int(11) 
            $LOT_COMMERCIAL_TERMS_Model->AB_FULL_PRICES_ID	int(11) 
            $LOT_COMMERCIAL_TERMS_Model->AB_GAP	varchar(45) 
            $LOT_COMMERCIAL_TERMS_Model->AB_PAYBACK	varchar(45) 
            $LOT_COMMERCIAL_TERMS_Model->AB_PROFITABILITY	varchar(45) 
            $LOT_COMMERCIAL_TERMS_Model->LAND_INFO	varchar(450) 
            $LOT_COMMERCIAL_TERMS_Model->NOTE	varchar(450) 
  */                  
            $LOT_COMMERCIAL_TERMS_Model->CREATE_UID           = Yii::app()->user->id;
            $LOT_COMMERCIAL_TERMS_Model->CREATE_DATE          = new CDbExpression('NOW()');
            $LOT_COMMERCIAL_TERMS_Model->LAST_UID             = Yii::app()->user->id;
            $LOT_COMMERCIAL_TERMS_Model->LAST_DATE            = new CDbExpression('NOW()');
            $LOT_COMMERCIAL_TERMS_Model->save();        
            
                    
                    

            
            
            echo "{success:true, data:{lot_id:$lot_id}}";    
            
            
        }
        

      public function actionEditLot()
	{       
                $model = new REBASELOT();
                $model->LOT ='Draft';
                $model->NAME ='Draft';
                $model->LOT_STATUS_ID =2;
                $model->BROKER_ID=Yii::app()->user->id;
                $model->CREATE_UID =Yii::app()->user->id;
                $model->CREATE_DATE =new CDbExpression('NOW()');
                $model->LAST_UID =Yii::app()->user->id;
                $model->LAST_DATE =new CDbExpression('NOW()');
                $model->save();
                echo "{success:true, lot_id:$model->ID}";    
	}
        
        
        //*справочники
        
        public function actionLotStatus()
        {           
            $model = new HANDBOOKVALUES;
            $criteria = new CDbCriteria;
            
            if (isset($_GET["query"])){
               $criteria->compare('VARCHAR_VALUE',$_GET["query"],true);   
            }
            $criteria->compare('HANDBOOK_ID','3');  
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            // results per page
            //$pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
            $handbooks = $model->findAll($criteria);
                    echo CJSON::encode(array(
                        'success' => true,
                        'vls' => $handbooks,
                        'total' => $pages->itemCount
                    )); 
         }
         
         
         //Новый лифт и его характристики
        public function actionSaveElevator(){
                $elevator = new ELEVATORS();
                $elevator->BRAND_ID=  $_POST['brand_id'];
                $elevator->ELEVATORS_COUNT=  $_POST['elevators_count'];
                $elevator->ELEVATORS_LOAD=  $_POST['elevators_load'];
                 
                $elevator->CREATE_UID =Yii::app()->user->id;
                $elevator->CREATE_DATE=new CDbExpression('NOW()');
                $elevator->LAST_UID=Yii::app()->user->id;
                $elevator->LAST_DATE=new CDbExpression('NOW()');
                
                $elevator->save();
            
            
             echo CJSON::encode(array(
                        'success' => true,
                        'elevator_id' => $elevator->ID,
                        'elevator_type' => $_POST['elevator_type'],
                        'elevators_count' => $_POST['elevators_count'],
                        'elevators_load' => $_POST['elevators_load'],
                        'brand'=>$elevator->BRANDS->VARCHAR_VALUE
                        
                        
                    ));
            
        }

        
        public function actionLotCard(){
            
            $lot_model = new REBASELOT;
            $model = new LOTROOMS;
            $address = new LOTADDRESSES;
            $dat = null;
            $criteria = new CDbCriteria;
            $lot_criteria = new CDbCriteria;
            $LOT_ID = null;
            if (isset($_GET["LOT_ID"]) ){
                $LOT_ID    = $_GET["LOT_ID"];
                $criteria->compare('LOT_ID',$LOT_ID);
                
            }
            $lot_criteria->compare('ID',$LOT_ID);
            $count=$model->count($criteria);
            $pages=new CPagination($count);

            // results per page
//            $pages->pageSize=$_GET["limit"];
//            $pages->applyLimit($criteria);
            
            $lot = $lot_model->findAll($lot_criteria);
            $addresses = $address->findAll($criteria);  
            
            $rooms = $model->findAll($criteria);
                    if ($rooms!=NULL){ 
                        foreach($rooms as $room){
                          $dat['ID']=  $room->ID;  
                          $dat['FLOOR_NUMBER']=  $room->FLOOR_NUMBER;  
                          $dat['AREA']=  $room->AREA; 
                          if ($room->CONTRACT_T!=null)
                             $dat['CONTARCT_TYPE']=  $room->CONTRACT_T->VARCHAR_VALUE;     

                          if ($room->UTILITY_BILLS!=null)
                             $dat['UTILITY_BILLS']=  $room->UTILITY_BILLS->VARCHAR_VALUE;     
                          
                          $dat['OPERATING_COSTS']=  $room->OPERATING_COSTS;
                          
                          
                          if ($room->DEPOSIT!=null)
                             $dat['DEPOSITS']=  $room->DEPOSIT->VARCHAR_VALUE;     

                          if ($room->RENT_TYPE!=null)
                             $dat['RENT_TYPE']=  $room->RENT_TYPE->VARCHAR_VALUE;  
                          if ($room->RENT_W_NDS!=null)
                             $dat['WITH_NDS']=  $room->RENT_W_NDS->VARCHAR_VALUE;     

                          if ($room->PLANING!=null)
                             $dat['PLANING']=  $room->PLANING->VARCHAR_VALUE;     
                          
                          if ($room->FINISHING!=null)
                             $dat['FINISHING']=  $room->FINISHING->VARCHAR_VALUE;     
                          
                          if ($room->RentPRICES!=null){
                             $dat['RENT_PRICES_RUB']=  $room->RentPRICES->PRICE_RUB;     
                             $dat['RENT_PRICES_USD']=  $room->RentPRICES->PRICE_USD;     
                             $dat['RENT_PRICES_EUR']=  $room->RentPRICES->PRICE_EUR;     
                          }
                          if ($room->broker!=null)
                            $dat['BROKER']=  $room->broker->NAME;  
                          $dat['OUR_PERCENT']=  $room->OUR_PERCENT;
                          $dat['PREPAYD_VALUE']=  $room->PREPAYD_VALUE;
                          $dat['READY_DATE']=  $room->READY_DATE;
                          $dat['LAST_DATE']=   date("d.m.Y",strtotime($room->LAST_DATE));;
                          $dat['ROOM_PIC']=  'app/store/img/41.jpg';
                          
                          
                         $res[] = $dat;
                        }
                   } else{
                    
                      
                      $res[] = $dat;
                   }
                    echo CJSON::encode(array(
                        'success' => true,
                            'LOT' => $lot[0]->NAME,
                            'ADDRESS'  => @$addresses[0]->StrName->ADDRESS,
                            'ROOMS' => $res,
                            'total' => $pages->itemCount
                    ));
        }
}
?>