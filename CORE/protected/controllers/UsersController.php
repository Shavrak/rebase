<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsersContrroller
 *
 * @author kxxb
 */
class UsersController  extends Controller{
    
    public function actionWholeUserList (){
        $model = new USER();
        $criteria = new CDbCriteria;
        if (isset($_GET["query"])){
          $criteria->compare('NAME',$_GET["query"]); 
        }
        $count=$model->count($criteria);
        $pages=new CPagination($count);
        // results per page
        $pages->pageSize=$_GET["limit"];
        $pages->applyLimit($criteria);
        $data = $model->findAll($criteria);
            
            foreach($data as $userName){
                      $dat['ID']       =  $userName->ID;  
                      $dat['NAME']=  $userName->NAME;
                      $dat1[] = $dat;
                    }
            
                    echo CJSON::encode(array(
                        'success' => true,
                        'users' => $dat1 ,
                        'total' => count($data)
                    ));        
            
    }
    
    public function actionUsersByDep(){
        $model = new USER();
        $criteria = new CDbCriteria;
        if (isset($_GET["query"])){
          $criteria->compare('NAME',$_GET["query"]); 
        
        }
        $criteria->compare('DEP_ID',$_GET["DEP_ID"]); 
        $count=$model->count($criteria);
        $pages=new CPagination($count);
        // results per page
        $pages->pageSize=$_GET["limit"];
        $pages->applyLimit($criteria);
        $data = $model->findAll($criteria);
            
            foreach($data as $userName){
                      $dat['ID']       =  $userName->ID;  
                      $dat['NAME']=  $userName->NAME;
                      $dat1[] = $dat;
                    }
            
                    echo CJSON::encode(array(
                        'success' => true,
                        'users' => $dat1 ,
                        'total' => count($data)
                    ));        
    }
    
    
    public function actionSave(){
        if(isset($_POST['ID'])){
          $model=USER::model()->findByPk($_POST['ID']);
        }
        else{
          $model = new USER();
          
          
        }
    }
    
    public function actionDepartmentsList (){
        $model = new DEPARTPENTS();
        $criteria = new CDbCriteria;
        if (isset($_GET["query"])){
          $criteria->compare('NAME',$_GET["query"]); 
          
        }
        $count=$model->count($criteria);
        $pages=new CPagination($count);
        // results per page
        $pages->pageSize=$_GET["limit"];
        $pages->applyLimit($criteria);
        $data = $model->findAll($criteria);
            foreach($data as $dep){
                      $dat['ID']           =  $dep->ID;  
                      $dat['NAME']         =  $dep->NAME;
                      $dat['DESCRIPTION']  =  $dep->DESCRIPTION;
                      $dat1[] = $dat;
                    }
            
                    echo CJSON::encode(array(
                        'success' => true,
                        'dep' => $dat1 ,
                        'total' => count($data)
                    ));        
       
       
       
    }
    
    public function actionSaveDepartment (){
            
        if(isset($_POST['ID'])){
            $model=DEPARTPENTS::model()->findByPk($_POST['ID']);
            $model->NAME =$_POST['NAME'];
            $model->DESCRIPTION =$_POST['DESCRIPTION'];
            $model->LAST_UID =Yii::app()->user->id;
            $model->LAST_DATE =new CDbExpression('NOW()');
            $model->save();
            
        }else{
            $model= new DEPARTPENTS();
            $model->NAME =$_POST['NAME'];
            $model->DESCRIPTION =$_POST['DESCRIPTION'];
            $model->LAST_UID =Yii::app()->user->id;
            $model->LAST_DATE =new CDbExpression('NOW()');
            $model->save();
        }
    }
    
    
    //put your code here
}

?>
