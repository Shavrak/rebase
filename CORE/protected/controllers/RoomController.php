<?php

class RoomController extends Controller
{

	
//              'ID','FLOOR_NUMBER', 'AREA', 'BROKER', 'OUR_PERCENT', 
//              'RENT_PRICES', 'WITH_NDS', 'FINISHING','CONTARCT_TYPE',
//              'PLANING','OPERATING_COSTS','UTILITY_BILLS','DEPOSITS',
//              'PREPAYD_VALUE','RENT_TYPE', 'RENT_CONTRACT_ID',
//              'SALE_PRICES', 'SALE_CONTRACT_ID', 'LAST_DATE', 'LAST_UID ',
//              'READY_DATE'
    
        public function actionRoomsList(){
            $model = new LOTROOMS;
            $roompicture  = new LOTROOMPICTURE();
            $areas  = new LOTROOMSAREA();
            $dat = null;
            $criteria = new CDbCriteria;
            if (isset($_GET["LOT_ID"]) ){
                $criteria->compare('LOT_ID',$_GET["LOT_ID"]);
            }

            $count=$model->count($criteria);
            $pages=new CPagination($count);

            // results per page
            $pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
            
              $rooms = $model->findAll($criteria);
                    if ($rooms!=NULL){ 
                        foreach($rooms as $room){
                          $roompic = $roompicture->findAll('LOT_ROOM_ID=:LOT_ROOM_ID', array(':LOT_ROOM_ID'=>$room->ID));   
                          $area  = $areas->findAll('ROOM_LOT_ID=:ROOM_LOT_ID', array(':ROOM_LOT_ID'=>$room->ID));
                          $dat['ID']=  $room->ID;  
                          $dat['ROOM_LOT']=  $room->ROOM_LOT;  
                          $dat['FLOOR_NUMBER']=  $room->FLOOR_NUMBER;  
//                          echo"<pre>";
//                          var_dump($area);
//                          echo "<hr>";
                          $ROOM_AREA = null;
                          $ROOM_AREA_FROM = null;
                          $ROOM_AREA_TO = null;
                          $IS_ROOM_INTERVAL = null;
                          foreach ($area as $item){
                            //  echo $room->ID;
                              $ROOM_AREA = $item->AREA;
                              $ROOM_AREA_FROM = $item->AREA_FROM;
                              $ROOM_AREA_TO = $item->AREA_TO;
                              $IS_ROOM_INTERVAL = $item->IS_INTERVAL;
                          
                          }
                          
                            if ($IS_ROOM_INTERVAL!=1){
                               $dat['AREA']=  $ROOM_AREA; 
                            } else {
                               $dat['AREA']=  'от '.$ROOM_AREA_FROM.' до '.$ROOM_AREA_TO;  
                            }
                          if ($room->CONTRACT_T!=null)
                             $dat['CONTARCT_TYPE']=  $room->CONTRACT_T->VARCHAR_VALUE;     

                          if ($room->UTILITY_BILLS!=null)
                            $dat['UTILITY_BILLS']=  $room->UTILITY_BILLS->VARCHAR_VALUE;     
                            $dat['OPERATING_COSTS']=  $room->OPERATING_COSTS;
                          
                          if ($room->DEPOSIT!=null)
                             $dat['DEPOSITS']=  $room->DEPOSIT->VARCHAR_VALUE;     

                          if ($room->RENT_TYPE!=null)
                             $dat['RENT_TYPE']=  $room->RENT_TYPE->VARCHAR_VALUE;  
                          if ($room->RENT_W_NDS!=null)
                             $dat['WITH_NDS']=  $room->RENT_W_NDS->VARCHAR_VALUE;     

                          if ($room->PLANING!=null)
                             $dat['PLANING']=  $room->PLANING->VARCHAR_VALUE;     
                          
                          if ($room->FINISHING!=null)
                             $dat['FINISHING']=  $room->FINISHING->VARCHAR_VALUE;     
                          
                          if ($room->RentPRICES!=null){
                             $dat['RENT_PRICES_RUB']=  $room->RentPRICES->PRICE_RUB;     
                             $dat['RENT_PRICES_USD']=  $room->RentPRICES->PRICE_USD;     
                             $dat['RENT_PRICES_EUR']=  $room->RentPRICES->PRICE_EUR;     
                          }
                          if ($room->broker!=null)
                            $dat['BROKER']=  $room->broker->NAME;  
                          $dat['OUR_PERCENT']=  $room->OUR_PERCENT;
                          $dat['PREPAYD_VALUE']=  $room->PREPAYD_VALUE;
                          $dat['READY_DATE']=  $room->READY_DATE;
                          $dat['LAST_DATE']=   date("d.m.Y",strtotime($room->LAST_DATE));;
                          
                          if (@$roompic[0]->PIC_PATH!=null){
                            $dat['ROOM_PIC']=  'app/store/img/Rooms/'.$roompic[0]->PIC_PATH; 
                          } else {
                             $dat['ROOM_PIC']=  'app/store/img/Rooms/41.jpg'; 
                          }
                         
                          
                         $res[] = $dat;
                        }
                   } else{
                    
                      
                      $res[] = $dat;
                   }
                    echo CJSON::encode(array(
                        'success' => true,
                        'rooms' => $res,
                        'total' => $pages->itemCount
                    ));
        }
        
        
       
        
        public function actionAddRoom(){
               
               $room  = new LOTROOMS();
               $price = new PRICES();
               $area  = new LOTROOMSAREA();
            
                $room->LOT_ID   =  $_POST['LOT_ID'];
                $room->ROOM_LOT =  $_POST['ROOM_LOT'];
                $room->AREA     =  $_POST['AREA'];
            
                
                $room->FLOOR_NUMBER=  $_POST['FLOOR'];
                $room->READY_DATE=  date('y-m-d', strtotime($_POST['READY_DATE']));
                $room->PLANING_ID=  $_POST['PLANING_ID'];
                $room->FINISHING_ID=  $_POST['FINISHING_ID'];
                $room->CONTRACT_TYPE =  $_POST['CONTRACT_TYPE_ID'];
                $room->BROKER_ID=  $_POST['BROKER_ID'];
                $room->FIAS_AOID=  $_POST['ADDRESS'];
                
                
              
                
                
                $room->RENT_WITH_NDS=  $_POST['RENT_WITH_NDS'];
                $room->OPERATING_COSTS=  $_POST['OPERATING_COSTS'];
                $room->UTILITY_BILLS_ID=  $_POST['UTILITY_BILLS_ID'];
                $room->DEPOSIT_ID=  $_POST['DEPOSIT_ID'];
                $room->PREPAYD_VALUE=  $_POST['PREPAYD_VALUE'];
                $room->RENT_TYPE_ID=  $_POST['RENT_TYPE_ID'];
                if($_POST['OwnerContrIdRent']!=0)
                   $room->RENT_CONTRACT_ID  = $_POST['OwnerContrIdRent']; 
                $room->OUR_PERCENT=  $_POST['OUR_PERCENT'];
                
                

                
                $room->SALE_WITH_NDS=  $_POST['SALE_WITH_NDS'];
                if($_POST['OwnerContrIdSale']!=0)
                   $room->SALE_CONTRACT_ID  = $_POST['OwnerContrIdSale']; 
                
                
                $room->COMMENT=  $_POST['COMMENT'];
                $room->CREATE_UID =Yii::app()->user->id;
                $room->CREATE_DATE=new CDbExpression('NOW()');
                $room->LAST_UID=Yii::app()->user->id;
                $room->LAST_DATE=new CDbExpression('NOW()');
                
                $room->save();
                
                $room_id = $room->ID;
                $room_rent_price_id = null;
                $room_sale_price_id = null;
                
                if (isset($_POST['AREA'])) {
                      $area->SaveRoomArea($_POST['LOT_ID'], 
                                      $room_id,  
                                      $_POST['AREA'], 
                                      Yii::app()->user->id,
                                      Yii::app()->user->id);
                 }
                
                if ($_POST['RENT_PRICE']!=null){
                  $room_rent_price_id=$price->saveRoomPrice($_POST['LOT_ID'],
                                                              $room_id,
                                                              2,  
                                                              $_POST['RENT_CURR_ID'], 
                                                              $_POST['RENT_PRICE'],
                                                              Yii::app()->user->id    
                                                            );
                }
                if ($_POST['SALE_PRICE']!=null){
                    $room_sale_price_id=$price->saveRoomPrice($_POST['LOT_ID'], 
                                                                $room_id,
                                                                3, 
                                                                $_POST['SALE_CURR_ID'], 
                                                                $_POST['SALE_PRICE'],
                                                                Yii::app()->user->id );
                }
                
                
                
                LOTROOMS::model()->updateByPk($room_id, 
                    array('SALE_PRICES_ID'=>$room_sale_price_id,
                          'RENT_PRICES_ID'=>$room_rent_price_id,
                    ));
                
        }
        
        
        
        public function actionTest()
	{   
              $is_interval = 0;  
              $area = ' 117-256';
              if(strrpos($area, "-") !== false){
                  $is_interval = 1;
                  echo substr($area,0,(strrpos($area, "-")));
                  echo "<br>";
                  echo substr($area,(strrpos($area, "-")+1));
                  echo "<br>";
              }
              echo "<br>";
               echo "is_interval = $is_interval";
               echo "<br>";
              echo $area;

              //print_r($res_cur);
                 
	}
}