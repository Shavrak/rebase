<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class SysController extends Controller
{
    
    /* action для загрузки курсов валют с сайта ЦБ РФ
     * Его необходимо поставить в крон для автоматической загрузки
     */
    public function actionDailycours(){
     Yii::import('application.extensions.cbr.CBRCurrency');
        $curses=new CBRCurrency();
        $model = new SYS_DAILY_COURS();
        $model->TODAY = new CDbExpression('NOW()');
        $model->RUB = 1;
        $model->USD = $curses->currency[840]['value'];
        $model->EUR = $curses->currency[978]['value'];
        $model->save();
        
    }
    
    public function actionTest(){
        $lots = LOTINPUTDATA::model()->findAll();
                    header('Content-type: application/json');
                    //echo CJSON::encode($car);
                    echo CJSON::encode(array(
                        'success' => true,
                        'lots' => $lots,
                        'total' => count($lots),
                        
                    ));
    }
    
    
}
?>
