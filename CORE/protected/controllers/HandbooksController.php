<?php

class HandbooksController extends Controller
{

        
        public function actionHandbooksList()
	{      
            $model = new HANDBOOKS;
            $criteria = new CDbCriteria;
            $count=$model->count($criteria);
            $pages=new CPagination($count);

            // results per page
            $pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
            $handbooks = $model->findAll($criteria);
                    foreach($handbooks as $hndb){
                      $dat['ID']=  $hndb->ID;  
                      $dat['HANDBOOK']=  $hndb->HANDBOOK;  
                      $dat['H_GROUP_ID']=  $hndb->H_GROUP_ID;  
                     // $dat['LAST_DATE']=  Yii::app()->dateFormatter->format('d MMMM yyyy', $hndb->LAST_DATE);  
                        $dat['USER_NAME']=  $hndb->lASTU->NAME;
                     $res[] = $dat;
                    }
                    echo CJSON::encode(array(
                        'success' => true,
                        'hndb' => $res,
                        'total' => $pages->itemCount
                    ));
                    //echo CJSON::encode($dat);
                    
                    //echo CJSON::encode($UsersNames[1]->lASTU);
                    
                    
	}
        
        public function actionHandbooksEdit(){
                //$iRequestBody = json_decode(file_get_contents('php://input'), true);
                $model=HANDBOOKS::model()->findByPk($_POST['HANDBOOK_ID']);
                $model->HANDBOOK =$_POST['HANDBOOK'];
                $model->H_GROUP_ID =$_POST['H_GROUP_ID'];
                //$dat['H_GROUP_ID']=  $hndb->H_GROUP_ID;  
                $model->LAST_UID =Yii::app()->user->id;
                $model->LAST_DATE =new CDbExpression('NOW()');
                $model->save();
        }
        
        public function actionHandbooksCreate(){
                //$iRequestBody = json_decode(file_get_contents('php://input'), true);
                $model= new HANDBOOKS;
                //$model->ID = 101;$_POST['NAME'];
                $model->HANDBOOK =$_POST['HANDBOOK'];
                $model->H_GROUP_ID =$_POST['H_GROUP_ID'];
                $model->LAST_UID =Yii::app()->user->id;
                $model->LAST_DATE =new CDbExpression('NOW()');
//        print_r($iRequestBody);
                if($model->save()){
                    echo "ok";
                }  else {
                echo "ID is -".$model->ID;    
                }
        }

        
        
         public function actionHandbooksValueCreate(){
                //$iRequestBody = json_decode(file_get_contents('php://input'), true);
                $model= new HANDBOOKVALUES;
                //$model->ID = 101;$_POST['NAME'];
                $model->HANDBOOK_ID =$_POST['HANDBOOK_ID'];
                $model->VARCHAR_VALUE =$_POST['H_VALUE'];
                $model->LAST_UID =Yii::app()->user->id;
                $model->LAST_DATE =new CDbExpression('NOW()');
//        print_r($iRequestBody);
                if($model->save()){
                    echo "ok";
                }  else {
                echo "ID is -".$model->ID;    
                }
        }
        
        public function actionHandbooksValueEdit(){
                //$iRequestBody = json_decode(file_get_contents('php://input'), true);
                $model=HANDBOOKVALUES::model()->findByPk($_POST['VALUE_ID']);
                $model->VARCHAR_VALUE =$_POST['H_VALUE'];
                //$dat['H_GROUP_ID']=  $hndb->H_GROUP_ID;  
                $model->LAST_UID =Yii::app()->user->id;
                $model->LAST_DATE =new CDbExpression('NOW()');
                $model->save();
        }
        
          public function actionHandbooksValueDelete(){
                //$iRequestBody = json_decode(file_get_contents('php://input'), true);
                $model=HANDBOOKVALUES::model()->deleteByPk($_POST['VALUE_ID']);
                
        }
        
        public function actionHandbooksValue(){
            $model = new HANDBOOKVALUES;
            $criteria = new CDbCriteria;
            
          
            
            if (isset($_GET["handbook_id"])){
              $criteria->compare('handbook_id',$_GET["handbook_id"]); 
            }
            
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            // results per page
            $pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
                $handbooks = $model->findAll($criteria);
                    echo CJSON::encode(array(
                        'success' => true,
                        'vls' => $handbooks,
                        'total' => $pages->itemCount
                    ));
        }
        
        public function actionPricetype(){
            $model = new SYSPRICETYPE;
            $criteria = new CDbCriteria;
            
          
            
//            if (isset($_GET["handbook_id"])){
//              $criteria->compare('handbook_id',$_GET["handbook_id"]); 
//            }
            
            $count=$model->count($criteria);
            $pages=new CPagination($count);
            // results per page
            $pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
                $handbooks = $model->findAll($criteria);
                    echo CJSON::encode(array(
                        'success' => true,
                        'vls' => $handbooks,
                        'total' => $pages->itemCount
                    ));
        }
        
        
      
}