    <?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class BlocksController extends Controller
{
    
    public function actionList(){
        
        $model = new LOTROOMS;
        $metro = new METROSTATIONS;
        $landmark = new LANDMARKS;
        $address = new LOTADDRESSES;
        $building_data = new LOTBUILDINGSDATA;
        $commercial = new LOTCOMMERCIALTERMS;
        $roomsarea  = new LOTROOMSAREA();
        $roompicture  = new LOTROOMPICTURE();
        $prices = new PRICES;
        $ar = array();  
        $prc = array();  
        $dat = null;
        $mt = null;
        $criteria = new CDbCriteria;
        $price_criteria = new CDbCriteria;
        $PRICE_FROM =null;
	$PRICE_TO =null;
        $area_from = null;
        $area_to   = null;
	if (isset($_GET["AREA_FROM"])) 
          $area_from = $_GET['AREA_FROM'];
        if (isset($_GET["AREA_TO"])) 
          $area_to   = $_GET['AREA_TO'];
	if (isset($_GET["PRICE_FROM"])) 
           $PRICE_FROM = $_GET["PRICE_FROM"];
	if (isset($_GET["PRICE_TO"])) 
            $PRICE_TO =  $_GET["PRICE_TO"];
        
        
        
//            if (isset($_GET["LOT_ID"]) ){
//                $criteria->compare('LOT_ID',$_GET["LOT_ID"]);
//            }

            
              if ($PRICE_FROM!=null ||$PRICE_TO!=null ) {
                 //$PRICE_TYPE   = $_GET['PRICE_TYPE'];
                   if ($PRICE_TO ==null) $PRICE_TO = $PRICE_FROM;
                   if ($PRICE_FROM ==null) $PRICE_FROM = 0;
                   
                   if ($_GET['rent_curr_id']== 4930){
                     $column = 'PRICE_RUB';
                   }elseif ($_GET['rent_curr_id']== 4931){
                     $column = 'PRICE_USD';
                   }elseif ($_GET['rent_curr_id']== 4932){
                     $column = 'PRICE_EUR';
                   }
                   $price_criteria->addBetweenCondition($column, $PRICE_FROM, $PRICE_TO);
                   $price_criteria->compare('PRICE_TYPE',2);
                   $price = $prices->findAll($price_criteria);
                   
                   
                    foreach($price as $l)
                        if ($l->ROOM_LOT_ID!= null)
                             $prc[] =  $l->ROOM_LOT_ID;
                    
                  
            }
          
            if ($area_from!=null ||  $area_to !=null ) {  
                //помоему гавно а не код
                //но пока по другому не могу
                //это поиск по интервалам
              
               
               if ($area_to ==null) $area_to = $area_from;
               if ($area_from ==null) $area_from = 0;
               
                    $areas = Yii::app()->db->createCommand()
                     ->select('ROOM_LOT_ID')
                     ->from('LOT_ROOMS_AREA')
                     ->where(array('and', 'area_from <='.$area_to, 'area_to >='.$area_from))
                            //where  AREA_FROM <= 25 and AREA_TO >= 10
//                     ->union('select x.ROOM_LOT_ID from LOT_ROOMS_AREA x
//                             where x.area between '.$area_from.' and '.$area_to)
                     ->query();
               
               
            
//                $areas = Yii::app()->db->createCommand()
//                     ->select('ROOM_LOT_ID')
//                     ->from('LOT_ROOMS_AREA')
//                     ->where('area between '.$area_from.' and '.$area_to)
//                     ->query();
               
                 foreach($areas as $a)
                  if ($a['ROOM_LOT_ID']!=null)
                    $ar[] =  $a['ROOM_LOT_ID'];
                 
             }
             
             
             //йобанный кастыль
              
              if($prc==null ) {$prc=$ar;}
              if($ar==null ){$ar=$prc;}
//              echo"ar -";
//              print_r($ar);
//              echo"<br> prc -";
//              print_r($prc);
//              echo"<br>";
//              print_r(array_unique(array_intersect($prc,$ar)));
              
            $res_lot =  array_unique(array_intersect($prc,$ar));
            
            // $res_lot =  $prc;
            if ($res_lot!=null){
                $criteria->addInCondition('ID',$res_lot);
            }
            
            $count=$model->count($criteria);
            $pages=new CPagination($count);

            // results per page
            $pages->pageSize=$_GET["limit"];
            $pages->applyLimit($criteria);
            
              $rooms = $model->findAll($criteria);
                        foreach($rooms as $room){
                          $metros    = $metro->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$room->LOT_ID));    
                          $landmarks = $landmark->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$room->LOT_ID));  
                          $addresses = $address->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$room->LOT_ID));  
                          $area      = $roomsarea->findAll('ROOM_LOT_ID=:ROOM_LOT_ID', array(':ROOM_LOT_ID'=>$room->ID));
                          $roompic   = $roompicture->findAll('LOT_ROOM_ID=:LOT_ROOM_ID', array(':LOT_ROOM_ID'=>$room->ID));    
                          $dat['ID'] =  $room->ID;  
                          $dat['FLOOR_NUMBER'] =  $room->FLOOR_NUMBER;  
                          $dat['LOT_ID']=  $room->LOT_ID;  
                          $dat['ROOM_LOT']=  $room->ROOM_LOT;  
                          
                          $ROOM_AREA = null;
                          $ROOM_AREA_FROM = null;
                          $ROOM_AREA_TO = null;
                          $IS_ROOM_INTERVAL = null;
                          foreach ($area as $item){
                            //  echo $room->ID;
                              $ROOM_AREA = $item->AREA;
                              $ROOM_AREA_FROM = $item->AREA_FROM;
                              $ROOM_AREA_TO = $item->AREA_TO;
                              $IS_ROOM_INTERVAL = $item->IS_INTERVAL;
                          
                          }
                          
                            if ($IS_ROOM_INTERVAL!=1){
                               $dat['AREA']=  $ROOM_AREA; 
                            } else {
                               $dat['AREA']=  'от '.$ROOM_AREA_FROM.' до '.$ROOM_AREA_TO;  
                            }
                          
//                          $dat['AREA']=  $room->AREA; 
                          
                          if ($metros!=NULL){
                            foreach ($metros as  $m)
                               $mt .=  $m->STATION_NAME->VARCHAR_VALUE .', ';
                               $dat['METRO_SATATION'] = $mt;
                               $mt='';
                        }else{
                          $dat['METRO_SATATION'] = 'Пусто';
                        }
                        
                        if(@$landmarks[0]->DIRECTION_NAME->VARCHAR_VALUE !=null){
                          $dat['DIRECTON'] = @$landmarks[0]->DIRECTION_NAME->VARCHAR_VALUE;  
                        }else{
                          $dat['DIRECTON']  = 'пусто';
                        }
                        $dat['ADDRESS']  = @$addresses[0]->StrName->ADDRESS;
                        
                          
                          if ($room->CONTRACT_T!=null)
                             $dat['CONTARCT_TYPE']=  $room->CONTRACT_T->VARCHAR_VALUE;     

                          if ($room->UTILITY_BILLS!=null)
                             $dat['UTILITY_BILLS']=  $room->UTILITY_BILLS->VARCHAR_VALUE;     
                          
                          $dat['OPERATING_COSTS']=  $room->OPERATING_COSTS;
                          
                          
                          if ($room->DEPOSIT!=null)
                             $dat['DEPOSITS']=  $room->DEPOSIT->VARCHAR_VALUE;     

                          if ($room->RENT_TYPE!=null)
                             $dat['RENT_TYPE']=  $room->RENT_TYPE->VARCHAR_VALUE;  
                          if ($room->RENT_W_NDS!=null)
                             $dat['WITH_NDS']=  $room->RENT_W_NDS->VARCHAR_VALUE;     

                          if ($room->PLANING!=null)
                             $dat['PLANING']=  $room->PLANING->VARCHAR_VALUE;     
                          
                          if ($room->FINISHING!=null)
                             $dat['FINISHING']=  $room->FINISHING->VARCHAR_VALUE;     
                          
                          if ($room->RentPRICES!=null){
                             $dat['RENT_PRICES_RUB']=  $room->RentPRICES->PRICE_RUB;     
                             $dat['RENT_PRICES_USD']=  $room->RentPRICES->PRICE_USD;     
                             $dat['RENT_PRICES_EUR']=  $room->RentPRICES->PRICE_EUR;     
                          }
                          if ($room->broker!=null)
                           $dat['BROKER']=  $room->broker->NAME;  
                           $dat['OUR_PERCENT']=  $room->OUR_PERCENT;
                           $dat['PREPAYD_VALUE']=  $room->PREPAYD_VALUE;
                           $dat['READY_DATE']=  $room->READY_DATE;
                           $dat['LAST_DATE']=   date("d.m.Y",strtotime($room->LAST_DATE));
  
                          if (@$roompic[0]->PIC_PATH!=null){
                            $dat['ROOM_PIC']=  'app/store/img/Rooms/'.$roompic[0]->PIC_PATH; 
                          } else {
                             $dat['ROOM_PIC']=  'app/store/img/Rooms/41.jpg'; 
                          }
                           
                           //$dat['ROOM_PIC']=  'app/store/img/Rooms/'.$roompic[0]->PIC_PATH;
                          
                          
                         $res[] = $dat;
                        }
                   
                  echo CJSON::encode(array(
                        'success' => true,
                        'blocks' => $res,
                        'total' => $pages->itemCount
                    ));
    }
}   
?>