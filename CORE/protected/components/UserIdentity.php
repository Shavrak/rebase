<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
        private $_id;
        
//        public function authenticate()
//	{
//            
//        $options = Yii::app()->params['ldap'];
//        $connection = ldap_connect($options['host'], $options['port']);
//        ldap_set_option($connection, LDAP_OPT_PROTOCOL_VERSION, 3);
//        ldap_set_option($connection, LDAP_OPT_REFERRALS, 0);
// 
//        if($connection)
//        {
//            try
//            {
//                @$bind = ldap_bind($connection, $options['domain']."\\".$this->username, $this->password);
//            }
//            catch (Exception $e){
//                echo $e->getMessage();
//            }
//            if(!$bind) $this->errorCode = self::ERROR_PASSWORD_INVALID;
//            else {     
//                       
//                        $username=strtolower($this->username);
//                        $user = USER::model()->find('LOWER(LOGIN)=?',array($username));
//                        $user->ipaddress = CHttpRequest::getUserHostAddress();
//                        $user->PASSWORD =$this->password;
//                        $user->LAST_DATE = new CDbExpression('NOW()');
//                        $user->save(); 
//                        if ($user->STATUS_ID!=1){
//                            $this->errorCode = 77;
//                        } else {
//                            $this->_id=$user->ID;
//                            $this->username=$user->NAME;
//
//                            $this->errorCode = self::ERROR_NONE;
//                        }
//            }
//        }
//        return !$this->errorCode;
//
//	}
        
        
//        
	public function authenticate()
	{
                $username=strtolower($this->username);
                $user = USER::model()->find('LOWER(LOGIN)=?',array($username));
                
                if($user===null)
                    $this->errorCode=self::ERROR_USERNAME_INVALID;
                else if(!$user->validatePassword($this->password))
                    $this->errorCode=self::ERROR_PASSWORD_INVALID;
                else
                {
                    $this->_id=$user->ID;
                    $this->username=$user->NAME;
                    $this->errorCode=self::ERROR_NONE;
                }
                return $this->errorCode==self::ERROR_NONE;
                
//                
//		$users=array(
//			// username => password
//			'demo'=>'demo',
//			'admin'=>'admin',
//		);
//		if(!isset($users[$this->username]))
//			$this->errorCode=self::ERROR_USERNAME_INVALID;
//		else if($users[$this->username]!==$this->password)
//			$this->errorCode=self::ERROR_PASSWORD_INVALID;
//		else
//			$this->errorCode=self::ERROR_NONE;
//		return !$this->errorCode;
	}
        
        public function getId()
        {
            return $this->_id;
        }
}