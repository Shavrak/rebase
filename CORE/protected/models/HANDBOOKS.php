<?php

/**
 * This is the model class for table "REBASE_HANDBOOKS".
 *
 * The followings are the available columns in table 'REBASE_HANDBOOKS':
 * @property integer $ID
 * @property string $HANDBOOK
 * @property integer $H_GROUP_ID
 * @property string $LAST_DATE
 * @property integer $LAST_UID
 *
 * The followings are the available model relations:
 * @property RebaseUser $lASTU
 * @property RebaseHGroups $hGROUP
 */
class HANDBOOKS extends CActiveRecord
{       
        public $NAME;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return HANDBOOKS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'REBASE_HANDBOOKS';
	}

//        public function defaultScope()
//    {
//        return array(
//            'alias' => $this->tableName()
//        );
//    } 
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('ID', 'required'),
			array('ID, H_GROUP_ID, LAST_UID', 'numerical', 'integerOnly'=>true),
			array('HANDBOOK', 'length', 'max'=>450),
			array('LAST_DATE', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, HANDBOOK, H_GROUP_ID, LAST_DATE, LAST_UID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lASTU' => array(self::BELONGS_TO, 'USER', 'LAST_UID'),
			'hGROUP' => array(self::BELONGS_TO, 'RebaseHGroups', 'H_GROUP_ID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'HANDBOOK' => 'Handbook',
			'H_GROUP_ID' => 'H Group',
			'LAST_DATE' => 'Last Date',
			'LAST_UID' => 'Last Uid',
                        'NAME' => 'UNA',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('HANDBOOK',$this->HANDBOOK,true);
		$criteria->compare('H_GROUP_ID',$this->H_GROUP_ID);
		$criteria->compare('LAST_DATE',$this->LAST_DATE,true);
		$criteria->compare('LAST_UID',$this->LAST_UID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}