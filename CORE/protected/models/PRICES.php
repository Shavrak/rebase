<?php

/**
 * This is the model class for table "REBASE_PRICES".
 *
 * The followings are the available columns in table 'REBASE_PRICES':
 * @property integer $ID
 * @property integer $PRICE_TYPE
 * @property string $PRICE_RUB
 * @property string $PRICE_USD
 * @property string $PRICE_EUR
 * @property integer $OBJECT_ID
 * @property integer $CREATE_UID
 * @property string $CREATE_DATE
 * @property integer $LAST_UID
 * @property string $LAST_DATE
 */
class PRICES extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PRICES the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'REBASE_PRICES';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'PRICE_TYPE' => 'Price Type',
			'PRICE_RUB' => 'Price Rub',
			'PRICE_USD' => 'Price Usd',
			'PRICE_EUR' => 'Price Eur',
			
			'CREATE_UID' => 'Create Uid',
			'CREATE_DATE' => 'Create Date',
			'LAST_UID' => 'Last Uid',
			'LAST_DATE' => 'Last Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('PRICE_TYPE',$this->PRICE_TYPE);
		$criteria->compare('PRICE_RUB',$this->PRICE_RUB,true);
		$criteria->compare('PRICE_USD',$this->PRICE_USD,true);
		$criteria->compare('PRICE_EUR',$this->PRICE_EUR,true);
		
		$criteria->compare('CREATE_UID',$this->CREATE_UID);
		$criteria->compare('CREATE_DATE',$this->CREATE_DATE,true);
		$criteria->compare('LAST_UID',$this->LAST_UID);
		$criteria->compare('LAST_DATE',$this->LAST_DATE,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function savePrice($lot_id,$price_type,$cur_id,$price_val, $uid){
        
            $price  = new PRICES();

            $res_cur = $this->curConvert($cur_id, $price_val);

            $price->PRICE_TYPE = $price_type;
            $price->PRICE_USD = $res_cur['USD'];
            $price->PRICE_EUR = $res_cur['EUR'];
            $price->PRICE_RUB = $res_cur['RUB'];
            $price->BASE_CURR_ID = $cur_id;
            $price->LOT_ID = $lot_id;
            $price->CREATE_UID =$uid;
            $price->CREATE_DATE=new CDbExpression('NOW()');
            $price->LAST_UID=$uid;
            $price->LAST_DATE=new CDbExpression('NOW()');
            $price->save();        

            return $price->ID;
        
        
    }
    
    public function saveRoomPrice($lot_id,$room_id,$price_type,$cur_id,$price_val, $uid){
        
            $price  = new PRICES();

            $res_cur = $this->curConvert($cur_id, $price_val);

            $price->PRICE_TYPE = $price_type;
            $price->PRICE_USD = $res_cur['USD'];
            $price->PRICE_EUR = $res_cur['EUR'];
            $price->PRICE_RUB = $res_cur['RUB'];
            $price->BASE_CURR_ID = $cur_id;
            $price->LOT_ID = $lot_id;
            $price->ROOM_LOT_ID = $room_id;
            $price->CREATE_UID =$uid;
            $price->CREATE_DATE=new CDbExpression('NOW()');
            $price->LAST_UID=$uid;
            $price->LAST_DATE=new CDbExpression('NOW()');
            $price->save();        
            return $price->ID;
        
        
    }
    
    public function curConvert($cur_id,$price){
         $cur_res = array();
         $daily_cours = new SYS_DAILY_COURS();
         $cours = $daily_cours->findAll(array(
                            'order'=>'1 desc',
                        ));
                  
         if ($cur_id == 4930) //РУБ.
         {
           $cur_res = array('RUB'=>$price,
                            'USD'=>$price/$cours[0]->USD,
                            'EUR'=>$price/$cours[0]->EUR, 
                    );
         }
         if ($cur_id == 4931) //USD.
         {
           $cur_res = array('RUB'=>$price*$cours[0]->USD, 
                            'USD'=>$price,
                            'EUR'=>$price*$cours[0]->USD/$cours[0]->EUR, 
                    );
         }
         if ($cur_id == 4932) //EUR.
         {
           $cur_res = array('RUB'=>$price*$cours[0]->EUR, 
                            'USD'=>$price*$cours[0]->EUR/$cours[0]->USD,
                            'EUR'=>$price, 
                    );
         }
         return $cur_res;
    }
}