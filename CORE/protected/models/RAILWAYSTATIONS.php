<?php

/**
 * This is the model class for table "LOT_RAILWAY_STATIONS".
 *
 * The followings are the available columns in table 'LOT_RAILWAY_STATIONS':
 * @property integer $ID
 * @property integer $LOT_ID
 * @property integer $RAILWAY_STATION_ID
 * @property integer $CREATE_UID
 * @property string $CREATE_DATE
 * @property integer $LAST_UID
 * @property string $LAST_DATE
 *
 * The followings are the available model relations:
 * @property RebaseLot $lOT
 */
class RAILWAYSTATIONS extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RAILWAYSTATIONS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LOT_RAILWAY_STATIONS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('ID', 'required'),
			array('ID, LOT_ID, RAILWAY_STATION_ID, CREATE_UID, LAST_UID', 'numerical', 'integerOnly'=>true),
			array('CREATE_DATE, LAST_DATE', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, LOT_ID, RAILWAY_STATION_ID, CREATE_UID, CREATE_DATE, LAST_UID, LAST_DATE', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lOT' => array(self::BELONGS_TO, 'RebaseLot', 'LOT_ID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'LOT_ID' => 'Lot',
			'RAILWAY_STATION_ID' => 'Railway Station',
			'CREATE_UID' => 'Create Uid',
			'CREATE_DATE' => 'Create Date',
			'LAST_UID' => 'Last Uid',
			'LAST_DATE' => 'Last Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('LOT_ID',$this->LOT_ID);
		$criteria->compare('RAILWAY_STATION_ID',$this->RAILWAY_STATION_ID);
		$criteria->compare('CREATE_UID',$this->CREATE_UID);
		$criteria->compare('CREATE_DATE',$this->CREATE_DATE,true);
		$criteria->compare('LAST_UID',$this->LAST_UID);
		$criteria->compare('LAST_DATE',$this->LAST_DATE,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}