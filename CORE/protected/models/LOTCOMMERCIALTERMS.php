<?php



class LOTCOMMERCIALTERMS extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return METROSTATIONS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'REBASE_LOT_COMMERCIAL_TERMS';
	}
        
        
        
        /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                        'RentPRICES' => array(self::BELONGS_TO, 'PRICES', 'RENT_PRICES_ID'),
                         'RENT_WITH_NDS_NAME'=> array(self::BELONGS_TO, 'HANDBOOKVALUES', 'RENT_WITH_NDS'),
                    
                        //'OWNERS' => array(self::BELONGS_TO, 'OWNERS', 'OWNER_ID'),
			//'OWNERS' => array(self::HAS_ONE, 'OWNERS', 'ID'),
                        //'COMPANYS' => array(self::HAS_ONE, 'COMPANY',array('COMPANY_ID'=>'ID'),'through'=>'OWNERS'),
                       // 'address'=>array(self::HAS_ONE,'Address',array('id'=>'profile_id'),'through'=>'profile'),
       
		);
	}
 
 
}

?>
