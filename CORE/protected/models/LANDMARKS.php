<?php

/**
 * This is the model class for table "LOT_LANDMARKS".
 *
 * The followings are the available columns in table 'LOT_LANDMARKS':
 * @property integer $ID
 * @property integer $LOT_ID
 * @property string $METRO_WALK_MIN
 * @property string $METRO_AUTO_MIN
 * @property string $RAILWAY_WALK_MIN
 * @property string $RAILWAY_AUTO_MIN
 * @property integer $BUSINESS_DISTRICT_ID
 * @property integer $DIRECTION_ID
 * @property integer $LOCATION_ID
 * @property integer $ZA_MKAD
 * @property string $NOTE
 * @property integer $CREATE_UID
 * @property string $CREATE_DATE
 * @property integer $LAST_UID
 * @property string $LAST_DATE
 *
 * The followings are the available model relations:
 * @property RebaseLot $lOT
 */
class LANDMARKS extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LANDMARKS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LOT_LANDMARKS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('ID', 'required'),
			array('ID, LOT_ID, BUSINESS_DISTRICT_ID, DIRECTION_ID, LOCATION_ID, ZA_MKAD, CREATE_UID, LAST_UID', 'numerical', 'integerOnly'=>true),
			array('METRO_WALK_MIN, METRO_AUTO_MIN, RAILWAY_WALK_MIN, RAILWAY_AUTO_MIN', 'length', 'max'=>10),
			array('NOTE', 'length', 'max'=>450),
			array('CREATE_DATE, LAST_DATE', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, LOT_ID, METRO_WALK_MIN, METRO_AUTO_MIN, RAILWAY_WALK_MIN, RAILWAY_AUTO_MIN, BUSINESS_DISTRICT_ID, DIRECTION_ID, LOCATION_ID, ZA_MKAD, NOTE, CREATE_UID, CREATE_DATE, LAST_UID, LAST_DATE', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lOT' => array(self::BELONGS_TO, 'RebaseLot', 'LOT_ID'),
                        'BUSINESS_DISTRICT'=> array(self::BELONGS_TO, 'HANDBOOKVALUES', 'BUSINESS_DISTRICT_ID'),
                        'DIRECTION_NAME'=> array(self::BELONGS_TO, 'HANDBOOKVALUES', 'DIRECTION_ID'),
                        'LOCATION' => array(self::BELONGS_TO, 'HANDBOOKVALUES', 'LOCATION_ID'),
                    
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'LOT_ID' => 'Lot',
			'METRO_WALK_MIN' => 'Metro Walk Min',
			'METRO_AUTO_MIN' => 'Metro Auto Min',
			'RAILWAY_WALK_MIN' => 'Railway Walk Min',
			'RAILWAY_AUTO_MIN' => 'Railway Auto Min',
			'BUSINESS_DISTRICT_ID' => 'Business District',
			'DIRECTION_ID' => 'Direction',
			'LOCATION_ID' => 'Location',
			'ZA_MKAD' => 'Za Mkad',
			'NOTE' => 'Note',
			'CREATE_UID' => 'Create Uid',
			'CREATE_DATE' => 'Create Date',
			'LAST_UID' => 'Last Uid',
			'LAST_DATE' => 'Last Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('LOT_ID',$this->LOT_ID);
		$criteria->compare('METRO_WALK_MIN',$this->METRO_WALK_MIN,true);
		$criteria->compare('METRO_AUTO_MIN',$this->METRO_AUTO_MIN,true);
		$criteria->compare('RAILWAY_WALK_MIN',$this->RAILWAY_WALK_MIN,true);
		$criteria->compare('RAILWAY_AUTO_MIN',$this->RAILWAY_AUTO_MIN,true);
		$criteria->compare('BUSINESS_DISTRICT_ID',$this->BUSINESS_DISTRICT_ID);
		$criteria->compare('DIRECTION_ID',$this->DIRECTION_ID);
		$criteria->compare('LOCATION_ID',$this->LOCATION_ID);
		$criteria->compare('ZA_MKAD',$this->ZA_MKAD);
		$criteria->compare('NOTE',$this->NOTE,true);
		$criteria->compare('CREATE_UID',$this->CREATE_UID);
		$criteria->compare('CREATE_DATE',$this->CREATE_DATE,true);
		$criteria->compare('LAST_UID',$this->LAST_UID);
		$criteria->compare('LAST_DATE',$this->LAST_DATE,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}