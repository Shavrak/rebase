<?php

/**
 * This is the model class for table "LOT_ROOMS_AREA".
 *
 * The followings are the available columns in table 'LOT_ROOMS_AREA':
 * @property integer $ID
 * @property integer $LOT_ID
 * @property string $AREA
 * @property string $AREA_FROM
 * @property string $AREA_TO
 * @property integer $CREATE_UID
 * @property string $CREATE_DATE
 * @property integer $LAST_UID
 * @property string $LAST_DATE
 */
class ROOMSAREA extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ROOMSAREA the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LOT_ROOMS_AREA';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID', 'required'),
			array('ID, LOT_ID, CREATE_UID, LAST_UID', 'numerical', 'integerOnly'=>true),
			array('AREA, AREA_FROM, AREA_TO', 'length', 'max'=>10),
			array('CREATE_DATE, LAST_DATE', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, LOT_ID, AREA, AREA_FROM, AREA_TO, CREATE_UID, CREATE_DATE, LAST_UID, LAST_DATE', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'LOT_ID' => 'Lot',
			'AREA' => 'Area',
			'AREA_FROM' => 'Area From',
			'AREA_TO' => 'Area To',
			'CREATE_UID' => 'Create Uid',
			'CREATE_DATE' => 'Create Date',
			'LAST_UID' => 'Last Uid',
			'LAST_DATE' => 'Last Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('LOT_ID',$this->LOT_ID);
		$criteria->compare('AREA',$this->AREA,true);
		$criteria->compare('AREA_FROM',$this->AREA_FROM,true);
		$criteria->compare('AREA_TO',$this->AREA_TO,true);
		$criteria->compare('CREATE_UID',$this->CREATE_UID);
		$criteria->compare('CREATE_DATE',$this->CREATE_DATE,true);
		$criteria->compare('LAST_UID',$this->LAST_UID);
		$criteria->compare('LAST_DATE',$this->LAST_DATE,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}