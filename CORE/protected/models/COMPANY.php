<?php



class COMPANY extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return METROSTATIONS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'REBASE_COMPANY';
	}
        
        
        /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ADDRESSNAME' => array(self::BELONGS_TO, 'FIASADDRESS', 'ADDRESS_ID'),
		);
	}

 
}

?>
