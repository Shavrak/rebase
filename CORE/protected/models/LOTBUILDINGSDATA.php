<?php



class LOTBUILDINGSDATA extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return METROSTATIONS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LOT_BUILDINGS_DATA';
	}
        
        
        
        /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                       // 'READY_NAME' => array(self::BELONGS_TO, 'OWNERS', 'OWNER_ID'),
                        'READY_NAME'=> array(self::BELONGS_TO, 'HANDBOOKVALUES', 'READY_ID'),
                        'CLASS_NAME'=> array(self::BELONGS_TO, 'HANDBOOKVALUES', 'BUILDING_CLASS_ID'),
			//'OWNERS' => array(self::HAS_ONE, 'OWNERS', 'ID'),
                        //'COMPANYS' => array(self::HAS_ONE, 'COMPANY',array('COMPANY_ID'=>'ID'),'through'=>'OWNERS'),
                       // 'address'=>array(self::HAS_ONE,'Address',array('id'=>'profile_id'),'through'=>'profile'),
       
		);
	}
 
 
}

?>
