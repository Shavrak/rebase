<?php

/**
 * This is the model class for table "REBASE_HANDBOOK_VALUES".
 *
 * The followings are the available columns in table 'REBASE_HANDBOOK_VALUES':
 * @property integer $ID
 * @property integer $HANDBOOK_ID
 * @property integer $INDEX_ID
 * @property string $VARCHAR_VALUE
 * @property integer $INT_VALUE
 * @property string $DATE_VALUE
 * @property integer $ORDER_ID
 * @property string $LAST_DATE
 * @property string $LAST_UID
 *
 * The followings are the available model relations:
 * @property RebaseHandbooks $hANDBOOK
 */
class HANDBOOKVALUES extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return HANDBOOKVALUES the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'REBASE_HANDBOOK_VALUES';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('HANDBOOK_ID', 'required'),
			array('ID, HANDBOOK_ID, INDEX_ID, INT_VALUE, ORDER_ID', 'numerical', 'integerOnly'=>true),
			array('VARCHAR_VALUE', 'length', 'max'=>450),
			array('LAST_UID', 'length', 'max'=>45),
			array('DATE_VALUE, LAST_DATE', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, HANDBOOK_ID, INDEX_ID, VARCHAR_VALUE, INT_VALUE, DATE_VALUE, ORDER_ID, LAST_DATE, LAST_UID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hANDBOOK' => array(self::BELONGS_TO, 'RebaseHandbooks', 'HANDBOOK_ID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'HANDBOOK_ID' => 'Handbook',
			'INDEX_ID' => 'Index',
			'VARCHAR_VALUE' => 'Varchar Value',
			'INT_VALUE' => 'Int Value',
			'DATE_VALUE' => 'Date Value',
			'ORDER_ID' => 'Order',
			'LAST_DATE' => 'Last Date',
			'LAST_UID' => 'Last Uid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('HANDBOOK_ID',$this->HANDBOOK_ID);
		$criteria->compare('INDEX_ID',$this->INDEX_ID);
		$criteria->compare('VARCHAR_VALUE',$this->VARCHAR_VALUE,true);
		$criteria->compare('INT_VALUE',$this->INT_VALUE);
		$criteria->compare('DATE_VALUE',$this->DATE_VALUE,true);
		$criteria->compare('ORDER_ID',$this->ORDER_ID);
		$criteria->compare('LAST_DATE',$this->LAST_DATE,true);
		$criteria->compare('LAST_UID',$this->LAST_UID,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
}