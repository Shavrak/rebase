<?php

/**
 * This is the model class for table "REBASE_LOT_COMMERCIAL_TERMS".
 *
 * The followings are the available columns in table 'REBASE_LOT_COMMERCIAL_TERMS':
 * @property integer $ID
 * @property integer $LOT_ID
 * @property integer $RENT_PRICES_ID
 * @property integer $RENT_WITH_NDS
 * @property integer $OPERATING_COSTS_ID
 * @property integer $OPERATING_PRICES_ID
 * @property integer $UTILITY_BILLS_ID
 * @property integer $UTILITY_BILLLS_PRICES
 * @property integer $PAYMENT_TERMS_ID
 * @property integer $DEPOSIT_ID
 * @property string $PREPAYD_VALUE
 * @property integer $MIN_RENT_PERIOD_ID
 * @property string $PERIOD_SUBRENT
 * @property integer $RENT_TYPE_ID
 * @property string $SALE_OF_PARTS
 * @property integer $SQM_PRICES_ID
 * @property integer $FULL_SALE_PRICES_ID
 * @property integer $SALE_WITH_NDS
 * @property integer $AB_SQM_PRICES_ID
 * @property integer $AB_FULL_PRICES_ID
 * @property string $AB_GAP
 * @property string $AB_PAYBACK
 * @property string $AB_PROFITABILITY
 * @property string $LAND_INFO
 * @property string $NOTE
 * @property string $CREATE_DATE
 * @property integer $CREATE_UID
 * @property string $LAST_DATE
 * @property integer $LAST_UID
 *
 * The followings are the available model relations:
 * @property RebaseLot $lOT
 * @property RebasePrices $rENTPRICES
 * @property RebasePrices $oPERATINGPRICES
 * @property RebasePrices $uTILITYBILLLSPRICES
 * @property RebasePrices $sQMPRICES
 * @property RebasePrices $fULLSALEPRICES
 * @property RebasePrices $aBSQMPRICES
 * @property RebasePrices $aBFULLPRICES
 */
class COMMERCIALTERMS extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return COMMERCIALTERMS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'REBASE_LOT_COMMERCIAL_TERMS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID', 'required'),
			array('ID, LOT_ID, RENT_PRICES_ID, RENT_WITH_NDS, OPERATING_COSTS_ID, OPERATING_PRICES_ID, UTILITY_BILLS_ID, UTILITY_BILLLS_PRICES, PAYMENT_TERMS_ID, DEPOSIT_ID, MIN_RENT_PERIOD_ID, RENT_TYPE_ID, SQM_PRICES_ID, FULL_SALE_PRICES_ID, SALE_WITH_NDS, AB_SQM_PRICES_ID, AB_FULL_PRICES_ID, CREATE_UID, LAST_UID', 'numerical', 'integerOnly'=>true),
			array('PREPAYD_VALUE, PERIOD_SUBRENT, SALE_OF_PARTS, AB_GAP, AB_PAYBACK, AB_PROFITABILITY', 'length', 'max'=>45),
			array('LAND_INFO, NOTE', 'length', 'max'=>450),
			array('CREATE_DATE, LAST_DATE', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, LOT_ID, RENT_PRICES_ID, RENT_WITH_NDS, OPERATING_COSTS_ID, OPERATING_PRICES_ID, UTILITY_BILLS_ID, UTILITY_BILLLS_PRICES, PAYMENT_TERMS_ID, DEPOSIT_ID, PREPAYD_VALUE, MIN_RENT_PERIOD_ID, PERIOD_SUBRENT, RENT_TYPE_ID, SALE_OF_PARTS, SQM_PRICES_ID, FULL_SALE_PRICES_ID, SALE_WITH_NDS, AB_SQM_PRICES_ID, AB_FULL_PRICES_ID, AB_GAP, AB_PAYBACK, AB_PROFITABILITY, LAND_INFO, NOTE, CREATE_DATE, CREATE_UID, LAST_DATE, LAST_UID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lOT' => array(self::BELONGS_TO, 'RebaseLot', 'LOT_ID'),
			'rENTPRICES' => array(self::BELONGS_TO, 'RebasePrices', 'RENT_PRICES_ID'),
			'oPERATINGPRICES' => array(self::BELONGS_TO, 'RebasePrices', 'OPERATING_PRICES_ID'),
			'uTILITYBILLLSPRICES' => array(self::BELONGS_TO, 'RebasePrices', 'UTILITY_BILLLS_PRICES'),
			'sQMPRICES' => array(self::BELONGS_TO, 'RebasePrices', 'SQM_PRICES_ID'),
			'fULLSALEPRICES' => array(self::BELONGS_TO, 'RebasePrices', 'FULL_SALE_PRICES_ID'),
			'aBSQMPRICES' => array(self::BELONGS_TO, 'RebasePrices', 'AB_SQM_PRICES_ID'),
			'aBFULLPRICES' => array(self::BELONGS_TO, 'RebasePrices', 'AB_FULL_PRICES_ID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'LOT_ID' => 'Lot',
			'RENT_PRICES_ID' => 'Rent Prices',
			'RENT_WITH_NDS' => 'Rent With Nds',
			'OPERATING_COSTS_ID' => 'Operating Costs',
			'OPERATING_PRICES_ID' => 'Operating Prices',
			'UTILITY_BILLS_ID' => 'Utility Bills',
			'UTILITY_BILLLS_PRICES' => 'Utility Billls Prices',
			'PAYMENT_TERMS_ID' => 'Payment Terms',
			'DEPOSIT_ID' => 'Deposit',
			'PREPAYD_VALUE' => 'Prepayd Value',
			'MIN_RENT_PERIOD_ID' => 'Min Rent Period',
			'PERIOD_SUBRENT' => 'Period Subrent',
			'RENT_TYPE_ID' => 'Rent Type',
			'SALE_OF_PARTS' => 'Sale Of Parts',
			'SQM_PRICES_ID' => 'Sqm Prices',
			'FULL_SALE_PRICES_ID' => 'Full Sale Prices',
			'SALE_WITH_NDS' => 'Sale With Nds',
			'AB_SQM_PRICES_ID' => 'Ab Sqm Prices',
			'AB_FULL_PRICES_ID' => 'Ab Full Prices',
			'AB_GAP' => 'Ab Gap',
			'AB_PAYBACK' => 'Ab Payback',
			'AB_PROFITABILITY' => 'Ab Profitability',
			'LAND_INFO' => 'Land Info',
			'NOTE' => 'Note',
			'CREATE_DATE' => 'Create Date',
			'CREATE_UID' => 'Create Uid',
			'LAST_DATE' => 'Last Date',
			'LAST_UID' => 'Last Uid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('LOT_ID',$this->LOT_ID);
		$criteria->compare('RENT_PRICES_ID',$this->RENT_PRICES_ID);
		$criteria->compare('RENT_WITH_NDS',$this->RENT_WITH_NDS);
		$criteria->compare('OPERATING_COSTS_ID',$this->OPERATING_COSTS_ID);
		$criteria->compare('OPERATING_PRICES_ID',$this->OPERATING_PRICES_ID);
		$criteria->compare('UTILITY_BILLS_ID',$this->UTILITY_BILLS_ID);
		$criteria->compare('UTILITY_BILLLS_PRICES',$this->UTILITY_BILLLS_PRICES);
		$criteria->compare('PAYMENT_TERMS_ID',$this->PAYMENT_TERMS_ID);
		$criteria->compare('DEPOSIT_ID',$this->DEPOSIT_ID);
		$criteria->compare('PREPAYD_VALUE',$this->PREPAYD_VALUE,true);
		$criteria->compare('MIN_RENT_PERIOD_ID',$this->MIN_RENT_PERIOD_ID);
		$criteria->compare('PERIOD_SUBRENT',$this->PERIOD_SUBRENT,true);
		$criteria->compare('RENT_TYPE_ID',$this->RENT_TYPE_ID);
		$criteria->compare('SALE_OF_PARTS',$this->SALE_OF_PARTS,true);
		$criteria->compare('SQM_PRICES_ID',$this->SQM_PRICES_ID);
		$criteria->compare('FULL_SALE_PRICES_ID',$this->FULL_SALE_PRICES_ID);
		$criteria->compare('SALE_WITH_NDS',$this->SALE_WITH_NDS);
		$criteria->compare('AB_SQM_PRICES_ID',$this->AB_SQM_PRICES_ID);
		$criteria->compare('AB_FULL_PRICES_ID',$this->AB_FULL_PRICES_ID);
		$criteria->compare('AB_GAP',$this->AB_GAP,true);
		$criteria->compare('AB_PAYBACK',$this->AB_PAYBACK,true);
		$criteria->compare('AB_PROFITABILITY',$this->AB_PROFITABILITY,true);
		$criteria->compare('LAND_INFO',$this->LAND_INFO,true);
		$criteria->compare('NOTE',$this->NOTE,true);
		$criteria->compare('CREATE_DATE',$this->CREATE_DATE,true);
		$criteria->compare('CREATE_UID',$this->CREATE_UID);
		$criteria->compare('LAST_DATE',$this->LAST_DATE,true);
		$criteria->compare('LAST_UID',$this->LAST_UID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}