<?php

/**
 * This is the model class for table "REBASE_H_GROUPS".
 *
 * The followings are the available columns in table 'REBASE_H_GROUPS':
 * @property integer $ID
 * @property string $GROUP_NAME
 * @property string $LAST_DATE
 * @property integer $LAST_UID
 */
class HGROUPS extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return HGROUPS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'REBASE_H_GROUPS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ID', 'required'),
			array('ID, LAST_UID', 'numerical', 'integerOnly'=>true),
			array('GROUP_NAME', 'length', 'max'=>45),
			array('LAST_DATE', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, GROUP_NAME, LAST_DATE, LAST_UID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'GROUP_NAME' => 'Group Name',
			'LAST_DATE' => 'Last Date',
			'LAST_UID' => 'Last Uid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('GROUP_NAME',$this->GROUP_NAME,true);
		$criteria->compare('LAST_DATE',$this->LAST_DATE,true);
		$criteria->compare('LAST_UID',$this->LAST_UID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}