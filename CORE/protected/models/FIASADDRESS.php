<?php

/**
 * This is the model class for table "FIAS_ADDRESS".
 *
 * The followings are the available columns in table 'FIAS_ADDRESS':
 * @property integer $ID
 * @property string $AOGUID
 * @property string $HOUSEID
 * @property string $ADDRESS
 */
class FIASADDRESS extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FIASADDRESS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'FIAS_ADDRESS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('AOGUID, HOUSEID', 'length', 'max'=>36),
			array('ADDRESS', 'length', 'max'=>450),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, AOGUID, HOUSEID, ADDRESS', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'AOGUID' => 'Aoguid',
			'HOUSEID' => 'Houseid',
			'ADDRESS' => 'Address',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('AOGUID',$this->AOGUID,true);
		$criteria->compare('HOUSEID',$this->HOUSEID,true);
		$criteria->compare('ADDRESS',$this->ADDRESS,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}