<?php

/**
 * This is the model class for table "REBASE_LOT".
 *
 * The followings are the available columns in table 'REBASE_LOT':
 * @property integer $ID
 * @property string $LOT
 * @property string $NAME
 * @property string $LOT_STATUS_ID
 * @property integer $BROKER_ID
 * @property string $CREATE_DATE
 * @property integer $CREATE_UID
 * @property string $LAST_DATE
 * @property integer $LAST_UID
 */
class REBASELOT extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return REBASELOT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'REBASE_LOT';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('ID', 'required'),
			array('ID, BROKER_ID, CREATE_UID, LAST_UID', 'numerical', 'integerOnly'=>true),
			array('LOT, NAME, LOT_STATUS_ID', 'length', 'max'=>45),
			array('CREATE_DATE, LAST_DATE', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ID, LOT, NAME, LOT_STATUS_ID, BROKER_ID, CREATE_DATE, CREATE_UID, LAST_DATE, LAST_UID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'lASTU' => array(self::BELONGS_TO, 'USER', 'LAST_UID'),
                    //'LANDMARK'=> array(self::BELONGS_TO, 'METROSTATIONS', 'LOT_ID'),
                    //'METRO' => array(self::BELONGS_TO, 'LANDMARK', 'METRO_STATION_ID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'LOT' => 'Lot',
			'NAME' => 'Name',
			'LOT_STATUS_ID' => 'Lot Status',
			'BROKER_ID' => 'Broker',
			'CREATE_DATE' => 'Create Date',
			'CREATE_UID' => 'Create Uid',
			'LAST_DATE' => 'Last Date',
			'LAST_UID' => 'Last Uid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('LOT',$this->LOT,true);
		$criteria->compare('NAME',$this->NAME,true);
		$criteria->compare('LOT_STATUS_ID',$this->LOT_STATUS_ID,true);
		$criteria->compare('BROKER_ID',$this->BROKER_ID);
		$criteria->compare('CREATE_DATE',$this->CREATE_DATE,true);
		$criteria->compare('CREATE_UID',$this->CREATE_UID);
		$criteria->compare('LAST_DATE',$this->LAST_DATE,true);
		$criteria->compare('LAST_UID',$this->LAST_UID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function search_from_form()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('LOT',$this->LOT,true);
		$criteria->compare('NAME',$this->NAME,true);
		$criteria->compare('LOT_STATUS_ID',$this->LOT_STATUS_ID,true);
		$criteria->compare('BROKER_ID',$this->BROKER_ID);
		$criteria->compare('CREATE_DATE',$this->CREATE_DATE,true);
		$criteria->compare('CREATE_UID',$this->CREATE_UID);
		$criteria->compare('LAST_DATE',$this->LAST_DATE,true);
		$criteria->compare('LAST_UID',$this->LAST_UID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}