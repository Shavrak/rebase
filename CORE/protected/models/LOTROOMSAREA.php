<?php

class LOTROOMSAREA extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return METROSTATIONS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'LOT_ROOMS_AREA';
	}
 
        public function SaveRoomArea($lotid, $roomid, $area, $create_uid, $last_uid){
            $model = new LOTROOMSAREA();
            $is_interval = 0;  
            
            $model->LOT_ID = $lotid;                          echo"<pre>";
//                          var_dump($area);
//                          echo "<hr>";
            
//            $model->AREA = $area;
//            $model->AREA_FROM = 0;
//            $model->AREA_TO = 0;
            
            
            
              //$area = '12';
              if(strrpos($area, "-") !== false){
                  $is_interval = 1;
                  $model->AREA_FROM = substr($area,0,(strrpos($area, "-")));
                  $model->AREA_TO = substr($area,(strrpos($area, "-")+1));
                  
              }else{
                  $model->AREA = $area;
                  $model->AREA_FROM = $area;
                  $model->AREA_TO = $area;
              }
            $model->ROOM_LOT_ID = $roomid;  
            $model->IS_INTERVAL = $is_interval;
            $model->CREATE_UID = $create_uid;
            $model->CREATE_DATE = new CDbExpression('NOW()');
            $model->LAST_UID = $last_uid;
            $model->LAST_DATE = new CDbExpression('NOW()');       
            $model->save();
//            $min_area = Yii::app()->db->createCommand()
//                    ->select('ifnull(min(AREA),0) as v')
//                    ->from('LOT_ROOMS_AREA t')
//                    ->where('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lotid))
//                    ->queryRow();
//            $max_area = Yii::app()->db->createCommand()
//                    ->select('ifnull(max(AREA),0) as v')
//                    ->from('LOT_ROOMS_AREA t')
//                    ->where('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lotid))
//                    ->queryRow();
//            
//
//            Yii::app()->db->createCommand()
//                    ->update('LOT_ROOMS_AREA',
//                     array('AREA_FROM'=>$min_area['v'],
//                             'AREA_TO'=>$max_area['v'],
//                       ),
//                    'LOT_ID=:LOT_ID', array(':LOT_ID'=>$lotid));
            
//            $model1 = $model5->findAll('LOT_ID=:LOT_ID', array(':LOT_ID'=>$lotid));    
//            //print_r($min_area);$min_area['v']
//            $model1->AREA_FROM = 50; //$min_area['v'];
//            $model1->AREA_TO = 100;//$max_area['v'];
//            $model1->save();
            
        }
        
        
        
//        /**
//	 * @return array relational rules.
//	 */
//	public function relations()
//	{
//		// NOTE: you may need to adjust the relation name and the related
//		// class name for the relations automatically generated below.
//		return array(
//			'COMPANYS' => array(self::BELONGS_TO, 'COMPANY', 'COMPANY_ID'),
//		);
//	}
}

?>
