<?php

/**
 * This is the model class for table "REBASE_LOT_ROOMS".
 *
 * The followings are the available columns in table 'REBASE_LOT_ROOMS':
 * @property integer $ID
 * @property integer $LOT_ID
 * @property string $AREA
 * @property integer $FLOOR_NUMBER
 * @property string $FIAS_AOID
 * @property string $FINISHING_ID
 * @property integer $CONTARCT_TYPE
 * @property integer $BROKER_ID
 * @property string $COMMENTS
 * @property integer $RENT_PRICES_ID
 * @property integer $WITH_NDS
 * @property string $OPERATING_COSTS
 * @property integer $UTILITY_BILLS_ID
 * @property integer $DEPOSITS_ID
 * @property string $PREPAYD_VALUE
 * @property integer $RENT_TYPE_ID
 * @property string $OUR_PERCENT
 * @property integer $RENT_CONTRACT_ID
 * @property integer $SALE_PRICES_ID
 * @property integer $SALE_CONTRACT_ID
 * @property string $CREATE_DATE
 * @property integer $CREATE_UID
 * @property string $LAST_DATE
 * @property integer $LAST_UID
 *
 * The followings are the available model relations:
 * @property RebaseLot $lOT
 * @property RebasePrices $rENTPRICES
 * @property RebasePrices $sALEPRICES
 * @property RebaseContracts $rENTCONTRACT
 * @property RebaseContracts $sALECONTRACT
 */
class LOTROOMS extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LOTROOMS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'REBASE_LOT_ROOMS';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lOT' => array(self::BELONGS_TO, 'RebaseLot', 'LOT_ID'),
			'RentPRICES' => array(self::BELONGS_TO, 'PRICES', 'RENT_PRICES_ID'),
                        'FINISHING' => array(self::BELONGS_TO, 'HANDBOOKVALUES', 'FINISHING_ID'),
                        'CONTRACT_T' => array(self::BELONGS_TO, 'HANDBOOKVALUES', 'CONTRACT_TYPE'),
                        'UTILITY_BILLS' => array(self::BELONGS_TO, 'HANDBOOKVALUES', 'UTILITY_BILLS_ID'),
                        'DEPOSIT' => array(self::BELONGS_TO, 'HANDBOOKVALUES', 'DEPOSIT_ID'),
                        'RENT_TYPE' => array(self::BELONGS_TO, 'HANDBOOKVALUES', 'RENT_TYPE_ID'),
                        'PLANING' => array(self::BELONGS_TO, 'HANDBOOKVALUES', 'PLANING_ID'),
                        'RENT_W_NDS' => array(self::BELONGS_TO, 'HANDBOOKVALUES', 'RENT_WITH_NDS'),
                    
//                        'FINISHING'=>array(self::HAS_ONE, 'HANDBOOKVALUES', 'FINISHING_ID',
//                        'condition'=>'FINISHING_ID.main=1'),
			//'sALEPRICES' => array(self::BELONGS_TO, 'RebasePrices', 'SALE_PRICES_ID'),
			//'rENTCONTRACT' => array(self::BELONGS_TO, 'RebaseContracts', 'RENT_CONTRACT_ID'),
			//'sALECONTRACT' => array(self::BELONGS_TO, 'RebaseContracts', 'SALE_CONTRACT_ID'),
                        'broker' => array(self::BELONGS_TO, 'USER', 'BROKER_ID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	
}